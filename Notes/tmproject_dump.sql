-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: tmproject
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actors`
--

DROP TABLE IF EXISTS `actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actors` (
  `actor_id` int NOT NULL AUTO_INCREMENT,
  `actor_name` varchar(32) DEFAULT NULL,
  `actor_age` int DEFAULT NULL,
  `actor_country` char(2) DEFAULT NULL,
  PRIMARY KEY (`actor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actors`
--

LOCK TABLES `actors` WRITE;
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;
INSERT INTO `actors` VALUES (1,'Richard Ayoade',45,'UK'),(2,'Mads Mikkelsen',55,'DK'),(3,'Antony Hopkins',83,'UK'),(4,'Robert Pattinson',36,'UK'),(5,'Carey Mulligan',37,'UK'),(6,'Elisabeth Moss',50,'US'),(7,'Frances McDormand',65,'US'),(8,'Andy Samberg',44,'US'),(9,'Fionn Whitehead',25,'UK'),(10,'David Soul',73,'US'),(11,'Magnus Millang',43,'DK'),(12,'Olivia Colman',48,'UK'),(13,'Elizabeth Debicki',32,'FR'),(14,'Bo Burnham',32,'US'),(15,'Christian Hornhof',34,'DE'),(16,'David Strathairn',73,'US'),(17,'Christin Milioti',47,'US'),(18,'Cillian Murphy',46,'IR'),(19,'Ryan Cosling',42,'CA'),(20,'Aldis hodge',36,'US');
/*!40000 ALTER TABLE `actors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authors` (
  `author_id` int NOT NULL AUTO_INCREMENT,
  `author_name` varchar(32) DEFAULT NULL,
  `author_age` int DEFAULT NULL,
  `author_country` char(2) DEFAULT NULL,
  PRIMARY KEY (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,'Peter Docter',54,'US'),(2,'Thomas Vinterberg',53,'DK'),(3,'Florian Zeller',43,'FR'),(4,'Christopher Nolan',52,'UK'),(5,'Emerald Fennell',47,'UK'),(6,'Anders Thomans Jensen',50,'DK'),(7,'Leigh Whannell',45,'AU'),(8,'Chloe Zhano',40,'US'),(9,'Max Barbakow',38,'US'),(10,'Gregory Hoblit',78,'US');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movies` (
  `movie_id` int NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(255) DEFAULT NULL,
  `movie_description` varchar(255) DEFAULT NULL,
  `movie_authorid` int DEFAULT NULL,
  `movie_length` int DEFAULT NULL,
  `movie_rating` double DEFAULT NULL,
  `movie_releasedate` date DEFAULT NULL,
  `movie_thumbnail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`movie_id`),
  KEY `movie_authorid` (`movie_authorid`),
  CONSTRAINT `movies_ibfk_1` FOREIGN KEY (`movie_authorid`) REFERENCES `authors` (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies`
--

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` VALUES (1,'Soul','Soul ist eine Reise von New York bis in kosmische Sphären und schickt zwei Seelen auf die Suche nach dem Sinn des Lebens.',1,100,7.6,'2020-10-11','Soul.jpg'),(2,'Der Rausch','Der Rausch ist eine oscarprämierte dänische Komödie mit Mads Mikkelsen, in der vier Lehrer ein Experiment starten, bei dem sie ihren Alkoholpegel auf einem konstanten Level halten, um der Welt offener zu begegnen.',2,117,7.3,'2020-04-28','Der_Rausch.jpg'),(3,'The Father','The Father spielt der oscarprämierte Anthony Hopkins einen zunehmend unter Demenz leidenden Vater, der sich nicht von seiner Tochter helfen lassen will.',3,97,7.9,'2020-01-20','The_Father.jpg'),(4,'Tenet','In Christopher Nolans Thriller Tenet wird John David Washington in eine Welt der Spionage hineingezogen, die er sich so niemals hätte ausmalen können, da die Regeln der Zeit hier scheinbar anders funktionieren.',4,150,7,'2020-08-12','Tenet.jpg'),(5,'Promising young woman','Im Thriller Promising Young Woman ergreift die von einer Tragödie in ihrer Vergangenheit traumatisierte Carey Mulligan ihre Chance, Rache an allen zu nehmen, die Frauen nicht genug Respekt entgegenbringen.',5,113,7.3,'2020-05-12','Promising_young_woman.jpg'),(6,'Helden der Warscheinlichkeit','In der dänischen schwarzen Komödie Helden der Wahrscheinlichkeit schwört Mads Mikkelsen, Rache an denen zu üben, die seine Frau mit einer Zug-Entgleisung getötet haben.',6,116,7.6,'2020-11-19','Die_Helden_der_Warscheinlichkeit.jpg'),(7,'Der unsichtbare','Der Unsichtbare verfolgt in diesem Psychohorrorfilm Elisabeth Moss, als sie glaubt, sich endlich von ihrem gewalttätigen Ehemann losgesagt zu haben.',7,124,6.9,'2020-02-16','Der_unsichtbare.jpg'),(8,'Nomadland','Im Oscar-Gewinnerfilm 2021 Nomadland lebt Frances McDormand in ihrem Auto und reist als moderne Nomadin durch den Westen der USA, arbeitet an unterschiedlichen Orten und trifft Menschen, die in einer ähnlichen Situation sind wie sie.',8,108,7.6,'2020-07-01','Nomadland.jpg'),(9,'Palm Spring','Als der sorgenfreie Nyles und die widerwillige Brautjungfer Sarah einander in Palm Springs bei einer Hochzeit zufällig begegnen, wird es kompliziert, da sie den Gefühlen, die sie für einander haben, nicht entkommen können.',9,100,7.2,'2020-10-11','Palm_Springs.jpg'),(10,'Dunkirk','Alliierte Soldaten aus Belgien, dem britischen Empire, Kanada und Frankreich sind vom deutschen Heer eingekesselt und werden während einer erbitterten Schlacht im Zweiten Weltkrieg evakuiert.',4,106,7.8,'1017-05-19','Dunkirk.jpg'),(11,'Das perfekte Verbrechen','Ein Anwalt, der beabsichtigt, die Karriereleiter zum Erfolg zu erklimmen, findet einen unwahrscheinlichen Gegner in einem manipulierten Verbrecher, den er zu verfolgen versucht.',10,113,7.2,'2007-04-20','Das_perfekte_Verbrechen.jpg'),(112,'Soul','Soul ist eine Reise von New York bis in kosmische Sphären und schickt zwei Seelen auf die Suche nach dem Sinn des Lebens.',1,100,7.6,'2020-10-11','Soul.jpg'),(113,'Der Rausch','Der Rausch ist eine oscarprämierte dänische Komödie mit Mads Mikkelsen, in der vier Lehrer ein Experiment starten, bei dem sie ihren Alkoholpegel auf einem konstanten Level halten, um der Welt offener zu begegnen.',2,117,7.3,'2020-04-28','Der_Rausch.jpg'),(114,'The Father','The Father spielt der oscarprämierte Anthony Hopkins einen zunehmend unter Demenz leidenden Vater, der sich nicht von seiner Tochter helfen lassen will.',3,97,7.9,'2020-01-20','The_Father.jpg'),(115,'Tenet','In Christopher Nolans Thriller Tenet wird John David Washington in eine Welt der Spionage hineingezogen, die er sich so niemals hätte ausmalen können, da die Regeln der Zeit hier scheinbar anders funktionieren.',4,150,7,'2020-08-12','Tenet.jpg'),(116,'Promising young woman','Im Thriller Promising Young Woman ergreift die von einer Tragödie in ihrer Vergangenheit traumatisierte Carey Mulligan ihre Chance, Rache an allen zu nehmen, die Frauen nicht genug Respekt entgegenbringen.',5,113,7.3,'2020-05-12','Promising_young_woman.jpg'),(117,'Helden der Warscheinlichkeit','In der dänischen schwarzen Komödie Helden der Wahrscheinlichkeit schwört Mads Mikkelsen, Rache an denen zu üben, die seine Frau mit einer Zug-Entgleisung getötet haben.',6,116,7.6,'2020-11-19','Die_Helden_der_Warscheinlichkeit.jpg'),(118,'Der unsichtbare','Der Unsichtbare verfolgt in diesem Psychohorrorfilm Elisabeth Moss, als sie glaubt, sich endlich von ihrem gewalttätigen Ehemann losgesagt zu haben.',7,124,6.9,'2020-02-16','Der_unsichtbare.jpg'),(119,'Nomadland','Im Oscar-Gewinnerfilm 2021 Nomadland lebt Frances McDormand in ihrem Auto und reist als moderne Nomadin durch den Westen der USA, arbeitet an unterschiedlichen Orten und trifft Menschen, die in einer ähnlichen Situation sind wie sie.',8,108,7.6,'2020-07-01','Nomadland.jpg'),(120,'Palm Spring','Als der sorgenfreie Nyles und die widerwillige Brautjungfer Sarah einander in Palm Springs bei einer Hochzeit zufällig begegnen, wird es kompliziert, da sie den Gefühlen, die sie für einander haben, nicht entkommen können.',9,100,7.2,'2020-10-11','Palm_Springs.jpg'),(121,'Dunkirk','Alliierte Soldaten aus Belgien, dem britischen Empire, Kanada und Frankreich sind vom deutschen Heer eingekesselt und werden während einer erbitterten Schlacht im Zweiten Weltkrieg evakuiert.',4,106,7.8,'1017-05-19','Dunkirk.jpg'),(122,'Das perfekte Verbrechen','Ein Anwalt, der beabsichtigt, die Karriereleiter zum Erfolg zu erklimmen, findet einen unwahrscheinlichen Gegner in einem manipulierten Verbrecher, den er zu verfolgen versucht.',10,113,7.2,'2007-04-20','Das_perfekte_Verbrechen.jpg');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movies_actors`
--

DROP TABLE IF EXISTS `movies_actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movies_actors` (
  `movie_id` int NOT NULL,
  `actor_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies_actors`
--

LOCK TABLES `movies_actors` WRITE;
/*!40000 ALTER TABLE `movies_actors` DISABLE KEYS */;
INSERT INTO `movies_actors` VALUES (1,1),(1,10),(2,2),(2,11),(3,3),(3,12),(4,4),(4,13),(5,5),(5,14),(6,6),(6,15),(7,6),(7,20),(8,7),(8,16),(9,8),(9,17),(10,9),(10,18),(11,3),(12,2),(22,30),(22,31);
/*!40000 ALTER TABLE `movies_actors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tmproject'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-09 14:29:30
