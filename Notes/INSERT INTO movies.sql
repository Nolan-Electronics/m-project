INSERT INTO movies 
(movie_name, movie_description, movie_directorname, movie_directorid, movie_length, movie_rating, movie_releasedate)
VALUES
("Soul","Soul ist eine Reise von New York bis in kosmische Sphären und schickt zwei Seelen auf die Suche nach dem Sinn des Lebens.","Peter Docter",1,100,7.6,"2020-10-11"),
("Der Rausch","Der Rausch ist eine oscarprämierte dänische Komödie mit Mads Mikkelsen, in der vier Lehrer ein Experiment starten, bei dem sie ihren Alkoholpegel auf einem konstanten Level halten, um der Welt offener zu begegnen.","Thomas Vinterberg",2,117,7.3,"2020-04-28"),
("The Father","The Father spielt der oscarprämierte Anthony Hopkins einen zunehmend unter Demenz leidenden Vater, der sich nicht von seiner Tochter helfen lassen will.","Florian Zeller",3,97,7.9,"2020-01-20"),
("Tenet","In Christopher Nolans Thriller Tenet wird John David Washington in eine Welt der Spionage hineingezogen, die er sich so niemals hätte ausmalen können, da die Regeln der Zeit hier scheinbar anders funktionieren.","Christopher Nolan",4,150,7,"2020-08-12"),
("Promising young woman","Im Thriller Promising Young Woman ergreift die von einer Tragödie in ihrer Vergangenheit traumatisierte Carey Mulligan ihre Chance, Rache an allen zu nehmen, die Frauen nicht genug Respekt entgegenbringen.","Emerald Fennell",5,113,7.3,"2020-05-12"),
("Helden der Warscheinlichkeit","In der dänischen schwarzen Komödie Helden der Wahrscheinlichkeit schwört Mads Mikkelsen, Rache an denen zu üben, die seine Frau mit einer Zug-Entgleisung getötet haben.","Anders Thomans Jensen",6,116,7.6,"2020-11-19"),
("Der unsichtbare","Der Unsichtbare verfolgt in diesem Psychohorrorfilm Elisabeth Moss, als sie glaubt, sich endlich von ihrem gewalttätigen Ehemann losgesagt zu haben.","Leigh Whannell",7,124,6.9,"2020-02-16"),
("Nomadland","Im Oscar-Gewinnerfilm 2021 Nomadland lebt Frances McDormand in ihrem Auto und reist als moderne Nomadin durch den Westen der USA, arbeitet an unterschiedlichen Orten und trifft Menschen, die in einer ähnlichen Situation sind wie sie.","Chloe Zhano",8,108,7.6,"2020-07-01"),
("Palm Spring","Als der sorgenfreie Nyles und die widerwillige Brautjungfer Sarah einander in Palm Springs bei einer Hochzeit zufällig begegnen, wird es kompliziert, da sie den Gefühlen, die sie für einander haben, nicht entkommen können.","Max Barbakow",9,100,7.2,"2020-10-11"),
("Dunkirk","Alliierte Soldaten aus Belgien, dem britischen Empire, Kanada und Frankreich sind vom deutschen Heer eingekesselt und werden während einer erbitterten Schlacht im Zweiten Weltkrieg evakuiert.","Christopher Nolan",4,106,7.8,"1017-05-19"),
("Das perfekte Verbrechen","Ein Anwalt, der beabsichtigt, die Karriereleiter zum Erfolg zu erklimmen, findet einen unwahrscheinlichen Gegner in einem manipulierten Verbrecher, den er zu verfolgen versucht.","Gregory Hoblit",10,113,7.2,"2007-04-20"),
("Dune", "Eine Adelsfamilie wird in einen Krieg um die Kontrolle über das wertvollste Gut der Galaxis verwickelt, während ihr Erbe von Visionen einer dunklen Zukunft geplagt wird.", "Denis Villeneuve",18, 155, 8.0, "2021-10-22"),
("Drive My Car", "Nishijima Hidetoshi ist als Bühnenschauspieler und Regisseur glücklich mit seiner Dramatikerin verheiratet. Dann verschwindet eines Tages die Frau.", "Ryûsuke Hamaguchi", 19,179, 7.6, "2021-12-22"),
("The French Dispatch", "The French Dispatch erweckt eine Sammlung von Geschichten aus der letzten Ausgabe eines amerikanischen Magazins, das in einer fiktiven französischen Stadt des 20. Jahrhunderts erscheint, zum Leben.", "Wes Anderson", 15,107, 7.2, "2021-10-21"),
("Die Ausgrabung", "Während der Zweite Weltkrieg auszubrechen droht, heuert eine vermögende Witwe einen Hobbyarchäologen an, um die Hügel auf ihrem Landsitz auszugraben.", "Simon Stone", 20,112, 7.1, "2021-1-14"),
("Little Fish", "Ein Paar kämpft darum, seine Beziehung zusammenzuhalten, während sich ein Virus des Gedächtnisverlusts ausbreitet und droht, die Geschichte ihrer Liebe und ihres Werbens auszulöschen.", "Chad Hartigan", 21,101, 6.8, "2021-2-5"),
("Last Night in Soho", "Geplanter Horror-Thriller von Edgar Wright, bei dem eine Frau aus London im Mittelpunkt stehen und der sich an <<Wenn die Gondeln Trauer tragen>> und Roman Polanskis <<Ekel>> orientieren soll.", "Edgar Wright", 22,116, 7.1, "2021-9-4"),
("Spider-Man No Way Home", "Da Spider-Mans Identität nun aufgedeckt ist, bittet Peter Doktor Strange um Hilfe. Als ein Zauber schiefgeht, tauchen gefährliche Feinde aus anderen Welten auf und zwingen Peter zu entdecken, was es wirklich bedeutet, Spider-Man zu sein.", "John Watts", 23,148, 8.3, "2021-12-17"),
("James Bond 007 Keine Zeit zu sterben", "Eigentlich wollte James Bond mit seiner großen Liebe Madeleine Swann seinen Ruhestand genießen und endlich ein normales Leben führen. Doch zurück in sein altes Leben holt ihn sein alter Freund, CIA-Agent Felix Leiter.", "Cary Joji Fukunaga", 24,163, 7.3, "2021-9-30"),
("The Green Knight", "Eine Fantasy-Nacherzählung der mittelalterlichen Geschichte von Sir Gawain und dem Grünen Ritter.", "David Lowery", 25, 130, 6.6, "2021-7-30"),
("The Power of the Dog", "Der charismatische Rancher Phil Burbank löst in seiner Umgebung Furcht und Ehrfurcht aus. Als sein Bruder eine neue Frau und ihren Sohn nach Hause bringt, quält Phil sie, bis er sich seiner eigenen Möglichkeit der Liebe ausgesetzt sieht.", "Jane Campion", 26,126, 6.8, "2021-9-2"),
("West Side Story", "Eine Adaption des Musicals von 1957, West Side Story, untersucht verbotene Liebe und die Rivalität zwischen den Jets und den Sharks, zwei Straßenbanden unterschiedlicher ethnischer Herkunft.", "Steven Spielberg", 27,156, 7.2, "2021-12-10");









###############################





INSERT INTO movies 
(movie_name, movie_description, movie_directorname, movie_directorid, movie_length, movie_rating, movie_releasedate)
VALUES
(\"Soul\",\"Soul ist eine Reise von New York bis in kosmische Sphären und schickt zwei Seelen auf die Suche nach dem Sinn des Lebens.\",\"Peter Docter\",1,100,7.6,\"2020-10-11\"),
(\"Der Rausch\",\"Der Rausch ist eine oscarprämierte dänische Komödie mit Mads Mikkelsen, in der vier Lehrer ein Experiment starten, bei dem sie ihren Alkoholpegel auf einem konstanten Level halten, um der Welt offener zu begegnen.\",\"Thomas Vinterberg\",2,117,7.3,\"2020-04-28\"),
(\"The Father\",\"The Father spielt der oscarprämierte Anthony Hopkins einen zunehmend unter Demenz leidenden Vater, der sich nicht von seiner Tochter helfen lassen will.\",\"Florian Zeller\",3,97,7.9,\"2020-01-20\"),
(\"Tenet\",\"In Christopher Nolans Thriller Tenet wird John David Washington in eine Welt der Spionage hineingezogen, die er sich so niemals hätte ausmalen können, da die Regeln der Zeit hier scheinbar anders funktionieren.\",\"Christopher Nolan\",4,150,7,\"2020-08-12\"),
(\"Promising young woman\",\"Im Thriller Promising Young Woman ergreift die von einer Tragödie in ihrer Vergangenheit traumatisierte Carey Mulligan ihre Chance, Rache an allen zu nehmen, die Frauen nicht genug Respekt entgegenbringen.\",\"Emerald Fennell\",5,113,7.3,\"2020-05-12\"),
(\"Helden der Warscheinlichkeit\",\"In der dänischen schwarzen Komödie Helden der Wahrscheinlichkeit schwört Mads Mikkelsen, Rache an denen zu üben, die seine Frau mit einer Zug-Entgleisung getötet haben.\",\"Anders Thomans Jensen\",6,116,7.6,\"2020-11-19\"),
(\"Der unsichtbare\",\"Der Unsichtbare verfolgt in diesem Psychohorrorfilm Elisabeth Moss, als sie glaubt, sich endlich von ihrem gewalttätigen Ehemann losgesagt zu haben.\",\"Leigh Whannell\",7,124,6.9,\"2020-02-16\"),
(\"Nomadland\",\"Im Oscar-Gewinnerfilm 2021 Nomadland lebt Frances McDormand in ihrem Auto und reist als moderne Nomadin durch den Westen der USA, arbeitet an unterschiedlichen Orten und trifft Menschen, die in einer ähnlichen Situation sind wie sie.\",\"Chloe Zhano\",8,108,7.6,\"2020-07-01\"),
(\"Palm Spring\",\"Als der sorgenfreie Nyles und die widerwillige Brautjungfer Sarah einander in Palm Springs bei einer Hochzeit zufällig begegnen, wird es kompliziert, da sie den Gefühlen, die sie für einander haben, nicht entkommen können.\",\"Max Barbakow\",9,100,7.2,\"2020-10-11\"),
(\"Dunkirk\",\"Alliierte Soldaten aus Belgien, dem britischen Empire, Kanada und Frankreich sind vom deutschen Heer eingekesselt und werden während einer erbitterten Schlacht im Zweiten Weltkrieg evakuiert.\",\"Christopher Nolan\",4,106,7.8,\"1017-05-19\"),
(\"Das perfekte Verbrechen\",\"Ein Anwalt, der beabsichtigt, die Karriereleiter zum Erfolg zu erklimmen, findet einen unwahrscheinlichen Gegner in einem manipulierten Verbrecher, den er zu verfolgen versucht.\",\"Gregory Hoblit\",10,113,7.2,\"2007-04-20\"),
(\"Dune\", \"Eine Adelsfamilie wird in einen Krieg um die Kontrolle über das wertvollste Gut der Galaxis verwickelt, während ihr Erbe von Visionen einer dunklen Zukunft geplagt wird.\", \"Denis Villeneuve\",18, 155, 8.0, \"2021-10-22\"),
(\"Drive My Car\", \"Nishijima Hidetoshi ist als Bühnenschauspieler und Regisseur glücklich mit seiner Dramatikerin verheiratet. Dann verschwindet eines Tages die Frau.\", \"Ryûsuke Hamaguchi\", 19,179, 7.6, \"2021-12-22\"),
(\"The French Dispatch\", \"The French Dispatch erweckt eine Sammlung von Geschichten aus der letzten Ausgabe eines amerikanischen Magazins, das in einer fiktiven französischen Stadt des 20. Jahrhunderts erscheint, zum Leben.\", \"Wes Anderson\", 15,107, 7.2, \"2021-10-21\"),
(\"Die Ausgrabung\", \"Während der Zweite Weltkrieg auszubrechen droht, heuert eine vermögende Witwe einen Hobbyarchäologen an, um die Hügel auf ihrem Landsitz auszugraben.\", \"Simon Stone\", 20,112, 7.1, \"2021-1-14\"),
(\"Little Fish\", \"Ein Paar kämpft darum, seine Beziehung zusammenzuhalten, während sich ein Virus des Gedächtnisverlusts ausbreitet und droht, die Geschichte ihrer Liebe und ihres Werbens auszulöschen.\", \"Chad Hartigan\", 21,101, 6.8, \"2021-2-5\"),
(\"Last Night in Soho\", \"Geplanter Horror-Thriller von Edgar Wright, bei dem eine Frau aus London im Mittelpunkt stehen und der sich an <<Wenn die Gondeln Trauer tragen>> und Roman Polanskis <<Ekel>> orientieren soll.\", \"Edgar Wright\", 22,116, 7.1, \"2021-9-4\"),
(\"Spider-Man No Way Home\", \"Da Spider-Mans Identität nun aufgedeckt ist, bittet Peter Doktor Strange um Hilfe. Als ein Zauber schiefgeht, tauchen gefährliche Feinde aus anderen Welten auf und zwingen Peter zu entdecken, was es wirklich bedeutet, Spider-Man zu sein.\", \"John Watts\", 23,148, 8.3, \"2021-12-17\"),
(\"James Bond 007 Keine Zeit zu sterben\", \"Eigentlich wollte James Bond mit seiner großen Liebe Madeleine Swann seinen Ruhestand genießen und endlich ein normales Leben führen. Doch zurück in sein altes Leben holt ihn sein alter Freund, CIA-Agent Felix Leiter.\", \"Cary Joji Fukunaga\", 24,163, 7.3, \"2021-9-30\"),
(\"The Green Knight\", \"Eine Fantasy-Nacherzählung der mittelalterlichen Geschichte von Sir Gawain und dem Grünen Ritter.\", \"David Lowery\", 25, 130, 6.6, \"2021-7-30\"),
(\"The Power of the Dog\", \"Der charismatische Rancher Phil Burbank löst in seiner Umgebung Furcht und Ehrfurcht aus. Als sein Bruder eine neue Frau und ihren Sohn nach Hause bringt, quält Phil sie, bis er sich seiner eigenen Möglichkeit der Liebe ausgesetzt sieht.\", \"Jane Campion\", 26,126, 6.8, \"2021-9-2\"),
(\"West Side Story\", \"Eine Adaption des Musicals von 1957, West Side Story, untersucht verbotene Liebe und die Rivalität zwischen den Jets und den Sharks, zwei Straßenbanden unterschiedlicher ethnischer Herkunft.\", \"Steven Spielberg\", 27,156, 7.2, \"2021-12-10\")



###############

INSERT INTO `directors` (`director_name`, `director_birthdate`, `director_country`) VALUES ("Peter Docter","1986-10-9","US"),("Thomas Vinterberg","1969-5-19","DK"),("Florian Zeller","1979-6-28","FR"),("Christopher Nolan","1970-7-30","UK"),("Emerald Fennell","1985-10-1","UK"),("Anders Thomans Jensen","1972-4-6","DK"),("Leigh Whannell","1977-1-17","AU"),("Chloe Zhano","1982-3-31","US"),("Max Barbakow","1982-3-4","US"),("Gregory Hoblit","1944-11-27","US"),("Matt Reeves", "1966-4-27", "US"),("James Cameron", "1954-8-16", "KH"),("Rian Johnson", "1973-12-17", "US"),("Matthew Vaughn", "1971-3-7", "UK"),("Wes Anderson", "1969-5-1", "US"),("Martin Scorsese", "1942-11-17", "US"),("Jamie Foxx", "1967-12-13", "US"),("Denis Villeneuve", "1967-10-3", "KH"),("Ryûsuke Hamaguchi", "1978-12-16", "JP"),("Simon Stone", "1984-8-19", "US"),("Chad Hartigan", "1982-8-31", "CY"),("Edgar Wright", "1974-4-18", "UK"),("John Watts", "1954-12-27", "US"),("Cary Joji Fukunaga", "1977-7-10", "US"),("David Lowery", "1980-12-26", "US"),("Jane Campion", "1954-4-30", "NZ"),("Steven Spielberg", "1946-12-18", "US"),("Paul Thomas Anderson", "1970-6-26", "US"),("Guillermo del Toro", "1964-10-9", "MX"),("Nikole Beckwith", "1970-1-1", "US"),("Sian Heder", "1977-6-23", "US"),("Ridley Scott", "1937-11-30", "UK"),("Ilya Naishuller", "1937-11-30", "RU"),("Michael Sarnoski", "1970-1-1", "US")