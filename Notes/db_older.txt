+-----------------------+--------------------------------------+----------+
| director_name         | movie_name                           | movie_id |
+-----------------------+--------------------------------------+----------+
| Peter Docter          | Soul                                 |        1 |
| Thomas Vinterberg     | Der Rausch                           |        2 |
| Florian Zeller        | The Father                           |        3 |
| Christopher Nolan     | Tenet                                |        4 |
| Emerald Fennell       | Promising young woman                |        5 |
| Anders Thomans Jensen | Helden der Warscheinlichkeit         |        6 |
| Leigh Whannell        | Der unsichtbare                      |        7 |
| Chloe Zhano           | Nomadland                            |        8 |
| Max Barbakow          | Palm Spring                          |        9 |
| Christopher Nolan     | Dunkirk                              |       10 |
| Gregory Hoblit        | Das perfekte Verbrechen              |       11 |
| Denis Villeneuve      | Dune                                 |       12 |
| Ryûsuke Hamaguchi     | Drive My Car                         |       13 |
| Wes Anderson          | The French Dispatch                  |       14 |
| Simon Stone           | Die Ausgrabung                       |       15 |
| Chad Hartigan         | Little Fish                          |       16 |
| Edgar Wright          | Last Night in Soho                   |       17 |
| John Watts            | Spider-Man No Way Home               |       18 |
| Cary Joji Fukunaga    | James Bond 007 Keine Zeit zu sterben |       19 |
| David Lowery          | The Green Knight                     |       20 |
| Jane Campion          | The Power of the Dog                 |       21 |
| Steven Spielberg      | West Side Story                      |       22 |
+-----------------------+--------------------------------------+----------+