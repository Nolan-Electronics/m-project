It is critical to follow this given structure to be able to use all functionalities once exported and installed:


dir - programm_directory
	exe - reset_db.py
	dir - binaries
	dir - images
		dir - icons
		dir - thumbnails