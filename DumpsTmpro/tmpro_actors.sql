CREATE DATABASE  IF NOT EXISTS `tmpro` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `tmpro`;
-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: tmpro
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actors`
--

DROP TABLE IF EXISTS `actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actors` (
  `actor_id` int NOT NULL AUTO_INCREMENT,
  `actor_name` varchar(32) DEFAULT NULL,
  `actor_birthdate` date DEFAULT NULL,
  `actor_country` char(2) DEFAULT NULL,
  PRIMARY KEY (`actor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actors`
--

LOCK TABLES `actors` WRITE;
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;
INSERT INTO `actors` VALUES (1,'Richard Ayoade','1977-05-23','UK'),(2,'Mads Mikkelsen','1965-11-22','DK'),(3,'Antony Hopkins','1937-12-31','UK'),(4,'Robert Pattinson','1986-05-13','UK'),(5,'Carey Mulligan','1985-05-28','UK'),(6,'Elisabeth Moss','1982-07-24','US'),(7,'Frances McDormand','1957-06-23','US'),(8,'Andy Samberg','1978-08-18','US'),(9,'Fionn Whitehead','1997-07-18','UK'),(10,'David Soul','1943-08-28','US'),(11,'Magnus Millang','1981-07-20','DK'),(12,'Olivia Colman','1974-01-30','UK'),(13,'Elizabeth Debicki','1990-08-24','FR'),(14,'Bo Burnham','1990-08-21','US'),(15,'Christian Hornhof','1970-02-01','DE'),(16,'David Strathairn','1949-01-26','US'),(17,'Christin Milioti','1985-08-16','US'),(18,'Cillian Murphy','1976-05-25','IR'),(19,'Ryan Cosling','1980-11-12','CA'),(20,'Aldis hodge','1986-09-20','US'),(21,'Ansel Elgort','1994-03-14','US'),(22,'Rachel Zegler','2001-05-03','US'),(23,'Ariana DeBose','1991-01-25','US'),(24,'Benedict Cumberbatch','1976-07-19','UK'),(25,'Genenieve Lemon','1958-04-21','AU'),(26,'Jesse Plemons','1988-03-02','US'),(27,'Anais Rizzo','1996-01-01','IT'),(28,'Joe Anderson','1982-03-26','EN'),(29,'Dev Patel','1990-04-23','EN'),(30,'Daniel Craig','1968-03-02','EN'),(31,'Lea Seydou','1985-07-01','FR'),(32,'Rami Malek','1981-05-12','US'),(33,'Tom Holland','1996-06-01','EN'),(34,'Zendaya','1996-09-01','US'),(35,'Thomasin McKenzie','2000-07-26','NZ'),(36,'Aimee Cassettari','1970-01-01','US'),(37,'Rita Tushingham','1942-03-14','EN'),(38,'Olivia Cooke','1993-12-27','EN'),(39,'Jack O\'Connell','1990-08-01','EN'),(40,'Soko','1985-10-26','FR'),(41,'Ralph Fiennes','1962-12-22','EN'),(42,'Lily James','1989-04-01','EN'),(43,'Benicio del Toro','1967-02-19','PR'),(44,'Adrien Brody','1973-04-14','US'),(45,'Tilda Swinton','1960-11-05','EN'),(46,'Hidetoshi Nishijima','1971-03-29','JP'),(47,'Toko Miura','1996-10-20','JP'),(48,'Reika Kirishima','1972-08-05','JP'),(49,'Timothee Chalamet','1995-12-27','US'),(50,'Rebecca Ferguson','1983-10-19','SE'),(51,'Oscar Isaac','1979-03-09','GT');
/*!40000 ALTER TABLE `actors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-25 11:45:26
