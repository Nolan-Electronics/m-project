CREATE DATABASE  IF NOT EXISTS `tmpro` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `tmpro`;
-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: tmpro
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `directors`
--

DROP TABLE IF EXISTS `directors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `directors` (
  `director_id` int NOT NULL AUTO_INCREMENT,
  `director_name` varchar(32) DEFAULT NULL,
  `director_birthdate` date DEFAULT NULL,
  `director_country` char(2) DEFAULT NULL,
  PRIMARY KEY (`director_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directors`
--

LOCK TABLES `directors` WRITE;
/*!40000 ALTER TABLE `directors` DISABLE KEYS */;
INSERT INTO `directors` VALUES (1,'Peter Docter','1986-10-09','US'),(2,'Thomas Vinterberg','1969-05-19','DK'),(3,'Florian Zeller','1979-06-28','FR'),(4,'Christopher Nolan','1970-07-30','UK'),(5,'Emerald Fennell','1985-10-01','UK'),(6,'Anders Thomans Jensen','1972-04-06','DK'),(7,'Leigh Whannell','1977-01-17','AU'),(8,'Chloe Zhano','1982-03-31','US'),(9,'Max Barbakow','1982-03-04','US'),(10,'Gregory Hoblit','1944-11-27','US'),(11,'Matt Reeves','1966-04-27','US'),(12,'James Cameron','1954-08-16','KH'),(13,'Rian Johnson','1973-12-17','US'),(14,'Matthew Vaughn','1971-03-07','UK'),(15,'Wes Anderson','1969-05-01','US'),(16,'Martin Scorsese','1942-11-17','US'),(17,'Jamie Foxx','1967-12-13','US'),(18,'Denis Villeneuve','1967-10-03','KH'),(19,'Ryûsuke Hamaguchi','1978-12-16','JP'),(20,'Simon Stone','1984-08-19','US'),(21,'Chad Hartigan','1982-08-31','CY'),(22,'Edgar Wright','1974-04-18','UK'),(23,'John Watts','1954-12-27','US'),(24,'Cary Joji Fukunaga','1977-07-10','US'),(25,'David Lowery','1980-12-26','US'),(26,'Jane Campion','1954-04-30','NZ'),(27,'Steven Spielberg','1946-12-18','US'),(28,'Paul Thomas Anderson','1970-06-26','US'),(29,'Guillermo del Toro','1964-10-09','MX'),(30,'Nikole Beckwith','1970-01-01','US'),(31,'Sian Heder','1977-06-23','US'),(32,'Ridley Scott','1937-11-30','UK'),(33,'Ilya Naishuller','1937-11-30','RU'),(34,'Michael Sarnoski','1970-01-01','US');
/*!40000 ALTER TABLE `directors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-25 11:45:26
