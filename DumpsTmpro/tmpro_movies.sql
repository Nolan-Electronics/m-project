CREATE DATABASE  IF NOT EXISTS `tmpro` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `tmpro`;
-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: tmpro
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movies` (
  `movie_id` int NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(255) DEFAULT NULL,
  `movie_description` varchar(255) DEFAULT NULL,
  `movie_directorid` int DEFAULT NULL,
  `movie_length` int DEFAULT NULL,
  `movie_rating` double DEFAULT NULL,
  `movie_releasedate` date DEFAULT NULL,
  PRIMARY KEY (`movie_id`),
  KEY `movie_directorid` (`movie_directorid`),
  CONSTRAINT `movies_ibfk_1` FOREIGN KEY (`movie_directorid`) REFERENCES `directors` (`director_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies`
--

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` VALUES (1,'Soul','Soul ist eine Reise von New York bis in kosmische Sphären und schickt zwei Seelen auf die Suche nach dem Sinn des Lebens.',1,100,7.6,'2020-10-11'),(2,'Der Rausch','Der Rausch ist eine oscarprämierte dänische Komödie mit Mads Mikkelsen, in der vier Lehrer ein Experiment starten, bei dem sie ihren Alkoholpegel auf einem konstanten Level halten, um der Welt offener zu begegnen.',2,117,7.3,'2020-04-28'),(3,'The Father','The Father spielt der oscarprämierte Anthony Hopkins einen zunehmend unter Demenz leidenden Vater, der sich nicht von seiner Tochter helfen lassen will.',3,97,7.9,'2020-01-20'),(4,'Tenet','In Christopher Nolans Thriller Tenet wird John David Washington in eine Welt der Spionage hineingezogen, die er sich so niemals hätte ausmalen können, da die Regeln der Zeit hier scheinbar anders funktionieren.',4,150,7,'2020-08-12'),(5,'Promising young woman','Im Thriller Promising Young Woman ergreift die von einer Tragödie in ihrer Vergangenheit traumatisierte Carey Mulligan ihre Chance, Rache an allen zu nehmen, die Frauen nicht genug Respekt entgegenbringen.',5,113,7.3,'2020-05-12'),(6,'Helden der Warscheinlichkeit','In der dänischen schwarzen Komödie Helden der Wahrscheinlichkeit schwört Mads Mikkelsen, Rache an denen zu üben, die seine Frau mit einer Zug-Entgleisung getötet haben.',6,116,7.6,'2020-11-19'),(7,'Der unsichtbare','Der Unsichtbare verfolgt in diesem Psychohorrorfilm Elisabeth Moss, als sie glaubt, sich endlich von ihrem gewalttätigen Ehemann losgesagt zu haben.',7,124,6.9,'2020-02-16'),(8,'Nomadland','Im Oscar-Gewinnerfilm 2021 Nomadland lebt Frances McDormand in ihrem Auto und reist als moderne Nomadin durch den Westen der USA, arbeitet an unterschiedlichen Orten und trifft Menschen, die in einer ähnlichen Situation sind wie sie.',8,108,7.6,'2020-07-01'),(9,'Palm Spring','Als der sorgenfreie Nyles und die widerwillige Brautjungfer Sarah einander in Palm Springs bei einer Hochzeit zufällig begegnen, wird es kompliziert, da sie den Gefühlen, die sie für einander haben, nicht entkommen können.',9,100,7.2,'2020-10-11'),(10,'Dunkirk','Alliierte Soldaten aus Belgien, dem britischen Empire, Kanada und Frankreich sind vom deutschen Heer eingekesselt und werden während einer erbitterten Schlacht im Zweiten Weltkrieg evakuiert.',4,106,7.8,'1017-05-19'),(11,'Das perfekte Verbrechen','Ein Anwalt, der beabsichtigt, die Karriereleiter zum Erfolg zu erklimmen, findet einen unwahrscheinlichen Gegner in einem manipulierten Verbrecher, den er zu verfolgen versucht.',10,113,7.2,'2007-04-20'),(12,'Dune','Eine Adelsfamilie wird in einen Krieg um die Kontrolle über das wertvollste Gut der Galaxis verwickelt, während ihr Erbe von Visionen einer dunklen Zukunft geplagt wird.',18,155,8,'2021-10-22'),(13,'Drive My Car','Nishijima Hidetoshi ist als Bühnenschauspieler und Regisseur glücklich mit seiner Dramatikerin verheiratet. Dann verschwindet eines Tages die Frau.',19,179,7.6,'2021-12-22'),(14,'The French Dispatch','The French Dispatch erweckt eine Sammlung von Geschichten aus der letzten Ausgabe eines amerikanischen Magazins, das in einer fiktiven französischen Stadt des 20. Jahrhunderts erscheint, zum Leben.',15,107,7.2,'2021-10-21'),(15,'Die Ausgrabung','Während der Zweite Weltkrieg auszubrechen droht, heuert eine vermögende Witwe einen Hobbyarchäologen an, um die Hügel auf ihrem Landsitz auszugraben.',20,112,7.1,'2021-01-14'),(16,'Little Fish','Ein Paar kämpft darum, seine Beziehung zusammenzuhalten, während sich ein Virus des Gedächtnisverlusts ausbreitet und droht, die Geschichte ihrer Liebe und ihres Werbens auszulöschen.',21,101,6.8,'2021-02-05'),(17,'Last Night in Soho','Geplanter Horror-Thriller von Edgar Wright, bei dem eine Frau aus London im Mittelpunkt stehen und der sich an <<Wenn die Gondeln Trauer tragen>> und Roman Polanskis <<Ekel>> orientieren soll.',22,116,7.1,'2021-09-04'),(18,'Spider-Man No Way Home','Da Spider-Mans Identität nun aufgedeckt ist, bittet Peter Doktor Strange um Hilfe. Als ein Zauber schiefgeht, tauchen gefährliche Feinde aus anderen Welten auf und zwingen Peter zu entdecken, was es wirklich bedeutet, Spider-Man zu sein.',23,148,8.3,'2021-12-17'),(19,'James Bond 007 Keine Zeit zu sterben','Eigentlich wollte James Bond mit seiner großen Liebe Madeleine Swann seinen Ruhestand genießen und endlich ein normales Leben führen. Doch zurück in sein altes Leben holt ihn sein alter Freund, CIA-Agent Felix Leiter.',24,163,7.3,'2021-09-30'),(20,'The Green Knight','Eine Fantasy-Nacherzählung der mittelalterlichen Geschichte von Sir Gawain und dem Grünen Ritter.',25,130,6.6,'2021-07-30'),(21,'The Power of the Dog','Der charismatische Rancher Phil Burbank löst in seiner Umgebung Furcht und Ehrfurcht aus. Als sein Bruder eine neue Frau und ihren Sohn nach Hause bringt, quält Phil sie, bis er sich seiner eigenen Möglichkeit der Liebe ausgesetzt sieht.',26,126,6.8,'2021-09-02'),(22,'West Side Story','Eine Adaption des Musicals von 1957, West Side Story, untersucht verbotene Liebe und die Rivalität zwischen den Jets und den Sharks, zwei Straßenbanden unterschiedlicher ethnischer Herkunft.',27,156,7.2,'2021-12-10');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-25 11:45:27
