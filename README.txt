This software is used to connect and work with a MySQL-Database. MySQL-Server must be installed and running on localhost.
The Database can be set up with the reset_db.py script.
It will automaticly create the schema and insert some movies/informations for testing and debuging purposes.

---
For the python reset_db.py script:

The mysql connector module is required for this script, it can be installed from the requirements.txt:
   pip install -r requirements.txt
or:
   python -m pip install mysql-connector-python
Additionally the Pillow module is used to display or save a preview for a thumbnail (for debuging-purposes):
   python -m pip install Pillow

It is critical to follow this given structure to be able to use all functionalities once exported and installed:

Project/Program working directory
  ->reset_db.py (Executable)
  ->images (Dir)
      ->thumbnails (Dir)
          ->All thumbnails with correct name.
---

---

Working with the C# Software:

The C# Project is used as a GUI for communicating with the database. It will require a login to get access, the user has to have all privileges over the schema "tmpro".
This way, the C# GUI has full acces to the database, so caution while working in it is expected.
- Deletions made in the C# Project affect the database imidiatly and can not be reversed.
- Actors and directors can not be deleted from the C# GUI, even if the movie used to insert them gets removed.
- Thumbnails should have a size of 878x585px and have to be in scale 3:2 in order to be displayed correctly.

---

This software was developed as a practice and is free to use, modify and deploy.