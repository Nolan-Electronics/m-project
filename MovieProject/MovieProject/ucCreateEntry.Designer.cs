﻿namespace MovieProject
{
    partial class ucCreateEntry
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExit = new MovieProject.ncButton();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblRating = new System.Windows.Forms.Label();
            this.pnlDirectorinfo = new System.Windows.Forms.Panel();
            this.pnlDirectorinfoToggle = new System.Windows.Forms.Panel();
            this.lblDirectorCountry = new System.Windows.Forms.Label();
            this.lblDirectorBirthdate = new System.Windows.Forms.Label();
            this.lblDirectorName = new System.Windows.Forms.Label();
            this.picboxDirectorImg = new System.Windows.Forms.PictureBox();
            this.lboxActors = new System.Windows.Forms.ListBox();
            this.btnSubmit = new MovieProject.ncButton();
            this.btnAddPerson = new MovieProject.ncButton();
            this.txtboxName = new System.Windows.Forms.TextBox();
            this.txtboxRating = new System.Windows.Forms.TextBox();
            this.txtboxLength = new System.Windows.Forms.TextBox();
            this.lblReleasedate = new System.Windows.Forms.Label();
            this.btnOverwrite = new MovieProject.ncButton();
            this.txtboxDesc = new System.Windows.Forms.TextBox();
            this.ofdThumbnail = new System.Windows.Forms.OpenFileDialog();
            this.picboxThumbnail = new System.Windows.Forms.PictureBox();
            this.dtpReleasedate = new MovieProject.NCDatePicker();
            this.pnlDirectorinfo.SuspendLayout();
            this.pnlDirectorinfoToggle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxDirectorImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picboxThumbnail)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundColor = System.Drawing.Color.White;
            this.btnExit.BorderColor = System.Drawing.Color.MediumPurple;
            this.btnExit.BorderRadius = 10;
            this.btnExit.BorderSize = 1;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.Image = global::MovieProject.Images.ReturnIcon_26_black;
            this.btnExit.Location = new System.Drawing.Point(50, 32);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 53);
            this.btnExit.TabIndex = 44;
            this.btnExit.TextColor = System.Drawing.Color.Black;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblLength
            // 
            this.lblLength.BackColor = System.Drawing.Color.White;
            this.lblLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLength.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblLength.Location = new System.Drawing.Point(50, 267);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(105, 30);
            this.lblLength.TabIndex = 42;
            this.lblLength.Text = "Length (min)";
            this.lblLength.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.White;
            this.lblName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblName.Location = new System.Drawing.Point(50, 106);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(105, 30);
            this.lblName.TabIndex = 39;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRating
            // 
            this.lblRating.BackColor = System.Drawing.Color.White;
            this.lblRating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRating.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblRating.Location = new System.Drawing.Point(50, 165);
            this.lblRating.Name = "lblRating";
            this.lblRating.Size = new System.Drawing.Size(105, 30);
            this.lblRating.TabIndex = 37;
            this.lblRating.Text = "Rating";
            this.lblRating.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlDirectorinfo
            // 
            this.pnlDirectorinfo.BackColor = System.Drawing.SystemColors.Control;
            this.pnlDirectorinfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDirectorinfo.Controls.Add(this.pnlDirectorinfoToggle);
            this.pnlDirectorinfo.Location = new System.Drawing.Point(832, 50);
            this.pnlDirectorinfo.Name = "pnlDirectorinfo";
            this.pnlDirectorinfo.Size = new System.Drawing.Size(180, 249);
            this.pnlDirectorinfo.TabIndex = 45;
            // 
            // pnlDirectorinfoToggle
            // 
            this.pnlDirectorinfoToggle.BackColor = System.Drawing.SystemColors.Control;
            this.pnlDirectorinfoToggle.Controls.Add(this.lblDirectorCountry);
            this.pnlDirectorinfoToggle.Controls.Add(this.lblDirectorBirthdate);
            this.pnlDirectorinfoToggle.Controls.Add(this.lblDirectorName);
            this.pnlDirectorinfoToggle.Controls.Add(this.picboxDirectorImg);
            this.pnlDirectorinfoToggle.Location = new System.Drawing.Point(3, 3);
            this.pnlDirectorinfoToggle.Name = "pnlDirectorinfoToggle";
            this.pnlDirectorinfoToggle.Size = new System.Drawing.Size(172, 241);
            this.pnlDirectorinfoToggle.TabIndex = 46;
            // 
            // lblDirectorCountry
            // 
            this.lblDirectorCountry.BackColor = System.Drawing.Color.White;
            this.lblDirectorCountry.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDirectorCountry.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDirectorCountry.Location = new System.Drawing.Point(3, 211);
            this.lblDirectorCountry.Name = "lblDirectorCountry";
            this.lblDirectorCountry.Size = new System.Drawing.Size(166, 23);
            this.lblDirectorCountry.TabIndex = 3;
            this.lblDirectorCountry.Text = "Origin";
            this.lblDirectorCountry.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDirectorBirthdate
            // 
            this.lblDirectorBirthdate.BackColor = System.Drawing.Color.White;
            this.lblDirectorBirthdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDirectorBirthdate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDirectorBirthdate.Location = new System.Drawing.Point(3, 185);
            this.lblDirectorBirthdate.Name = "lblDirectorBirthdate";
            this.lblDirectorBirthdate.Size = new System.Drawing.Size(166, 23);
            this.lblDirectorBirthdate.TabIndex = 2;
            this.lblDirectorBirthdate.Text = "Birthdate";
            this.lblDirectorBirthdate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblDirectorName
            // 
            this.lblDirectorName.BackColor = System.Drawing.Color.White;
            this.lblDirectorName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDirectorName.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDirectorName.Location = new System.Drawing.Point(3, 158);
            this.lblDirectorName.Name = "lblDirectorName";
            this.lblDirectorName.Size = new System.Drawing.Size(166, 23);
            this.lblDirectorName.TabIndex = 1;
            this.lblDirectorName.Text = "Name";
            this.lblDirectorName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // picboxDirectorImg
            // 
            this.picboxDirectorImg.BackColor = System.Drawing.Color.White;
            this.picboxDirectorImg.Image = global::MovieProject.Images.PersonIcon_75_black;
            this.picboxDirectorImg.Location = new System.Drawing.Point(3, 3);
            this.picboxDirectorImg.Name = "picboxDirectorImg";
            this.picboxDirectorImg.Size = new System.Drawing.Size(166, 150);
            this.picboxDirectorImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picboxDirectorImg.TabIndex = 0;
            this.picboxDirectorImg.TabStop = false;
            // 
            // lboxActors
            // 
            this.lboxActors.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lboxActors.FormattingEnabled = true;
            this.lboxActors.ItemHeight = 20;
            this.lboxActors.Location = new System.Drawing.Point(660, 318);
            this.lboxActors.Name = "lboxActors";
            this.lboxActors.Size = new System.Drawing.Size(246, 184);
            this.lboxActors.TabIndex = 48;
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.White;
            this.btnSubmit.BackgroundColor = System.Drawing.Color.White;
            this.btnSubmit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(215)))), ((int)(((byte)(255)))));
            this.btnSubmit.BorderRadius = 10;
            this.btnSubmit.BorderSize = 1;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSubmit.ForeColor = System.Drawing.Color.Black;
            this.btnSubmit.Location = new System.Drawing.Point(660, 522);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(352, 40);
            this.btnSubmit.TabIndex = 49;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.TextColor = System.Drawing.Color.Black;
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnAddPerson
            // 
            this.btnAddPerson.BackColor = System.Drawing.Color.White;
            this.btnAddPerson.BackgroundColor = System.Drawing.Color.White;
            this.btnAddPerson.BorderColor = System.Drawing.Color.Lime;
            this.btnAddPerson.BorderRadius = 10;
            this.btnAddPerson.BorderSize = 1;
            this.btnAddPerson.FlatAppearance.BorderSize = 0;
            this.btnAddPerson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddPerson.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAddPerson.ForeColor = System.Drawing.Color.Black;
            this.btnAddPerson.Location = new System.Drawing.Point(912, 350);
            this.btnAddPerson.Name = "btnAddPerson";
            this.btnAddPerson.Size = new System.Drawing.Size(100, 40);
            this.btnAddPerson.TabIndex = 50;
            this.btnAddPerson.Text = "Add";
            this.btnAddPerson.TextColor = System.Drawing.Color.Black;
            this.btnAddPerson.UseVisualStyleBackColor = false;
            this.btnAddPerson.Click += new System.EventHandler(this.btnAddPerson_Click);
            // 
            // txtboxName
            // 
            this.txtboxName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxName.Location = new System.Drawing.Point(181, 108);
            this.txtboxName.Name = "txtboxName";
            this.txtboxName.Size = new System.Drawing.Size(434, 29);
            this.txtboxName.TabIndex = 53;
            // 
            // txtboxRating
            // 
            this.txtboxRating.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxRating.Location = new System.Drawing.Point(181, 166);
            this.txtboxRating.Name = "txtboxRating";
            this.txtboxRating.Size = new System.Drawing.Size(434, 29);
            this.txtboxRating.TabIndex = 54;
            // 
            // txtboxLength
            // 
            this.txtboxLength.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxLength.Location = new System.Drawing.Point(181, 266);
            this.txtboxLength.Name = "txtboxLength";
            this.txtboxLength.Size = new System.Drawing.Size(434, 29);
            this.txtboxLength.TabIndex = 57;
            // 
            // lblReleasedate
            // 
            this.lblReleasedate.BackColor = System.Drawing.Color.White;
            this.lblReleasedate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblReleasedate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblReleasedate.Location = new System.Drawing.Point(50, 216);
            this.lblReleasedate.Name = "lblReleasedate";
            this.lblReleasedate.Size = new System.Drawing.Size(105, 30);
            this.lblReleasedate.TabIndex = 58;
            this.lblReleasedate.Text = "Release Date";
            this.lblReleasedate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnOverwrite
            // 
            this.btnOverwrite.BackColor = System.Drawing.Color.White;
            this.btnOverwrite.BackgroundColor = System.Drawing.Color.White;
            this.btnOverwrite.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.btnOverwrite.BorderRadius = 10;
            this.btnOverwrite.BorderSize = 1;
            this.btnOverwrite.Enabled = false;
            this.btnOverwrite.FlatAppearance.BorderSize = 0;
            this.btnOverwrite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOverwrite.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnOverwrite.ForeColor = System.Drawing.Color.Black;
            this.btnOverwrite.Location = new System.Drawing.Point(912, 425);
            this.btnOverwrite.Name = "btnOverwrite";
            this.btnOverwrite.Size = new System.Drawing.Size(100, 40);
            this.btnOverwrite.TabIndex = 60;
            this.btnOverwrite.Text = "Overwrite";
            this.btnOverwrite.TextColor = System.Drawing.Color.Black;
            this.btnOverwrite.UseVisualStyleBackColor = false;
            this.btnOverwrite.Visible = false;
            this.btnOverwrite.Click += new System.EventHandler(this.btnOverwrite_Click);
            // 
            // txtboxDesc
            // 
            this.txtboxDesc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxDesc.Location = new System.Drawing.Point(50, 318);
            this.txtboxDesc.Multiline = true;
            this.txtboxDesc.Name = "txtboxDesc";
            this.txtboxDesc.Size = new System.Drawing.Size(565, 184);
            this.txtboxDesc.TabIndex = 61;
            // 
            // ofdThumbnail
            // 
            this.ofdThumbnail.FileName = "openFileDialog1";
            // 
            // picboxThumbnail
            // 
            this.picboxThumbnail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picboxThumbnail.Image = global::MovieProject.Images.OpenfileIcon_60_black;
            this.picboxThumbnail.Location = new System.Drawing.Point(660, 50);
            this.picboxThumbnail.Name = "picboxThumbnail";
            this.picboxThumbnail.Size = new System.Drawing.Size(166, 249);
            this.picboxThumbnail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picboxThumbnail.TabIndex = 65;
            this.picboxThumbnail.TabStop = false;
            this.picboxThumbnail.Click += new System.EventHandler(this.picboxThumbnail_Click);
            // 
            // dtpReleasedate
            // 
            this.dtpReleasedate.BorderColor = System.Drawing.Color.Black;
            this.dtpReleasedate.BorderSize = 1;
            this.dtpReleasedate.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtpReleasedate.Location = new System.Drawing.Point(181, 213);
            this.dtpReleasedate.MinimumSize = new System.Drawing.Size(0, 35);
            this.dtpReleasedate.Name = "dtpReleasedate";
            this.dtpReleasedate.Size = new System.Drawing.Size(434, 35);
            this.dtpReleasedate.SkinColor = System.Drawing.Color.White;
            this.dtpReleasedate.TabIndex = 66;
            this.dtpReleasedate.TextColor = System.Drawing.Color.Black;
            // 
            // ucCreateEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dtpReleasedate);
            this.Controls.Add(this.picboxThumbnail);
            this.Controls.Add(this.txtboxDesc);
            this.Controls.Add(this.btnOverwrite);
            this.Controls.Add(this.lblReleasedate);
            this.Controls.Add(this.txtboxLength);
            this.Controls.Add(this.txtboxRating);
            this.Controls.Add(this.txtboxName);
            this.Controls.Add(this.btnAddPerson);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.lboxActors);
            this.Controls.Add(this.pnlDirectorinfo);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblRating);
            this.Name = "ucCreateEntry";
            this.Size = new System.Drawing.Size(1070, 628);
            this.VisibleChanged += new System.EventHandler(this.ucCreateEntry_VisibleChanged);
            this.pnlDirectorinfo.ResumeLayout(false);
            this.pnlDirectorinfoToggle.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picboxDirectorImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picboxThumbnail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ncButton btnExit;
        private Label lblLength;
        private Label lblName;
        private Label lblRating;
        private Panel pnlDirectorinfo;
        private Panel pnlDirectorinfoToggle;
        private Label lblDirectorCountry;
        private Label lblDirectorBirthdate;
        private Label lblDirectorName;
        private PictureBox picboxDirectorImg;
        private ListBox lboxActors;
        private ncButton btnSubmit;
        private ncButton btnAddPerson;
        private TextBox txtboxName;
        private TextBox txtboxRating;
        private TextBox txtboxLength;
        private Label lblReleasedate;
        private ncButton btnOverwrite;
        private TextBox txtboxDesc;
        private OpenFileDialog ofdThumbnail;
        private PictureBox picboxThumbnail;
        private NCDatePicker dtpReleasedate;
    }
}
