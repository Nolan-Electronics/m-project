﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace MovieProject
{
    public partial class ucAddPerson : UserControl
    {
        int PRIMARY_KEY = 0;
        bool shownAuthOverwriteMB = false;

        public ucAddPerson()
        {
            InitializeComponent();
            comboxPersons.SelectedItem = "Directors";
        }
        private void btnAddPerson_Click(object sender, EventArgs e)
        {
            if(comboxPersons.SelectedItem.ToString() == "Directors")
            {
                if(Global.addPerson_Director != null && !shownAuthOverwriteMB) 
                {
                    MessageBox.Show("Only one director can be specified. Adding a new Director will overwrite the old one."); 
                    shownAuthOverwriteMB = true;
                } 
                Person person_to_add = new Person(txtboxName.Text, dtpBirthdate.Value, txtboxOrigin.Text);
                if(validPerson(person_to_add))
                {
                    Global.addPerson_Director = person_to_add;
                }
            }
            else
            {
                Person person_to_add = new Person(PRIMARY_KEY, txtboxName.Text, dtpBirthdate.Value, txtboxOrigin.Text);
                if (validPerson(person_to_add))
                {
                    Global.addPerson_Actors.Add(person_to_add);
                }
                PRIMARY_KEY++;
            }
            fillPersonListbox();
        }
        private void fillPersonListbox()
        {
            lboxPersons.Items.Clear();
            
            if(isActorsSelected())
            {
                if (Global.addPerson_Actors != null)
                {
                    foreach (Person actor in Global.addPerson_Actors)
                    {
                        lboxPersons.Items.Add($"{actor.Id}, {actor.Name}, {actor.BirthdateDT.ToString("dd.MM.yyyy")}, {actor.Country}");
                    }
                }
            }
            else
            {
                if (Global.addPerson_Director != null)
                {
                    lboxPersons.Items.Add($"{Global.addPerson_Director.Name}, {Global.addPerson_Director.BirthdateDT.ToString("dd.MM.yyyy")}, {Global.addPerson_Director.Country}");
                }
            }
        }
        private void btnRemovePerson_Click(object sender, EventArgs e)
        {
            if (isActorsSelected())
            {
                int item = lboxPersons.SelectedIndex;
                lboxPersons.Items.RemoveAt(item);
                Global.addPerson_Actors.RemoveAt(item);
            }
            else
            {
                Global.addPerson_Director = null;
            }
            fillPersonListbox();
        }

        private bool isActorsSelected()
        {
            if (comboxPersons.SelectedItem.ToString() == "Actors")
            {
                return true;
            }
            return false;
        }


        /// <summary>
        /// Full procces into the database, expecting the movie has already be created
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            void clearFields()
            {
                txtboxName.Text = "";
                txtboxOrigin.Text = "";
                dtpBirthdate.Value = DateTime.Now;
            }
            if(Global.addPerson_Director != null && Global.addPerson_Actors.Any())
            {
                clearFields();
                Global.createEntryToFront();
                return;
            }
            Global.displayErrorMessage("The movie cast is not complete and cannot be processed (At least one director and actor is required).");
        }

        private bool validPerson(Person person)
        {
            if(person.Name.Length < 0 || person.Name.Length > 30)
            {
                Global.displayErrorMessage("The name for this person exceeds the limit of 30 characters.");
                return false;
            }
            if(person.Country.Length != 2)
            {
                Global.displayErrorMessage("The format of the persons origin is wrong, please use the ISO3166-APLHA-2 standard (Ex: US, DE, FR, CH, UK)");
                return false;
            }
            return true;
        }
        
        private void closeUsercontrol()
        {
            Global.createEntryToFront();
            Global.addPerson_Actors = new List<Person>();
            Global.addPerson_Director = null;
            return;
        }

        private void ucAddPerson_Load(object sender, EventArgs e)
        {
            comboxPersons.OnSelectedIndexChanged += new System.EventHandler(comboxPersons_SelectedIndexChanged);
        }

        private void setLboxBackcol()
        {
            if (Layoutinfo.darkmode)
            {
                lboxPersons.BackColor = ProjectColors.darkmode_fillcolor;
            }
            else
            {
                lboxPersons.BackColor = ProjectColors.lightmode_fillcolor;
            }
        }
        private void comboxPersons_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillPersonListbox();
            if (isActorsSelected()) { lboxPersons.Size = new Size(655, 109); }
            else { lboxPersons.Size = new Size(633, 46); }
            //setLboxBackcol(); //Reset the backcolor that now got changed to white for some reason
        }

        void setFormColor(Color color)
        {
            lboxPersons.BackColor = color;
            comboxPersons.BackColor = color;
            btnAddPerson.BackColor = color;
            btnSubmit.BackColor = color;
            btnRemovePerson.BackColor = color;
            lblName.BackColor = color;
            lblBirthdate.BackColor = color;
            lblOrigin.BackColor = color;
            dtpBirthdate.BackColor = color;
            txtboxName.BackColor = color;
            txtboxOrigin.BackColor = color;
            btnSubmit.BackColor = color;
            btnExit.BackColor = color;
            dtpBirthdate.SkinColor = color;
        }
        void setTextColor(Color color)
        {
            lboxPersons.ForeColor = color;
            comboxPersons.ForeColor = color;
            btnAddPerson.ForeColor = color;
            btnRemovePerson.ForeColor = color;
            btnSubmit.ForeColor = color;
            lblName.ForeColor = color;
            lblBirthdate.ForeColor = color;
            lblOrigin.ForeColor = color;
            dtpBirthdate.ForeColor = color;
            txtboxName.ForeColor = color;
            txtboxOrigin.ForeColor = color;
            btnSubmit.ForeColor = color;
            btnExit.ForeColor = color;
            dtpBirthdate.TextColor = color;
            dtpBirthdate.BorderColor = color;
        }

        void setFont(Font font)
        {
            lboxPersons.Font = font;
            comboxPersons.Font = font;
            txtboxName.Font = font;
            txtboxOrigin.Font = font;
            lblBirthdate.Font = font;
            lblName.Font = font;
            lblOrigin.Font = font;
            btnAddPerson.Font = font;
            btnRemovePerson.Font = font;
            btnSubmit.Font = font;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            closeUsercontrol();
        }

        private void ucAddPerson_VisibleChanged(object sender, EventArgs e)
        {
            fillPersonListbox();
            if (Layoutinfo.darkmode)
            {
                BackColor = ProjectColors.darkmode_background;
                setFormColor(ProjectColors.darkmode_fillcolor);
                setTextColor(ProjectColors.darkmode_fontcolor);
                btnExit.Image = Images.ReturnIcon_26_white;
            }
            else
            {
                BackColor = ProjectColors.lightmode_background;
                setFormColor(ProjectColors.lightmode_fillcolor);
                setTextColor(ProjectColors.lightmode_fontcolor);
                //btnExit.FlatAppearance.MouseOverBackColor = ProjectColors.lightmode_btnhover;
                //btnRemovePerson.FlatAppearance.MouseOverBackColor = ProjectColors.lightmode_btnhover;
                //btnAddPerson.FlatAppearance.MouseOverBackColor = ProjectColors.lightmode_btnhover;
                //btnSubmit.FlatAppearance.MouseOverBackColor = ProjectColors.lightmode_btnhover;
                btnExit.Image = Images.ReturnIcon_26_black;
            }
            if (Layoutinfo.font != null) { setFont(Layoutinfo.font); }
            btnExit.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderReturnPurple;
            btnRemovePerson.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderRed;
            btnAddPerson.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderGreenlighter;
            btnSubmit.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderSubmitBlue;
        }
    }
}
