﻿namespace MovieProject
{
    partial class ucDisplaymovie
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblID = new System.Windows.Forms.Label();
            this.picboxThumbnail = new System.Windows.Forms.PictureBox();
            this.lblIdContent = new System.Windows.Forms.Label();
            this.lblActorsContent = new System.Windows.Forms.Label();
            this.lblActors = new System.Windows.Forms.Label();
            this.lblDirectorContent = new System.Windows.Forms.Label();
            this.lblDirector = new System.Windows.Forms.Label();
            this.lblRatingContent = new System.Windows.Forms.Label();
            this.lblRating = new System.Windows.Forms.Label();
            this.lblNameContent = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDescContent = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblLengthContent = new System.Windows.Forms.Label();
            this.btnExit = new MovieProject.ncButton();
            this.lblReleasedate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picboxThumbnail)).BeginInit();
            this.SuspendLayout();
            // 
            // lblID
            // 
            this.lblID.BackColor = System.Drawing.Color.White;
            this.lblID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblID.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblID.Location = new System.Drawing.Point(50, 114);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(104, 30);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID:";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // picboxThumbnail
            // 
            this.picboxThumbnail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picboxThumbnail.Location = new System.Drawing.Point(660, 50);
            this.picboxThumbnail.Name = "picboxThumbnail";
            this.picboxThumbnail.Size = new System.Drawing.Size(352, 528);
            this.picboxThumbnail.TabIndex = 1;
            this.picboxThumbnail.TabStop = false;
            // 
            // lblIdContent
            // 
            this.lblIdContent.BackColor = System.Drawing.Color.White;
            this.lblIdContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIdContent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblIdContent.Location = new System.Drawing.Point(181, 114);
            this.lblIdContent.Name = "lblIdContent";
            this.lblIdContent.Size = new System.Drawing.Size(433, 30);
            this.lblIdContent.TabIndex = 15;
            this.lblIdContent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblActorsContent
            // 
            this.lblActorsContent.BackColor = System.Drawing.Color.White;
            this.lblActorsContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblActorsContent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblActorsContent.Location = new System.Drawing.Point(181, 318);
            this.lblActorsContent.Name = "lblActorsContent";
            this.lblActorsContent.Size = new System.Drawing.Size(433, 30);
            this.lblActorsContent.TabIndex = 17;
            this.lblActorsContent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblActors
            // 
            this.lblActors.BackColor = System.Drawing.Color.White;
            this.lblActors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblActors.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblActors.Location = new System.Drawing.Point(50, 318);
            this.lblActors.Name = "lblActors";
            this.lblActors.Size = new System.Drawing.Size(104, 30);
            this.lblActors.TabIndex = 16;
            this.lblActors.Text = "Actors";
            this.lblActors.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDirectorContent
            // 
            this.lblDirectorContent.BackColor = System.Drawing.Color.White;
            this.lblDirectorContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDirectorContent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDirectorContent.Location = new System.Drawing.Point(181, 267);
            this.lblDirectorContent.Name = "lblDirectorContent";
            this.lblDirectorContent.Size = new System.Drawing.Size(433, 30);
            this.lblDirectorContent.TabIndex = 19;
            this.lblDirectorContent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDirector
            // 
            this.lblDirector.BackColor = System.Drawing.Color.White;
            this.lblDirector.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDirector.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDirector.Location = new System.Drawing.Point(50, 267);
            this.lblDirector.Name = "lblDirector";
            this.lblDirector.Size = new System.Drawing.Size(104, 30);
            this.lblDirector.TabIndex = 18;
            this.lblDirector.Text = "Director";
            this.lblDirector.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRatingContent
            // 
            this.lblRatingContent.BackColor = System.Drawing.Color.White;
            this.lblRatingContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRatingContent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblRatingContent.Location = new System.Drawing.Point(181, 216);
            this.lblRatingContent.Name = "lblRatingContent";
            this.lblRatingContent.Size = new System.Drawing.Size(433, 30);
            this.lblRatingContent.TabIndex = 21;
            this.lblRatingContent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRating
            // 
            this.lblRating.BackColor = System.Drawing.Color.White;
            this.lblRating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblRating.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblRating.Location = new System.Drawing.Point(50, 216);
            this.lblRating.Name = "lblRating";
            this.lblRating.Size = new System.Drawing.Size(104, 30);
            this.lblRating.TabIndex = 20;
            this.lblRating.Text = "Rating";
            this.lblRating.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNameContent
            // 
            this.lblNameContent.BackColor = System.Drawing.Color.White;
            this.lblNameContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNameContent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNameContent.Location = new System.Drawing.Point(181, 165);
            this.lblNameContent.Name = "lblNameContent";
            this.lblNameContent.Size = new System.Drawing.Size(433, 30);
            this.lblNameContent.TabIndex = 23;
            this.lblNameContent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.White;
            this.lblName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblName.Location = new System.Drawing.Point(50, 165);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(104, 30);
            this.lblName.TabIndex = 22;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDescContent
            // 
            this.lblDescContent.BackColor = System.Drawing.Color.White;
            this.lblDescContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDescContent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDescContent.Location = new System.Drawing.Point(50, 422);
            this.lblDescContent.Name = "lblDescContent";
            this.lblDescContent.Size = new System.Drawing.Size(564, 156);
            this.lblDescContent.TabIndex = 25;
            this.lblDescContent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLength
            // 
            this.lblLength.BackColor = System.Drawing.Color.White;
            this.lblLength.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLength.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblLength.Location = new System.Drawing.Point(50, 369);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(104, 30);
            this.lblLength.TabIndex = 26;
            this.lblLength.Text = "Length";
            this.lblLength.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLengthContent
            // 
            this.lblLengthContent.BackColor = System.Drawing.Color.White;
            this.lblLengthContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblLengthContent.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblLengthContent.Location = new System.Drawing.Point(181, 369);
            this.lblLengthContent.Name = "lblLengthContent";
            this.lblLengthContent.Size = new System.Drawing.Size(433, 30);
            this.lblLengthContent.TabIndex = 27;
            this.lblLengthContent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundColor = System.Drawing.Color.White;
            this.btnExit.BorderColor = System.Drawing.Color.MediumPurple;
            this.btnExit.BorderRadius = 10;
            this.btnExit.BorderSize = 1;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.Image = global::MovieProject.Images.ReturnIcon_26_black;
            this.btnExit.Location = new System.Drawing.Point(50, 32);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 53);
            this.btnExit.TabIndex = 28;
            this.btnExit.TextColor = System.Drawing.Color.Black;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblReleasedate
            // 
            this.lblReleasedate.BackColor = System.Drawing.Color.White;
            this.lblReleasedate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblReleasedate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblReleasedate.Location = new System.Drawing.Point(394, 49);
            this.lblReleasedate.Name = "lblReleasedate";
            this.lblReleasedate.Size = new System.Drawing.Size(220, 30);
            this.lblReleasedate.TabIndex = 29;
            this.lblReleasedate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ucDisplaymovie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblReleasedate);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblLengthContent);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.lblDescContent);
            this.Controls.Add(this.lblNameContent);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblRatingContent);
            this.Controls.Add(this.lblRating);
            this.Controls.Add(this.lblDirectorContent);
            this.Controls.Add(this.lblDirector);
            this.Controls.Add(this.lblActorsContent);
            this.Controls.Add(this.lblActors);
            this.Controls.Add(this.lblIdContent);
            this.Controls.Add(this.picboxThumbnail);
            this.Controls.Add(this.lblID);
            this.Name = "ucDisplaymovie";
            this.Size = new System.Drawing.Size(1070, 628);
            this.VisibleChanged += new System.EventHandler(this.ucDisplaymovie_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.picboxThumbnail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblID;
        private PictureBox picboxThumbnail;
        private Label lblIdContent;
        private Label lblActorsContent;
        private Label lblActors;
        private Label lblDirectorContent;
        private Label lblDirector;
        private Label lblRatingContent;
        private Label lblRating;
        private Label lblNameContent;
        private Label lblName;
        private Label lblDescContent;
        private Label lblLength;
        private Label lblLengthContent;
        private ncButton btnExit;
        private Label lblReleasedate;
    }
}
