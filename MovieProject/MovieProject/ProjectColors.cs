﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject
{
    static class ProjectColors
    {
        //All used colors are defined here for easier editing.
        public static readonly Color darkmode_background = Color.FromArgb(64, 64, 64);
        public static readonly Color darkmode_fontcolor = Color.White;
        public static readonly Color darkmode_fillcolor = Color.Black;
        public static readonly Color darkmode_btnhover = Color.FromArgb(86, 86, 86);
        public static readonly Color lightmode_background = SystemColors.Control;
        public static readonly Color lightmode_fontcolor = Color.Black;
        public static readonly Color lightmode_fillcolor = SystemColors.Window;
        public static readonly Color lightmode_btnhover = Color.Azure;
        public static readonly Color darkmode_pnlImageConInactive = Color.FromArgb(105, 105, 105);
        public static readonly Color darkmode_pnlImageConActive = Color.FromArgb(135, 135, 135);
        public static readonly Color lightmode_pnlImageConInactive = Color.White;
        public static readonly Color lightmode_pnlImageConActive = Color.AliceBlue;
        public static readonly Color buttonBorderBlue = Color.DeepSkyBlue;
        public static readonly Color buttonBorderGreen = Color.Lime;
        public static readonly Color buttonBorderGreenlighter = Color.FromArgb(103,206,58);
        public static readonly Color buttonBorderRed = Color.FromArgb(255, 120, 120);
        public static readonly Color buttonBorderSubmitBlue = Color.FromArgb(195, 215, 255);
        public static readonly Color buttonBorderReturnPurple = Color.MediumPurple;

        //DeepSkyBlue = Color.FromArgb(59,185,255);
        //Lime = Color.FromArgb(65,163,23);
        //MediumPurple = Color.FromArgb(132,103,215);
        //AliceBlue = Color.FromArgb(240,248,255);
        //Azure = Color.FromArgb(240,255,255);

        //Default configuration:
        //public static readonly Color darkmode_background = Color.FromArgb(64, 64, 64);
        //public static readonly Color darkmode_fontcolor = Color.White;
        //public static readonly Color darkmode_fillcolor = Color.Black;
        //public static readonly Color darkmode_btnhover = Color.FromArgb(86, 86, 86);
        //public static readonly Color lightmode_background = SystemColors.Control;
        //public static readonly Color lightmode_fontcolor = Color.Black;
        //public static readonly Color lightmode_fillcolor = SystemColors.Window;
        //public static readonly Color lightmode_btnhover = Color.Azure;
        //public static readonly Color darkmode_pnlImageConInactive = Color.FromArgb(105, 105, 105);
        //public static readonly Color darkmode_pnlImageConActive = Color.FromArgb(135, 135, 135);
        //public static readonly Color lightmode_pnlImageConInactive = Color.White;
        //public static readonly Color lightmode_pnlImageConActive = Color.AliceBlue;
        //public static readonly Color buttonBorderBlue = Color.DeepSkyBlue;
        //public static readonly Color buttonBorderGreen = Color.Lime;
        //public static readonly Color buttonBorderRed = Color.FromArgb(255, 120, 120);
        //public static readonly Color buttonBorderSubmitBlue = Color.FromArgb(195, 215, 255);
        //public static readonly Color buttonBorderReturnPurple = Color.MediumPurple;
    }
}
