﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject
{
    internal class Movieinfo
    {
        public Movieinfo(string name, double rating, Person director, string description, int length, int id, string date, List<Person> actors)
        {
            Name = name;
            Rating = rating;
            Director = director;
            Description = description;
            Length = length;
            Id = id;
            Actors = actors;
            Date = date;
            Actors = actors;
        }

        public string Name { get; set; }
        public double Rating { get; set; }
        public Person Director { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
        public int Id { get; set; }
        public string Date { get; set; }
        public List<Person> Actors { get; set; }
    }
}
