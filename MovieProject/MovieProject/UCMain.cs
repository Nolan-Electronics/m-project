﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Serilog;

namespace MovieProject
{
    public partial class ucMain : UserControl
    {
        int imagesPerPage = 18; //Default 18, fitts min-width of form.
        List<PictureBox> thumbnails_list = new List<PictureBox>();
        List<Label> pagenumLabels = new List<Label>();
        Label? activePagenumLabel;
        int pagenumLabel_Width = 80;

        public ucMain()
        {
            InitializeComponent();
            Global.comboxTbl = comboxTables;
        }

        private void ucMain_Load(object sender, EventArgs e)
        {
            addEvents();
            Log.Information("Usercontrols sucessfully loaded.");
        }
        private void addEvents()
        {
            comboxTables.OnSelectedIndexChanged += new System.EventHandler(comboxTables_SelectedIndexChanged);
        }

        /// <summary>
        /// Change the table to retreive data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboxTables_SelectedIndexChanged(object? sender, EventArgs e)
        {
            string? selectedItem = comboxTables.SelectedItem.ToString();
            if (selectedItem != null)
            {
                Global.tableSelected = selectedItem;
                calcPages();
                SelectedEntry.selected_entry_ids.Clear();
            }
        }

        /// <summary>
        /// Get informations about a movie.
        /// </summary>
        /// <param name="name"></param>
        private Movieinfo? movieInfo(string name)
        {
            Log.Information("Retrieving infos about a movie.");
            SearchDatabase search = new SearchDatabase(name);

            Movie? movie = null;
            List<Person> actors = new List<Person>();
            Person director = null;
            try
            {
                movie = search.movieByName()[0];
                actors = search.actorByMoviename();
                search = new SearchDatabase(movie.DirectorID);
                director = search.directorByName()[0];
            }
            catch (NullReferenceException e)
            {
                Log.Error("Either movie or actors could not be loaded: {0}", e);
            }

            if (movie != null && actors != null && director != null)
            {
                Movieinfo movieinfo = new Movieinfo(
                        movie.Name,
                        movie.Rating,
                        director,
                        movie.Description,
                        movie.Length,
                        movie.ID,
                        movie.Date,
                        actors
                );
                return movieinfo;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get informations about an director.
        /// </summary>
        /// <param name="name"></param>
        private void directorInfo(string name)
        {
            Log.Information("Retrieving infos about an director.");
            SearchDatabase search = new SearchDatabase(name);
            Person? person = search.directorByName()[0];
            if (person != null)
            {
                //txtboxID.Text = person.Id.ToString();
                //txtboxName.Text = person.Name;
                //txtboxActors.Text = person.Country;
                //txtboxRating.Text = person.Age.ToString();
            }
            else
            {
                Log.Warning("Infos for actor {@actor} could not be loaded.", name);
            }
        }

        /// <summary>
        /// Get informations about an actor.
        /// </summary>
        /// <param name="name"></param>
        private void actorInfo(string name)
        {
            SearchDatabase search = new SearchDatabase(name);
            Person person = search.actorByName()[0];
            //txtboxID.Text = person.Id.ToString();
            //txtboxName.Text = person.Name;
            //txtboxActors.Text = person.Country;
            //txtboxRating.Text = person.Age.ToString();
        }


        /// <summary>
        /// Changes the textbox-layout and adds click-eventlisteners.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            switch (Global.tableSelected)
            {
                case "Movies":
                    Log.Information("Entered create entry mode/uc.");
                    Global.createEntryToFront();
                    break;
                //Add case for actors or directors..
                default:
                    Global.displayErrorMessage("This feature is currently not available.");
                    break;
            }
        }

        /// <summary>
        /// Go into select mode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (SelectedEntry.select_mode == false)
            {
                SelectedEntry.select_mode = true;
                if (Layoutinfo.darkmode)
                {
                    btnSelect.BackColor = Color.FromArgb(190, 220, 255);
                    btnDelete.BackColor = Color.FromArgb(255, 120, 120);
                }
                else
                {
                    btnSelect.BackColor = Color.FromArgb(170, 200, 235);
                    btnDelete.BackColor = Color.FromArgb(235, 100, 100);
                }
                btnDelete.BorderColor = Color.Black;
                btnSelect.BorderColor = Color.Black;
            }
            else
            {
                foreach(PictureBox picbox in pnlImagecontainer.Controls)
                {
                    picbox.Paint -= new PaintEventHandler(picboxSelected_Paint);
                    picbox.Refresh();
                }
                SelectedEntry.select_mode = false;
                if(Layoutinfo.darkmode)
                {
                    btnSelect.BackColor = ProjectColors.darkmode_fillcolor;
                    btnDelete.BackColor = ProjectColors.darkmode_fillcolor;
                }
                else
                {
                    btnSelect.BackColor = ProjectColors.lightmode_fillcolor;
                    btnDelete.BackColor = ProjectColors.lightmode_fillcolor;
                }
                btnDelete.BorderColor = Color.DeepSkyBlue;
                btnSelect.BorderColor = Color.DeepSkyBlue;
            }
        }
        void delMovie(int movie_id)
        {
            Database.deleteMovieEntry(movie_id);
            Log.Information("Sucessfully deleted movie with ID: {@movie}", movie_id);
            Database.deleteMoviesactorsEntry(movie_id);
            Log.Information("Sucessfully deleted movie_actors link with ID: {@movie}", movie_id);
            Database.deleteThumbEntry(movie_id);
            Log.Information("Sucessfully deleted movie_thumbnail with ID: {@movie}", movie_id);
        }
        void delDirector(int director_id)
        {
            try
            {
                //SearchDatabase search = new SearchDatabase(director_id);
                //Person directorToDel = search.directorById()[0];
                Database.deleteDirectorEntry(director_id);
                Log.Information("Sucessfully deleted director with ID: {@id}", director_id);
                Database.deleteDirectorProfileEntry(director_id);
                Log.Information("Sucessfully deleted actor profile picture with ID: {@id}", director_id);
            }
            catch
            {
                Log.Warning("Deleting of director failed.");
            }

        }
        void delActor(int actor_id)
        {
            try
            {
                //SearchDatabase search = new SearchDatabase(actor_id);
                //Person actorToDel = search.actorById()[0];
                Database.deleteActorEntry(actor_id);
                Log.Information("Sucessfully deleted actor with ID: {@id}", actor_id);
                Database.deleteActorProfileEntry(actor_id);
                Log.Information("Sucessfully deleted actor profile picture with ID: {@id}", actor_id);
            }
            catch
            {
                Log.Warning("Deleting of actor failed.");
            }
        }

        /// <summary>
        /// Allows options - Coming soon.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOptions_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Allows reload of the layout per button-click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReload_Click(object sender, EventArgs e)
        {
            if (!pnlPageNumbers.Visible)
            {
                pnlPageNumbers.Visible = true;
                pnlImagecontainer.BorderStyle = BorderStyle.None;
                pnlImagecontainer.Paint += new PaintEventHandler(pnlImagecontainer_Paint);
                pnlImagecontainer.AutoScroll = false;

            }

            displayImagesOnPage(1);
            set_pnlPageNumbers_Highlight(pagenumLabels[0]);
            pnlPageNumbers.Refresh();
            pnlImagecontainer.Refresh();
        }

        /// <summary>
        /// Calls a class which consists of a set of branches to determine the type of value which is used to search by trial- and error-principle. Search for Persons is
        /// currently not available.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            //Global.setDefaultTable();
            Log.Information("Combobox table set to default (Movies).");
            //if (Layoutinfo.darkmode) { lboxEntries.BackColor = Color.Black; }
            //else { lboxEntries.BackColor = Color.White; }
            clearThumbnailPanel();
            String searchStr = txtboxSearch.Text;

            Search s = new Search();
            Log.Information("Starting new search.");

            switch(Global.tableSelected)
            {
                case "Movies":
                    List<Movie> movies = s.SearchForMovie(searchStr);
                    if (movies.Any())
                    {
                        Log.Information("Search for movie sucessfull.");
                        fill_Picboxlist_From_Movlist(movies);
                        pnlImagecontainer_Resize(null, null);
                    }
                    break;
                case "Directors":
                    List<Person> directors = s.SearchForPerson(searchStr, true);
                    if(directors.Any())
                    {
                        Log.Information("Search for director sucessfull.");

                        fill_Picboxlist_From_Perslist(directors);
                        pnlImagecontainer_Resize(null, null);
                    }
                    break;
                case "Actors":
                    List<Person> actors = s.SearchForPerson(searchStr, false);
                    if (actors.Any())
                    {
                        Log.Information("Search for director sucessfull.");
                        fill_Picboxlist_From_Perslist(actors);
                        pnlImagecontainer_Resize(null, null);
                    }
                    break;
            }

            pnlPageNumbers.Visible = false;
            pnlImagecontainer.BorderStyle = BorderStyle.FixedSingle;
            pnlImagecontainer.Paint -= new PaintEventHandler(pnlImagecontainer_Paint);
            pnlImagecontainer.AutoScroll = true;
        }

        private void txtboxSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { btnSearch_Click(sender, e); }
        }

        private void clearThumbnailPanel()
        {
            thumbnails_list.Clear();
            pnlImagecontainer.Controls.Clear();
        }

        /// <summary>
        /// Dark and light design.
        /// </summary>
        /// <param name="color"></param>
        private void setFormColor(Color color)
        {
            txtboxSearch.BackColor = color;
            btnSearch.BackColor = color;
            comboxTables.BackColor = color;
            btnReload.BackColor = color;
            btnCreate.BackColor = color;
            btnDelete.BackColor = color;
            btnSelect.BackColor = color;
            btnOptions.BackColor = color;
        }
        private void setTextColor(Color color)
        {
            btnCreate.ForeColor = color;
            btnDelete.ForeColor = color;
            comboxTables.ForeColor = color;
            txtboxSearch.ForeColor = color;
        }
        void setFormFont(Font font)
        {
            txtboxSearch.Font = font;
            btnCreate.Font = font;
            btnDelete.Font = font;
            comboxTables.Font = font;
        }


        /// <summary>
        /// Get the ammount of movies from the database, then create labels for the pages in which a given number of
        /// movies should be displayed. Every label has a click event to retrieve the thumbnails.
        /// Click event for the first and always existing label will be launched automaticly to allow drawing of border
        /// in the displayal panel's paint event. This click event will be launched with a fake/null Eventargs parameter.
        /// Also all the labels are added to a higher scoped list to be able to change their style in other methods.
        /// 
        /// This method is used for the movies, directors and actors.
        /// </summary>
        private void calcPages()
        {
            pnlPageNumbers.Controls.Clear();
            pagenumLabels.Clear();

            string table = Global.tableSelected.ToLower();

            double records = Math.Ceiling((double)Database.getAmmountOfRecs(table) / imagesPerPage);
            for (int i = 1; i <= records; i++)
            {
                Label label = new Label();
                //label.Left = (i-1) * (label.Width) + Math.Sign(i-1) * margin;
                label.Left = (i - 1) * label.Width;
                label.Height = pnlPageNumbers.Height;
                label.Font = Layoutinfo.font;
                label.Top = 0;
                label.Width = pagenumLabel_Width;
                label.TextAlign = ContentAlignment.MiddleCenter;
                label.Text = i.ToString();
                label.Name = "lblPageNumber_" + i.ToString();
                label.Click += new EventHandler(lblPagenumbers_Click);
                label.Paint += new System.Windows.Forms.PaintEventHandler(control_PaintBorder);
                if (Layoutinfo.darkmode)
                {
                    label.BackColor = ProjectColors.darkmode_pnlImageConInactive;
                }
                else
                {
                    label.BackColor = ProjectColors.lightmode_pnlImageConInactive;
                }
                pnlPageNumbers.Controls.Add(label);
                pagenumLabels.Add(label);
                if (i == 1) { setLabelStyle(label); }
            }
            if (pagenumLabels.Any()) { set_pnlPageNumbers_Highlight(pagenumLabels[0]); };
            pnlImagecontainer.Refresh();
        }

        /// <summary>
        /// Set the style of the pagenumlabels.
        /// </summary>
        /// <param name="label"></param>
        private void setLabelStyle(Label label)
        {
            if (label != null)
            {
                int pageIndex;
                if (int.TryParse(label.Text, out pageIndex))
                {
                    displayImagesOnPage(pageIndex);
                }
                else
                {
                    Log.Error("Pagenum label did contain a non numarable value, cast failed - images could not be displayed.");
                }
            }
        }

        /// <summary>
        /// Click event for the pagenumber labels. This method changes the style of the controls and then redirects to the actual
        /// displayal of the thumbnails.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblPagenumbers_Click(object? sender, EventArgs? e)
        {
            if (sender != null)
            {
                int pageIndex = set_pnlPageNumbers_Highlight((Label)sender);
                displayImagesOnPage(pageIndex);
                pnlImagecontainer.Refresh();
            }
        }
        /// <summary>
        /// Set the higlight backcolor and border for the selected pagenumber.
        /// </summary>
        /// <param name="senderAsLabel"></param>
        /// <returns></returns>
        private int set_pnlPageNumbers_Highlight(Label senderAsLabel)
        {
            foreach (Label l in pagenumLabels)
            {
                if (Layoutinfo.darkmode)
                {
                    l.BackColor = ProjectColors.darkmode_pnlImageConInactive;
                }
                else
                {
                    l.BackColor = ProjectColors.lightmode_pnlImageConInactive;
                }
                //l.Paint -= new System.Windows.Forms.PaintEventHandler(control_PaintBorder);
            }

            int pageIndex = 1;
            try
            {
                pageIndex = int.Parse(senderAsLabel.Text);
                if (Layoutinfo.darkmode)
                {
                    senderAsLabel.BackColor = ProjectColors.darkmode_pnlImageConActive;
                }
                else
                {
                    senderAsLabel.BackColor = ProjectColors.lightmode_pnlImageConActive;
                }
                //senderAsLabel.Paint += new System.Windows.Forms.PaintEventHandler(control_PaintBorder);
                activePagenumLabel = senderAsLabel;
                return pageIndex;
            }
            catch (InvalidCastException)
            {
                Log.Error("Page could not be loaded due to an InvalidCastException of event object/sender.");
                return -1;
            }
            catch
            {
                Log.Error("Page could not be loaded due to a unknown error.");
                return -1;
            }
        }

        /// <summary>
        /// This method gets the number of the current page as a parameter.
        /// This parameter is used to determine the starting index in the range of the index for the images.
        /// so will page 1 have a range of 1-10, page 2 has 11-20, page 3 has 21-30 and so on.
        /// Splitting the display of all the images into arrays of 10 prevents the floating of memory in case of a large database.
        /// The thumbnails are retrieved and returned by a method in the Global.cs class.
        /// </summary>
        /// <param name="range"></param>
        private void displayImagesOnPage(int range)
        { 
            int imgPerPage = imagesPerPage;
            int rangemax = range * imgPerPage;
            int rangemin = rangemax - (imgPerPage - 1);

            if(Global.tableSelected == "Movies")
            {
                List<Movie> movies = new List<Movie>();
           
                //This loop will break if a thumbnail (movie) is null.
                for (int i = rangemin; i <= rangemax; i++)
                {
                    Movie varMovie = new Movie(i, Database.RetrieveBLOBFromID(i, Global.tableSelected.ToLower()));  //Returns BLOB byte[].
                    if (varMovie.ThumbnailBArray != null)
                    {
                        movies.Add(varMovie);
                    }
                    else
                    {
                        continue;
                    }
                }

                fill_Picboxlist_From_Movlist(movies);
            }
            else if(Global.tableSelected == "Directors" || Global.tableSelected == "Actors")
            {
                List<Person> persons = new List<Person>();
                for (int i = rangemin; i <= rangemax; i++)
                {
                    Person varPers = new Person(i, Database.RetrieveBLOBFromID(i, Global.tableSelected.ToLower()));  //Returns BLOB byte[].
                    if (varPers.Profilepic != null)
                    {
                        persons.Add(varPers);
                    }
                    else
                    {
                        continue;
                    }
                }

                fill_Picboxlist_From_Perslist(persons);

            }
            else 
            {
                Log.Error("No table selected in global variable...");
            }
            pnlImagecontainer_Resize(null, null);
        }

        /// <summary>
        /// Fills a list with thumbnails as pictureboxes equivalent to a list of movies.
        /// </summary>
        /// <param name="movies"></param>
        private void fill_Picboxlist_From_Movlist(List<Movie> movies)
        {
            int imageWidht = 100;
            int imageHeight = 149;
            thumbnails_list.Clear();

            foreach (Movie movie in movies)
            {
                PictureBox picbox = new PictureBox();
                Bitmap? image;

                image = Global.byteToImage(movie.ThumbnailBArray);
                if(image == null) { continue; }
                picbox.Size = new Size(imageWidht, imageHeight);
                picbox.Image = Global.resizeBitmap(image, imageWidht, imageHeight);
                picbox.Click += new System.EventHandler(thumbnail_Click);
                picbox.Name = "picbox_" + movie.ID.ToString();
                thumbnails_list.Add(picbox);
                //pnlImagecontainer.Controls.Add(picbox);
            }
        }

        /// <summary>
        /// Same as fill_Picboxlist_From_Movlist but for the directors and actors.
        /// </summary>
        /// <param name="movies"></param>
        private void fill_Picboxlist_From_Perslist(List<Person> persons)
        {
            int imageWidht = 100;
            int imageHeight = 149;
            thumbnails_list.Clear();

            foreach (Person person in persons)
            {
                PictureBox picbox = new PictureBox();
                Bitmap image;

                image = Global.byteToImage(person.Profilepic);
                picbox.Size = new Size(imageWidht, imageHeight);
                picbox.Image = Global.resizeBitmap(image, imageWidht, imageHeight);
                picbox.Click += new System.EventHandler(thumbnail_Click);
                picbox.Name = "picbox_" + person.Id.ToString();
                thumbnails_list.Add(picbox);
                //pnlImagecontainer.Controls.Add(picbox);
            }
        }

        /// <summary>
        /// This method can be used to dynamically spread the thumbnails in theyr container.
        /// Currently not active due to complications with the calculation of the tabs.
        /// Its name is confusing and cant be changed due to its dependency to the Usercontrols eventshandler.
        /// This Method is used from different sources to display a given set of pictureboxes into the container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlImagecontainer_Resize(object? sender, EventArgs? e)
        {
            pnlImagecontainer.Controls.Clear();
            int margin = 10;
            int x = margin;  
            int y = margin;

            foreach (PictureBox picbox in thumbnails_list)
            {
                picbox.Location = new Point(x, y);
                //picbox.Padding = new Padding(3, 3, 3, 3);
                pnlImagecontainer.Controls.Add(picbox);

                x += picbox.Size.Width;
                x += margin;
                if (x + picbox.Size.Width > pnlImagecontainer.Size.Width - margin)
                {
                    x = margin;
                    y += picbox.Size.Height;
                    y += margin;
                }
            }
        }

        /// <summary>
        /// Paint a border for the control that launched this method. Border width is specified in the borderWidth array after
        /// the type of the control is specified.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void control_PaintBorder(object? sender, PaintEventArgs? e)
        {
            if(sender != null && e != null)
            {
                int[] borderWidth = new int[4];
                Graphics g = e.Graphics;
                Rectangle clientRect = new Rectangle(0, 0, 0, 0);

                if (sender.GetType() == typeof(Label))
                {
                    Label label = (Label)sender;
                    clientRect = label.ClientRectangle;
                    borderWidth = new int[4] { 1, 1, 1, 0 };
                }
                ControlPaint.DrawBorder(g, clientRect,
                    Color.Black, borderWidth[0], ButtonBorderStyle.Solid,
                    Color.Black, borderWidth[1], ButtonBorderStyle.Solid,
                    Color.Black, borderWidth[2], ButtonBorderStyle.Solid,
                    Color.Black, borderWidth[3], ButtonBorderStyle.Solid
                );
            }
        }

        /// <summary>
        /// Border for the image container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pnlImagecontainer_Paint(object sender, PaintEventArgs e)
        {
            if (activePagenumLabel != null)
            {
                Panel panel = (Panel)sender;
                Graphics g = e.Graphics;
                Pen p = new Pen(Color.Black, 1);
                int ammountOfTabs;
                int.TryParse(activePagenumLabel.Text, out ammountOfTabs);
                int line1_length = (ammountOfTabs - 1) * 100;
                int line2_start = ammountOfTabs * 100 - 21;
                ControlPaint.DrawBorder(g, panel.ClientRectangle,
                    Color.Black, 1, ButtonBorderStyle.Solid,
                    Color.Black, 0, ButtonBorderStyle.Solid,
                    Color.Black, 1, ButtonBorderStyle.Solid,
                    Color.Black, 1, ButtonBorderStyle.Solid
                );

                Rectangle rect = pnlImagecontainer.ClientRectangle;
                Point topl_line1 = new Point(rect.X, rect.Y);
                Point topr_line1 = new Point(rect.X + line1_length, rect.Y);

                Point topl_line2 = new Point(rect.X + line2_start, rect.Y);
                Point topr_line2 = new Point(rect.X + rect.Width, rect.Y);

                g.DrawLine(p, topl_line1, topr_line1);
                g.DrawLine(p, topl_line2, topr_line2);

                p.Dispose();
            }
        }

        /// <summary>
        /// Draw a border to selected movies/id's.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picboxSelected_Paint(object? sender, PaintEventArgs? e)
        {
            try
            {
                Graphics g = e.Graphics;
                PictureBox picbox = (PictureBox)sender;
                Color col = Color.FromArgb(255, 120, 120);
                ControlPaint.DrawBorder(g, picbox.ClientRectangle,
                        col, 3, ButtonBorderStyle.Solid,
                        col, 3, ButtonBorderStyle.Solid,
                        col, 3, ButtonBorderStyle.Solid,
                        col, 3, ButtonBorderStyle.Solid
                );
            }
            catch (Exception ex)
            {
                Log.Error("Exception at painting border to selected picturebox: {@ex}", ex);
            }
        }

        /// <summary>
        /// Show the usercontrol used to display informations about the movie which are stored in a globaly available Movie object.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void thumbnail_Click(object? sender, EventArgs? e)
        {
            if(sender == null || e == null) return;

            PictureBox picbox = (PictureBox)sender;

            //Select-mode is on, don't show movie info, but add id to a list for further steps.
            if (SelectedEntry.select_mode == true)
            {
                string s = picbox.Name.Split('_')[1];

                int id;
                if (int.TryParse(s, out id))
                {
                    if (SelectedEntry.selected_entry_ids.Any())
                    {
                        bool already_selected = false;
                        foreach (int id_in_list in SelectedEntry.selected_entry_ids)
                        {
                            if (id_in_list == id)
                            {
                                already_selected = true;
                                break;
                            }
                        }
                        if (already_selected)
                        {
                            SelectedEntry.selected_entry_ids.Remove(id);
                            picbox.Paint -= new PaintEventHandler(picboxSelected_Paint);
                            picbox.Refresh();
                            return;
                        }
                    }
                    SelectedEntry.selected_entry_ids.Add(id);
                    picbox.Paint += new PaintEventHandler(picboxSelected_Paint);
                    picbox.Refresh();
                }
            }
            //Select-mode if off, show movie info.
            else
            {
                picbox.Paint -= new PaintEventHandler(picboxSelected_Paint);
                int id;
                string s = picbox.Name.Split('_')[1];
                if (int.TryParse(s, out id))
                {
                    SearchDatabase search = new SearchDatabase(id);
                    switch (Global.tableSelected)
                    {
                        case "Movies":
                            try
                            {
                                List<Movie> movielist = search.movieByID();
                                Movie movie = movielist.First();
                                Global.currentMovie = new Movie(movie.Name, movie.Description, movie.Date, movie.ID, movie.Rating, movie.Director, movie.Length);
                                Global.displayMovieToFront();
                            }
                            //For does not work because the exception gets triggered inside the search. ???
                            catch (IndexOutOfRangeException ex)
                            {
                                Log.Debug("Exception caught while getting informations about a entry: {@ex}", ex);
                                Log.Error("Could not load information page due to invalid index of entry. Entry may partialy be broken...");
                            }
                            break;
                        case "Directors":
                            try
                            {
                                Person director = search.directorById()[0];
                                Global.currentPerson = new Person(director.Id, director.Name, director.Birthdate, director.Country);
                                Global.DisplayPersonToFront();
                            }
                            catch (IndexOutOfRangeException ex)
                            {
                                Log.Debug("Exception caught while getting informations about a entry: {@ex}");
                                Log.Error("Could not load information page due to invalid index of entry. Entry may partialy be broken...");
                            }
                            break;
                        case "Actors":
                            try
                            {
                                Person actor = search.actorById()[0];
                                Global.currentPerson = new Person(actor.Id, actor.Name, actor.Birthdate, actor.Country);
                                Global.DisplayPersonToFront();
                            }
                            catch (Exception ex)
                            {
                                Log.Debug("Exception caught while getting informations about a entry: {@ex}");
                                Log.Error("Could not load information page due to invalid index of entry. Entry may partialy be broken...");
                            }
                            break;
                    }
                    try
                    {
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        Log.Debug("Exception caught while getting informations about a entry: {@ex}");
                        Log.Error("Could not load information page due to invalid index of entry. Entry may partialy be broken...");
                    }
                }
            }
        }

        /// <summary>
        /// Start deletion of selected movie id's.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (!SelectedEntry.selected_entry_ids.Any())
            {
                string msg = "Delete failed, no entries are selected!";
                Global.displayErrorMessage(msg);
                return;
            }

            if(Global.tableSelected == "Movies")
            {
                foreach (int i in SelectedEntry.selected_entry_ids)
                {
                    delMovie(i);
                }
                calcPages();
                string m = "Movie(s) sucessfuly deleted. Please not that the movie cast did not get affected from this deletion.";
                string c = "Sucess!";
                MessageBoxButtons b = MessageBoxButtons.OK;
                MessageBox.Show(m, c, b);
            }
            else if (Global.tableSelected == "Directors")
            {
                foreach (int i in SelectedEntry.selected_entry_ids)
                {
                    delDirector(i);
                }
                calcPages();
                string m = "Director(s) sucessfuly deleted.";
                string c = "Sucess!";
                MessageBoxButtons b = MessageBoxButtons.OK;
                MessageBox.Show(m, c, b);
            }
            else if (Global.tableSelected == "Actors")
            {
                foreach (int i in SelectedEntry.selected_entry_ids)
                {
                    delActor(i);
                }
                calcPages();
                string m = "Actor(s) sucessfuly deleted.";
                string c = "Sucess!";
                MessageBoxButtons b = MessageBoxButtons.OK;
                MessageBox.Show(m, c, b);
            }
        }

        /// <summary>
        /// Set the design.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucMain_VisibleChanged(object sender, EventArgs e)
        {
            if (Layoutinfo.darkmode)
            {
                BackColor = ProjectColors.darkmode_background;
                pnlImagecontainer.BackColor = ProjectColors.darkmode_pnlImageConActive;
                pnlPageNumbers.BackColor = ProjectColors.darkmode_background;
                setFormColor(ProjectColors.darkmode_fillcolor);
                setTextColor(ProjectColors.darkmode_fontcolor);
                btnReload.Image = Images.ReloadIcon_26_white;
                btnSearch.Image = Images.SearchIcon_23_white;
                btnSelect.Image = Images.SelectIcon_26_white;
                btnOptions.Image = Images.Setup_23_white;
                //btnSelect.FlatAppearance.MouseOverBackColor = ProjectColors.darkmode_btnhover;
                //btnSearch.FlatAppearance.MouseOverBackColor = ProjectColors.darkmode_btnhover;
                //btnReload.FlatAppearance.MouseOverBackColor = ProjectColors.darkmode_btnhover;
                //btnCreate.FlatAppearance.MouseOverBackColor = ProjectColors.darkmode_btnhover;
                //btnDelete.FlatAppearance.MouseOverBackColor = ProjectColors.darkmode_btnhover;
                //btnOptions.FlatAppearance.MouseOverBackColor = ProjectColors.darkmode_btnhover;

            }
            else
            {
                BackColor = ProjectColors.lightmode_background;
                pnlImagecontainer.BackColor = ProjectColors.lightmode_pnlImageConActive;
                pnlPageNumbers.BackColor = ProjectColors.lightmode_background;
                setFormColor(ProjectColors.lightmode_fillcolor);
                setTextColor(ProjectColors.lightmode_fontcolor);
                btnReload.Image = Images.ReloadIcon_26_black;
                btnSearch.Image = Images.SearchIcon_23_black;
                btnSelect.Image = Images.SelectIcon_26_black;
                btnOptions.Image = Images.Setup_23_black;
            }
            if (Layoutinfo.font != null) { setFormFont(Layoutinfo.font); }
            btnSelect.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderSubmitBlue;
            btnSearch.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderSubmitBlue;
            btnReload.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderSubmitBlue;
            btnCreate.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderGreenlighter;
            btnDelete.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderRed;
            btnOptions.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderSubmitBlue;
        }
    }
}
