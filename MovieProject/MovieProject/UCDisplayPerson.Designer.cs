﻿namespace MovieProject
{
    partial class UCDisplayPerson
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.btnExit = new MovieProject.ncButton();
            this.lblOrigin = new System.Windows.Forms.Label();
            this.lblBirthdate = new System.Windows.Forms.Label();
            this.picboxProfile = new System.Windows.Forms.PictureBox();
            this.lboxMovies = new System.Windows.Forms.ListBox();
            this.lblMovies = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picboxProfile)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.White;
            this.lblName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblName.Location = new System.Drawing.Point(50, 222);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(332, 29);
            this.lblName.TabIndex = 47;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundColor = System.Drawing.Color.White;
            this.btnExit.BorderColor = System.Drawing.Color.MediumPurple;
            this.btnExit.BorderRadius = 10;
            this.btnExit.BorderSize = 1;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.Image = global::MovieProject.Images.ReturnIcon_26_black;
            this.btnExit.Location = new System.Drawing.Point(50, 102);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 53);
            this.btnExit.TabIndex = 58;
            this.btnExit.TextColor = System.Drawing.Color.Black;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblOrigin
            // 
            this.lblOrigin.BackColor = System.Drawing.Color.White;
            this.lblOrigin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOrigin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblOrigin.Location = new System.Drawing.Point(50, 364);
            this.lblOrigin.Name = "lblOrigin";
            this.lblOrigin.Size = new System.Drawing.Size(332, 29);
            this.lblOrigin.TabIndex = 59;
            this.lblOrigin.Text = "Origin";
            this.lblOrigin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBirthdate
            // 
            this.lblBirthdate.BackColor = System.Drawing.Color.White;
            this.lblBirthdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBirthdate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblBirthdate.Location = new System.Drawing.Point(50, 296);
            this.lblBirthdate.Name = "lblBirthdate";
            this.lblBirthdate.Size = new System.Drawing.Size(332, 29);
            this.lblBirthdate.TabIndex = 60;
            this.lblBirthdate.Text = "Birthdate";
            this.lblBirthdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // picboxProfile
            // 
            this.picboxProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picboxProfile.Location = new System.Drawing.Point(435, 164);
            this.picboxProfile.Name = "picboxProfile";
            this.picboxProfile.Size = new System.Drawing.Size(200, 300);
            this.picboxProfile.TabIndex = 61;
            this.picboxProfile.TabStop = false;
            // 
            // lboxMovies
            // 
            this.lboxMovies.FormattingEnabled = true;
            this.lboxMovies.ItemHeight = 15;
            this.lboxMovies.Location = new System.Drawing.Point(683, 254);
            this.lboxMovies.Name = "lboxMovies";
            this.lboxMovies.Size = new System.Drawing.Size(333, 139);
            this.lboxMovies.TabIndex = 62;
            // 
            // lblMovies
            // 
            this.lblMovies.BackColor = System.Drawing.Color.White;
            this.lblMovies.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblMovies.Location = new System.Drawing.Point(683, 222);
            this.lblMovies.Name = "lblMovies";
            this.lblMovies.Size = new System.Drawing.Size(333, 29);
            this.lblMovies.TabIndex = 63;
            this.lblMovies.Text = "Movies from this Person";
            this.lblMovies.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UCDisplayPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblMovies);
            this.Controls.Add(this.lboxMovies);
            this.Controls.Add(this.picboxProfile);
            this.Controls.Add(this.lblBirthdate);
            this.Controls.Add(this.lblOrigin);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblName);
            this.Name = "UCDisplayPerson";
            this.Size = new System.Drawing.Size(1070, 628);
            this.Load += new System.EventHandler(this.UCDisplayPerson_Load);
            this.VisibleChanged += new System.EventHandler(this.UCDisplayPerson_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.picboxProfile)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lblName;
        private ncButton btnExit;
        private Label lblOrigin;
        private Label lblBirthdate;
        private PictureBox picboxProfile;
        private ListBox lboxMovies;
        private Label lblMovies;
    }
}
