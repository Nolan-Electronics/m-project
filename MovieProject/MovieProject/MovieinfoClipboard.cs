﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject
{
    /// <summary>
    /// This class is used to share data between the ucMain and ucCreateperson usercontrols.
    /// Its is important to dispose all data after the process of creating a movie-entry to prevent interferences.
    /// </summary>
    public static class MovieinfoClipboard
    {
        public static string name;
        public static string description;
        public static string reldate;
        public static int length;
        public static float rating;
    }
}
