﻿/*

Custom combobox based on a panel, button and normal combobox. The OnSelectedIndexChanged event has to be added in code and can not
be set trought the form builder.

*/
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Drawing.Design;

namespace MovieProject
{
    internal class ncCombobox:UserControl
    {
        private Color backColor = Color.WhiteSmoke;
        private Color iconColor = Color.FromArgb(124, 129, 242);
        private Color listBackColor = Color.FromArgb(230, 228, 245);
        private Color listTextColor = Color.Black;
        private Color borderColor = Color.FromArgb(124, 129, 242);
        private int borderSize = 1;
        private Color foreColor = Color.Black;


        private ComboBox cmbList;
        private Label lblText;
        private Button btnIcon;

        public EventHandler OnSelectedIndexChanged;

        public new Color BackColor { get => backColor; set { backColor = value; lblText.BackColor = backColor; btnIcon.BackColor = backColor; } }
        public Color IconColor { get => iconColor; set { iconColor = value; btnIcon.Invalidate(); } }
        public Color ListBackColor { get => listBackColor; set { listBackColor = value; cmbList.BackColor = listBackColor; } }
        public Color ListTextColor { get => listTextColor; set { listTextColor = value; cmbList.ForeColor = listTextColor; } }
        public Color BorderColor { get => borderColor; set { borderColor = value; base.BackColor = borderColor; } }
        public int BorderSize { get => borderSize; set { borderSize = value; this.Padding = new Padding(BorderSize); adjustComboBoxDimension(); } }

        public override Font Font
        {
            get { return base.Font; }
            set
            {
                base.Font = value;
                lblText.Font = value;
                cmbList.Font = value;
            }
        }

        public override Color ForeColor { get => base.ForeColor; set { base.ForeColor = value; lblText.ForeColor = value; } }


        public string Caption
        {
            get { return lblText.Text; }
            set { lblText.Text = value; }
        }
        public ComboBoxStyle DropDownStyle
        {
            get { return cmbList.DropDownStyle; }
            set { if (cmbList.DropDownStyle != ComboBoxStyle.Simple) { cmbList.DropDownStyle = value; } }
        }

        [Category("My own")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor("System.Windows.Forms.Design.ListControlStringCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [Localizable(true)]
        [MergableProperty(false)]
        public ComboBox.ObjectCollection Items
        {
            get { return cmbList.Items; }
        }
        [Category("My own")]
        [AttributeProvider(typeof(IListSource))]
        [DefaultValue(null)]
        public object DataSource
        {
            get { return cmbList.DataSource; }
            set { cmbList.DataSource = value; }
        }
        [Category("My own")]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor("System.Windows.Forms.Design.ListControlStringCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Localizable(true)]
        public AutoCompleteStringCollection AutoCompleteCustomSource
        {
            get { return cmbList.AutoCompleteCustomSource; }
            set { cmbList.AutoCompleteCustomSource = value; }
        }
        [Category("My own")]
        [Browsable(true)]
        [DefaultValue(AutoCompleteSource.None)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public AutoCompleteSource AutoCompleteSource
        {
            get { return cmbList.AutoCompleteSource; }
            set { cmbList.AutoCompleteSource = value; }
        }
        [Category("My own")]
        [Browsable(true)]
        [DefaultValue(AutoCompleteMode.None)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public AutoCompleteMode AutoCompleteMode
        {
            get { return cmbList.AutoCompleteMode; }
            set { cmbList.AutoCompleteMode = value; }
        }
        [Category("My own")]
        [Bindable(true)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public object SelectedItem
        {
            get { return cmbList.SelectedItem; }
            set { cmbList.SelectedItem = value; }
        }
        [Category("My own")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelectedIndex
        {
            get { return cmbList.SelectedIndex; }
            set { cmbList.SelectedIndex = value; }
        }
        [Category("My own")]
        [DefaultValue("")]
        [Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        [TypeConverter("System.Windows.Forms.Design.DataMemberFieldConverter, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
        public string DisplayMember
        {
            get { return cmbList.DisplayMember; }
            set { cmbList.DisplayMember = value; }
        }
        [Category("My own")]
        [DefaultValue("")]
        [Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor))]
        public string ValueMember
        {
            get { return cmbList.ValueMember; }
            set { cmbList.ValueMember = value; }
        }


        public ncCombobox()
        {
            cmbList = new ComboBox();
            lblText = new Label();
            btnIcon = new Button();
            this.SuspendLayout();   //Layout/Formdrawing pausieren um performance zu erhöhen.

            cmbList.BackColor = listBackColor;
            cmbList.Font = new Font(this.Font.Name, 10F); cmbList.ForeColor = listTextColor;
            cmbList.SelectedIndexChanged += new System.EventHandler(ncCombobox_SelectedIndexChanged);
            cmbList.TextChanged += new System.EventHandler(ncCombobox_TextChanged);
            cmbList.ItemHeight = 30;
            cmbList.Width = lblText.Width + btnIcon.Width;

            btnIcon.Dock = DockStyle.Right;
            btnIcon.FlatStyle = FlatStyle.Flat;
            btnIcon.FlatAppearance.BorderSize = 0;
            btnIcon.BackColor = backColor;
            btnIcon.Size = new Size(30, 30);
            btnIcon.Cursor = Cursors.Hand;
            btnIcon.Click += new System.EventHandler(btnIcon_Click);
            btnIcon.Paint += new PaintEventHandler(btnIcon_Paint);

            lblText.Dock = DockStyle.Fill;
            lblText.AutoSize = false;
            lblText.BackColor = backColor;
            lblText.ForeColor = foreColor;
            lblText.TextAlign = ContentAlignment.MiddleCenter;
            lblText.Padding = new Padding(8, 0, 0, 0);
            lblText.Font = new Font(this.Font.Name, 10F); lblText.Click += new System.EventHandler(btnIcon_Click);
            lblText.MouseEnter += new System.EventHandler(Surface_MouseEnter);
            lblText.MouseLeave += new System.EventHandler(Surface_MouseLeave);

            this.Controls.Add(lblText);
            this.Controls.Add(btnIcon);
            this.Controls.Add(cmbList);
            this.MinimumSize = new Size(200, 30);
            this.Size = new Size(200, 30);
            this.ForeColor = Color.DimGray;
            this.Padding = new Padding(borderSize);
            base.BackColor = borderColor;
            this.ResumeLayout();

            adjustComboBoxDimension();
        }

        public void SetFont(Font f)
        {
            cmbList.Font = f;
            cmbList.Refresh();
        }

        private void Surface_MouseLeave(object sender, EventArgs e)
        {
            this.OnMouseLeave((e as MouseEventArgs));
        }

        private void Surface_MouseEnter(object sender, EventArgs e)
        {
            this.OnMouseLeave((e as MouseEventArgs));
        }

        private void adjustComboBoxDimension()
        {
            double marginConst = lblText.Height / 5;
            cmbList.Location = new Point()
            {
                X = lblText.Location.X,
                Y = lblText.Location.Y + (int)Math.Round(marginConst) - 1,
            };
        }

        private void ncCombobox_TextChanged(object sender, EventArgs e)
        {
            lblText.Text = cmbList.Text;
        }

        private void ncCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (OnSelectedIndexChanged != null) { OnSelectedIndexChanged.Invoke(sender, e); }
            lblText.Text = cmbList.Text;
        }

        private void btnIcon_Click(object sender, EventArgs e)
        {
            cmbList.Select();
            cmbList.DroppedDown = true;
        }

        private void btnIcon_Paint(object sender, PaintEventArgs e)
        {
            int iconWidth = 14;
            int iconHeight = 6;
            var rectIcon = new Rectangle((btnIcon.Width - iconWidth) / 2, (btnIcon.Height - iconHeight) / 2, iconWidth, iconHeight);
            Graphics g = e.Graphics;

            using (GraphicsPath path = new GraphicsPath())
            using (Pen p = new Pen(iconColor, 2))
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
                path.AddLine(rectIcon.X, rectIcon.Y, rectIcon.X + (iconWidth / 2), rectIcon.Bottom);
                path.AddLine(rectIcon.X + (iconWidth / 2), rectIcon.Bottom, rectIcon.Right, rectIcon.Y);
                g.DrawPath(p, path);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            cmbList.Width = lblText.Width + btnIcon.Width;
        }
    }
}
