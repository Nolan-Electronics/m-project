﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog;

namespace MovieProject
{
    public partial class UCDisplayPerson : UserControl
    {
        public UCDisplayPerson()
        {
            InitializeComponent();
        }

        private void resetFields()
        {
            lblBirthdate.Text = "";
            lblMovies.Text = "";
            lblName.Text = "";
            lblOrigin.Text = "";
            lboxMovies.Items.Clear();
            picboxProfile.Image = null;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            resetFields();
            Global.mainToFront();
        }

        private void UCDisplayPerson_Load(object sender, EventArgs e)
        {

        }

        private void SetInfos()
        {
            if (Global.currentPerson != null)
            {
                Global.changelblCaption(Global.currentPerson.Name);
                lblBirthdate.Text = Global.currentPerson.Birthdate;
                lblMovies.Text = $"Movies with {Global.currentPerson.Name}";
                lblName.Text = Global.currentPerson.Name;
                lblOrigin.Text = Global.currentPerson.Country;

                byte[] profile_blob = Database.RetrieveBLOBFromID(Global.currentPerson.Id, Global.tableSelected.ToLower());
                Bitmap image = Global.byteToImage(profile_blob);
                picboxProfile.Image = Global.resizeBitmap(image, picboxProfile.Width, picboxProfile.Height);

                if(Global.tableSelected == "Directors")
                {
                    List<Movie> movies_with_person = new List<Movie>();
                    SearchDatabase search = new SearchDatabase(Global.currentPerson.Id);
                    movies_with_person = search.movieByDirector();
                    foreach (Movie movie in movies_with_person)
                    {

                        lboxMovies.Items.Add(movie.Name);
                    }
                }
                else
                {
                    List<Movie> movies_with_person = new List<Movie>();
                    SearchDatabase search = new SearchDatabase(Global.currentPerson.Id);
                    movies_with_person = search.movieByActor();
                    foreach (Movie movie in movies_with_person)
                    {

                        lboxMovies.Items.Add(movie.Name);
                    }
                }
            }
            else
            {
                Log.Error("Globaly stored person related informations where null, aborting the displayal of a person.");
                Global.mainToFront();
            }
        }

        private void SetFormFont(Font f)
        {
            lblName.Font = f;
            lblOrigin.Font = f;
            lblBirthdate.Font = f;
            lblMovies.Font = f;
            lboxMovies.Font = f;
        }
        private void SetTextColor(Color c)
        {
            lblName.ForeColor = c;
            lblOrigin.ForeColor = c;
            lblBirthdate.ForeColor = c;
            lblMovies.ForeColor = c;
            lboxMovies.ForeColor = c;
        }
        private void SetFormColor(Color c)
        {
            lblName.BackColor = c;
            lblOrigin.BackColor = c;
            lblBirthdate.BackColor = c;
            lblMovies.BackColor = c;
            lboxMovies.BackColor = c;
            btnExit.BackColor = c;
        }

        private void SetStyle()
        {
            if(Layoutinfo.font!= null) { SetFormFont(Layoutinfo.font); }
            btnExit.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderReturnPurple;
            btnExit.BorderColor = ProjectColors.buttonBorderReturnPurple;
            if(Layoutinfo.darkmode) 
            {
                btnExit.Image = Images.ReturnIcon_26_white;
                this.BackColor= ProjectColors.darkmode_background;
                SetFormColor(ProjectColors.darkmode_fillcolor);
                SetTextColor(ProjectColors.darkmode_fontcolor);
            }
            else 
            {
                btnExit.Image = Images.ReturnIcon_26_black;
                this.BackColor = ProjectColors.lightmode_background;
                SetFormColor(ProjectColors.lightmode_fillcolor);
                SetTextColor(ProjectColors.lightmode_fontcolor);
            }
        }

        private void UCDisplayPerson_VisibleChanged(object sender, EventArgs e)
        {
            if(this.Visible)
            {
                SetInfos();
                SetStyle();
            }
        }
    }
}
