﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject
{
    /// <summary>
    /// This class holds all informations concering the ability to select movies and delete them, inclusive the selected movies/ids.
    /// </summary>
    public static class SelectedEntry
    {
        public static bool select_mode = false;
        public static List<int> selected_entry_ids = new List<int>();
    }
}
