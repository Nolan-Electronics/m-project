﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog;

namespace MovieProject
{
    public partial class ucLogin : UserControl
    {
        public ucLogin()
        {
            InitializeComponent();
        }

        private void radbtnDarkm_CheckedChanged(object sender, EventArgs e)
        {
            Layoutinfo.darkmode = true;
            Log.Information("Design changed to dark.");
        }

        private void radbtnLightm_CheckedChanged(object sender, EventArgs e)
        {
            Layoutinfo.darkmode = false;
            Log.Information("Design changed to light.");
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            enter();
        }
        private void UCLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) enter();
        }
        /// <summary>
        /// Store prefs and connect to DB.
        /// </summary>
        private void enter()
        {
            string server = txtboxServer.Text;
            string user = txtboxUser.Text;
            string db = txtboxDB.Text;

            if (comboxFontname != null && int.TryParse(txtboxFontsize.Text, out int fontsize))
            {
                Layoutinfo.fontsize = fontsize;
                Layoutinfo.fontname = comboxFontname.Text;
                Layoutinfo.font = new Font(Layoutinfo.fontname, Layoutinfo.fontsize);
            }

            if (Database.EstablishConnection(server, user, txtboxPassword.Text, db))
            {
                Log.Information("Connected to {@server} as {@user} with schema {@db}", server, user, db);
                Global.mainToFront();
                Global.setDefaultTable();
            }
            else
            {
                Log.Warning("Connetion has failed. (To {@server} as {@user} with schema {@db}). Check the credentials and assure you have the correct database installen or available.", server,user,db);
                lblMessage.Visible = true;
            }
        }

        /// <summary>
        /// Fill the listbox with all available fonts on the system.
        /// </summary>
        private void fillFontListbox()
        {
            foreach(FontFamily font in System.Drawing.FontFamily.Families)
            {
                comboxFontname.Items.Add(font.Name);
            }
        }

        /// <summary>
        /// Set the settings from the retrieved values of the json file so they can (but dont have to) be overwritten.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucLogin_VisibleChanged(object sender, EventArgs e)
        {
            if (Layoutinfo.darkmode)
            {
                radbtnDarkm.Checked = true;
                radbtnLightm.Checked = false;
                Log.Information("Changed to darkmode from config file.");
            }
            else
            {
                radbtnDarkm.Checked = false;
                radbtnLightm.Checked = true;
                Log.Information("Changed to lightmode from config file.");
            }
            if (Layoutinfo.fontname != null) { comboxFontname.SelectedItem = Layoutinfo.fontname; Log.Information("Changed fontname from config file."); }
            if (Layoutinfo.fontsize != 0) { txtboxFontsize.Text = Layoutinfo.fontsize.ToString(); Log.Information("Changed fontsize from config file."); }

            fillFontListbox();
        }
    }
}
