﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject
{
    internal class Movie
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ID { get; set; }
        public string Date { get; set; }
        public DateTime Releasedate { get; set; }
        public double Rating { get; set; }
        public string Director { get; set; }
        public int Length { get; set; }
        public int DirectorID { get; set; }
        public byte[] ThumbnailBArray { get; set; }

        public Image? Thumbnail { get; set; }

        /// <summary>
        /// Constructor used to create/insert a movie into the database.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <param name="date"></param>
        /// <param name="rating"></param>
        /// <param name="directorid"></param>
        /// <param name="length"></param>
        public Movie(string name, string desc, string date, double rating, int directorid, int length)
        {
            Name = name;
            Description = desc;
            Date = date;
            Rating = rating;
            DirectorID = directorid;
            Length = length;

            //Set empties:
            ThumbnailBArray= new byte[0];
            Director= string.Empty;
        }
        /// <summary>
        /// Constructor used to create/insert a movie into the database.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <param name="date"></param>
        /// <param name="rating"></param>
        /// <param name="directorid"></param>
        /// <param name="length"></param>
        public Movie(string name, string desc, DateTime date, double rating, int directorid, int length)
        {
            Name = name;
            Description = desc;
            Releasedate = date;
            Rating = rating;
            DirectorID = directorid;
            Length = length;

            //Set empties:
            Date = string.Empty;
            Director= string.Empty;
            ThumbnailBArray= new byte[0];
        }

        /// <summary>
        /// Basic and most often used constructor, for example to retrieve data from the database in the database.movieRetAction method.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <param name="date"></param>
        /// <param name="id"></param>
        /// <param name="rating"></param>
        /// <param name="length"></param>
        public Movie(string name, string desc, string date, int id, double rating, string director, int length, byte[] thumb)
        {
            Name = name;
            Description = desc;
            Date = date;
            Rating = rating;
            Director = director;
            Length = length;
            ID = id;
            ThumbnailBArray = thumb;
        }

        /// <summary>
        /// Constructor used to contruct a movie as the return value for the Database.RetrieveThumbFromMovieID method.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="thumb"></param>
        public Movie(int id, byte[] thumb)
        {
            ID = id;
            ThumbnailBArray = thumb;

            //Set empties:
            Name = string.Empty;
            Description = string.Empty;
            Date = string.Empty;
            Director= string.Empty;
        }

        /// <summary>
        /// Constructor used for construction the Global.currentMovie movie, which is the read from in the Displaymovie usercontrol.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <param name="date"></param>
        /// <param name="rating"></param>
        /// <param name="length"></param>
        /// <param name="id"></param>
        public Movie(string name, string desc, string date, int id, double rating, string director, int length)
        {
            Name = name;
            Description = desc;
            Date = date;
            Rating = rating;
            Director = director;
            Length = length;
            ID = id;

            //Set empties:
            ThumbnailBArray = new byte[0];
        }

        /*
        public Movie(string name, string desc, int id, string date, double rating, int authorid, int length, byte[] thumb)
        {
            Name= name;
            Description = desc;
            ID = id;
            Date = date;
            Rating = rating;
            AuthorID = authorid;
            Length = length;
            ThumbnailBArray = thumb;
        }
        */
        /*
        public Movie(string name, string desc, string date, double rating, int authorid, int length, byte[] thumb)
        {
            Name = name;
            Description = desc;
            Date = date;
            Rating = rating;
            AuthorID = authorid;
            Length = length;
            ThumbnailBArray = thumb;
        }
        */
    }
}
