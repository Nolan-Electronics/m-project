﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject
{
    /// <summary>
    /// Static class to hold the layout and design preferences.
    /// </summary>
    static class Layoutinfo
    {
        public static bool darkmode;
        public static int fontsize;
        public static string? fontname;
        public static Font? font;
    }
}
