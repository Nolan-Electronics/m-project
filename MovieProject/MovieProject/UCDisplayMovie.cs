﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog;

namespace MovieProject
{
    public partial class ucDisplaymovie : UserControl
    {
        public ucDisplaymovie()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Global.mainToFront();
        }

        /// <summary>
        /// Design methods.
        /// </summary>
        /// <param name="color"></param>
        private void SetFormColor(Color color)
        {
            foreach (Label l in AllLabels())
            {
                l.BackColor = color;
            }
            btnExit.BackColor = color;
        }
        private void SetTextColor(Color color)
        {
            foreach (Label l in AllLabels())
            {
                l.ForeColor = color;
            }
        }
        private void SetFont(Font font)
        {
            foreach (Label l in AllLabels())
            {
                l.Font = font;
            }
        }

        /// <summary>
        /// Shortcut to get all the labels in the usercontrol.
        /// </summary>
        /// <returns></returns>
        private List<Label> AllLabels()
        {
            return new List<Label> { lblActors, lblActorsContent, lblDescContent, lblDirector, lblDirectorContent, lblID, lblIdContent, lblLength, lblLengthContent, lblName, lblNameContent, lblRating, lblRatingContent, lblReleasedate };
        }

        /// <summary>
        /// Fill the labels and set theyr design.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucDisplaymovie_VisibleChanged(object sender, EventArgs e)
        {
            if(this.Visible && Global.currentMovie != null)
            {
                lblNameContent.Text = Global.currentMovie.Name;
                lblIdContent.Text = Global.currentMovie.ID.ToString();
                lblRatingContent.Text = Global.currentMovie.Rating.ToString();
                lblDirectorContent.Text = Global.currentMovie.Director;
                lblLengthContent.Text = Global.currentMovie.Length.ToString();
                lblDescContent.Text = Global.currentMovie.Description;
                lblActorsContent.Text = "";
                lblReleasedate.Text = Global.currentMovie.Date.ToString();

                SearchDatabase search = new SearchDatabase(Global.currentMovie.Name);
                List<Person> actors = search.actorByMoviename();

                int index = 0;
                foreach (Person actor in actors)
                {
                    if (index >= actors.Count() - 1)
                    {
                        lblActorsContent.Text += actor.Name;
                        lblActorsContent.Text += ".";
                    }
                    else
                    {
                        lblActorsContent.Text += actor.Name;
                        lblActorsContent.Text += ", ";
                    }
                    index++;
                }

                //Retrieve movie info (id and thumbnail) with the in Global.currentMovie.ID stored id, take the thumbnail and convert the byte[] to an image.
                try
                {
                    Image img = Global.byteToImage(Database.RetrieveBLOBFromID(Global.currentMovie.ID, Global.tableSelected.ToLower()));
                    picboxThumbnail.Image = Global.resizeBitmap(img, 352, 528);
                }
                catch (Exception ex)
                {
                    Log.Error("Exception at retrieving thumbnail and converting into byte array: {ex}", ex);
                }
                if (Global.currentMovie.Name != null) { Global.changelblCaption(Global.currentMovie.Name); }
            }

            if (Layoutinfo.darkmode)
            {
                this.BackColor = ProjectColors.darkmode_background;
                btnExit.Image = Images.ReturnIcon_26_white;
                SetFormColor(ProjectColors.darkmode_fillcolor);
                SetTextColor(ProjectColors.darkmode_fontcolor);
            }
            else
            {
                this.BackColor = ProjectColors.lightmode_background;
                btnExit.Image = Images.ReturnIcon_26_black;
                SetFormColor(ProjectColors.lightmode_fillcolor);
                SetTextColor(ProjectColors.lightmode_fontcolor);
            }
            btnExit.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderReturnPurple;
            if (Layoutinfo.font != null) { SetFont(Layoutinfo.font); }
        }

    }
}
