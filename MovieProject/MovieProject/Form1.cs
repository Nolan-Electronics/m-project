using Serilog;
using System.Diagnostics;
using System.Text.Json;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;

namespace MovieProject
{
    public record class Prefs(
            bool darkmode,
            int fontsize,
            string fontname
    );
    public partial class Form1 : Form
    {
        private bool mouseDown;
        private Point lastLoc;
        bool allowResize = false;

        /// <summary>
        /// Initialise important global variables.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            Global.picboxIcon = picboxIcon;
            Global.lblCaption = lblCaption;
            Global.ucAddPerson = ucAddPerson1;
            Global.ucLogin = ucLogin1;
            Global.ucMain = ucMain1;
            Global.ucDisplayMovie = ucDisplaymovie1;
            Global.ucCreateEntry = ucCreateEntry1;
            Global.ucDisplayPerson = ucDisplayPerson1;
            Global.usercontrols = new List<UserControl>() { ucAddPerson1, ucLogin1, ucMain1, ucDisplaymovie1, ucCreateEntry1, ucDisplayPerson1 };
            Log.Information("Initialized Global.cs controls.");
            Global.loginToFront();

            try
            {

                Global.varDir = Directory.GetCurrentDirectory() + @"\var";
                Directory.CreateDirectory(Global.varDir);
                
            }
            catch 
            {
                Log.Error("Directory path not retrieved.");
            }
        }
        
        /// <summary>
        /// Exit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picboxClose_Click(object sender, EventArgs e)
        {
            savePreferences();
            Log.CloseAndFlush();
            Close();
        }
        /// <summary>
        /// Save preferences into json file.
        /// </summary>
        private void savePreferences()
        {
            Preferences preferences = new Preferences(Layoutinfo.darkmode, Layoutinfo.fontsize, Layoutinfo.fontname);
            var filename = Global.varDir+@"\preferences.json";
            JsonUtils.StreamWriteJson(preferences, filename);
            Log.Information("Saved preferences into json file {@path}.", filename);
        }

        /// <summary>
        /// Make the form draggable.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void obj_mouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLoc = e.Location;
        }
        private void obj_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLoc.X) + e.X, (this.Location.Y - lastLoc.Y) + e.Y);
                this.Update();
            }
        }
        private void obj_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        /// <summary>
        /// Hover-effect on close btn.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picboxClose_MouseEnter(object sender, EventArgs e)
        {
            picboxClose.BackColor = Color.Red;
        }
        private void picboxClose_MouseLeave(object sender, EventArgs e)
        {
            picboxClose.BackColor = Color.Maroon;

        }


        /// <summary>
        /// Try to read the preferences saved in a json file from a previous session.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            string jsonFileContent = "{\"Darkmode\":false,\"Fontsize\":12,\"Fontname\":\"Segeote UI\"}";
            try
            {
                jsonFileContent = File.ReadAllText(Global.varDir+@"\preferences.json");
            }
            catch
            {
                Log.Error("Could not read json-preferences file.");
            }

            Preferences? preferences = JsonSerializer.Deserialize<Preferences>(jsonFileContent);
            if (preferences != null && preferences.Fontname != null)
            {
                Log.Information("Preferences loaded: {@JsonInput}", $"{preferences.Darkmode}, {preferences.Fontsize}, {preferences.Fontname}");
                Layoutinfo.darkmode = preferences.Darkmode;
                Layoutinfo.fontsize = preferences.Fontsize;
                Layoutinfo.fontname = preferences.Fontname;
                Layoutinfo.font = new Font(preferences.Fontname, preferences.Fontsize);
                lblCaption.Font = new Font(preferences.Fontname, preferences.Fontsize);
            }           
        }

        /// <summary>
        /// This thing would allow resizing, but this feature is currently on ice.
        /// </summary>
        /// <param name="m"></param>
        [DebuggerStepThrough]
        protected override void WndProc(ref Message m)
        {
            const int RESIZE_HANDLE_SIZE = 10;
            if(allowResize && m.Msg == 0x0084)
            {
                base.WndProc(ref m);

                if ((int)m.Result == 0x01/*HTCLIENT*/)
                {
                    Point screenPoint = new Point(m.LParam.ToInt32());
                    Point clientPoint = this.PointToClient(screenPoint);
                    if (clientPoint.Y <= RESIZE_HANDLE_SIZE)
                    {
                        if (clientPoint.X <= RESIZE_HANDLE_SIZE)
                            m.Result = (IntPtr)13/*HTTOPLEFT*/ ;
                        else if (clientPoint.X < (Size.Width - RESIZE_HANDLE_SIZE))
                            m.Result = (IntPtr)12/*HTTOP*/ ;
                        else
                            m.Result = (IntPtr)14/*HTTOPRIGHT*/ ;
                    }
                    else if (clientPoint.Y <= (Size.Height - RESIZE_HANDLE_SIZE))
                    {
                        if (clientPoint.X <= RESIZE_HANDLE_SIZE)
                            m.Result = (IntPtr)10/*HTLEFT*/ ;
                        else if (clientPoint.X < (Size.Width - RESIZE_HANDLE_SIZE))
                            m.Result = (IntPtr)2/*HTCAPTION*/ ;
                        else
                            m.Result = (IntPtr)11/*HTRIGHT*/ ;
                    }
                    else
                    {
                        if (clientPoint.X <= RESIZE_HANDLE_SIZE)
                            m.Result = (IntPtr)16/*HTBOTTOMLEFT*/ ;
                        else if (clientPoint.X < (Size.Width - RESIZE_HANDLE_SIZE))
                            m.Result = (IntPtr)15/*HTBOTTOM*/ ;
                        else
                            m.Result = (IntPtr)17/*HTBOTTOMRIGHT*/ ;
                    }
                }
                return;
            }
            base.WndProc(ref m);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style |= 0x20000; // <--- use 0x20000
                return cp;
            }
        }
    }
}