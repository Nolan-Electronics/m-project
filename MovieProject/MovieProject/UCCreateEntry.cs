﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog;
using Windows.ApplicationModel.Payments;

namespace MovieProject
{
    public partial class ucCreateEntry : UserControl
    {
        string thumbnail_image_location = null;

        public ucCreateEntry()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Exit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            Log.Information("Leaving create entry mode/uc.");
            Global.mainToFront();
        }

        /// <summary>
        /// Show ucAddPerson to get new values.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddPerson_Click(object sender, EventArgs e)
        {
            Global.addPersonToFront();
        }

        /// <summary>
        /// Show ucAddPerson to get new values, overwrite the old ones.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOverwrite_Click(object sender, EventArgs e)
        {
            Global.addPerson_Actors = new List<Person>();
            Global.addPerson_Director = null;
            Global.addPersonToFront();
        }

        /// <summary>
        /// Full checks and inserts into the database plus retrieving new ids for other inserts.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            void resetFields()
            {
                txtboxName.Text = "";
                txtboxRating.Text = "";
                txtboxLength.Text = "";
                txtboxDesc.Text = "";
                dtpReleasedate.Value = DateTime.Now;
                lblDirectorName.Text = "Name";
                lblDirectorBirthdate.Text = "Birthdate";
                lblDirectorCountry.Text = "Origin";
                lboxActors.Items.Clear();
            }
            bool movieDuplicate(string name)
            {
                SearchDatabase searchDupli = new SearchDatabase(name);
                List<Movie> returned_movies_dupli = searchDupli.movieByName();

                if(returned_movies_dupli.Any())
                {
                    if (returned_movies_dupli[0] != null)
                    {
                        Global.displayErrorMessage($"A movie with the same name was found in the database! Its id/primary key is: {returned_movies_dupli[0].ID}. Please delete it or choose a different name!");
                        return true;
                    }
                }
                return false;
            }
            bool invalidRating(double rating)
            {
                if(rating < 0.0 || rating > 10.0)
                {
                    Global.displayErrorMessage($"The rating of value: {rating} is not in the correct range.");
                    return true;
                }
                return false;
            }
            bool noThumbnail()
            {
                if(thumbnail_image_location == null)
                {
                    Global.displayErrorMessage("Currently it is not possible to insert a movie into the database without a thumbnail, please select one.");
                    return true;
                }
                return false;
            }
            bool invalidLength(int length)
            {
                if(length < 0 || length > 1200)
                {
                    Global.displayErrorMessage($"The length of value {length} minutes is not in the correct range.");
                    return true;
                }
                return false;
            }
            bool tooBigName(string name)
            {
                if(name.Length > 250)
                {
                    Global.displayErrorMessage($"The choosen name has a length of {name.Length} characters which exceeds the limit of 250.");
                    return true;
                }
                return false;
            }
            bool tooBigDesc(string desc)
            {
                if (desc.Length > 250)
                {
                    Global.displayErrorMessage($"The choosen name has a length of {desc.Length} characters which exceeds the limit of 250.");
                    return true;
                }
                return false;
            }


            string movie_name = txtboxName.Text;
            string movie_desc = txtboxDesc.Text;
            DateTime movie_reldate = dtpReleasedate.Value;
            double movie_rating;
            int movie_length;
            if(!double.TryParse(txtboxRating.Text, out movie_rating))
            {
                Log.Information("Movie rating in create entry uc could not be parsed to variable from type double.");
                Global.displayErrorMessage("Field \"rating\" must be a number.");
                return;
            }
            if (!int.TryParse(txtboxLength.Text, out movie_length))
            {
                Log.Information("Movie length in create entry uc could not be parsed to variable from type int32.");
                Global.displayErrorMessage("Field \"length\" must be a number.");
                return;
            }
            
            //Check values for errors
  
            if(movieDuplicate(movie_name) || invalidRating(movie_rating) || noThumbnail() || invalidLength(movie_length) || tooBigName(movie_name) || tooBigDesc(movie_desc)) { Log.Information("Movie insertion aborted due to invalid values."); return;}
 
            int[] insDirValues = insertDirector();
            if (insDirValues[0] != 0)
            {
                Log.Error("Aborted the movie-create process due to a movie cast related error.");
                return;
            }
            int director_id = insDirValues[1];

            Movie movie_to_insert = new Movie(movie_name, movie_desc, movie_reldate, movie_rating, director_id, movie_length);
            Database.insertMovie(movie_to_insert);
            SearchDatabase s = new SearchDatabase(movie_name);
            List<Movie> returned_movies = new List<Movie>();
            int movie_id = 99;
            try
            {
                returned_movies = s.movieByName();
            }
            catch (Exception)
            {

                throw;
            }
            if(returned_movies.Any())
            {
                if (returned_movies[0] != null)
                {
                    movie_id = returned_movies[0].ID;
                }
            }

            int[] actor_ids= insertActors();
            if(actor_ids.Length == 0)
            {
                Log.Error("Error from creating actors makes it impossible to contine inserting into movies_actors table..");
                return;
            }

            for(int i = 0; i < actor_ids.Length; i++)
            {
              
                Database.insertMovActLink(movie_id, actor_ids[i]);
            }

            insertThumbnails(movie_id);

            resetFields();
            Global.mainToFront();
        }

        /// <summary>
        /// insert the thumbnails into the movie_thumbnails table.
        /// </summary>
        private void insertThumbnails(int movie_id)
        {
            byte[] image_bytes = new byte[0];

            if (File.Exists(thumbnail_image_location))
            {
                image_bytes = Global.imageToByte(thumbnail_image_location);
            }
            if (image_bytes.Length > 0)
            {
                Database.insertThumbnail(movie_id, image_bytes);
            }
        }

        /// <summary>
        /// Insert the director into the database. Returns an array with 2 values, first the exit code and then, if available, the directors id.
        /// </summary>
        /// <returns></returns>
        private int[] insertDirector()
        {
            if (Global.addPerson_Director != null)
            {
                SearchDatabase search = new SearchDatabase(Global.addPerson_Director.Name);
                List<Person> returned_persons = new List<Person>();
                Person? existingDirector = null;
                try
                {
                    returned_persons = search.directorByName();
                }
                catch (NullReferenceException)
                {
                    Log.Information("Director does not exist in the database so a new entry will be created.");
                }
                if (returned_persons.Any())
                {
                    existingDirector = returned_persons[0];
                }
                if (existingDirector == null)
                {
                    Database.insertDirector(Global.addPerson_Director);
                    SearchDatabase s = new SearchDatabase(Global.addPerson_Director.Name);
                    int? d_id = null;
                    try
                    {
                        returned_persons = s.directorByName();
                    }
                    catch (NullReferenceException)
                    {
                        Log.Error("Directors newly created ID could not be retrieved, it might not exist. Aborting..");
                    }
                    if (returned_persons.Any())
                    {
                        d_id = returned_persons[0].Id;
                        if (d_id != null) { return new int[2] { 0, (int)d_id }; }
                        else
                        {
                            Log.Error("Directors id is not available, aborting the inserting-process.");
                            return new int[2] { 2, 0 }; //Exit code 1 - Fatal, Id not available, abort the whole process.
                        }
                    }
                    else
                    {
                        Log.Error("Directors id is not available, aborting the inserting-process.");
                        return new int[2] { 2, 0 }; //Exit code 1 - Fatal, Id not available, abort the whole process.
                    } 
                }
                else
                {
                    return new int[2] { 0, 0 }; //Exit code 0 - OK - Director does not have to be created because it already is.
                }
            }
            else
            {
                Global.displayErrorMessage("No director is loaded, please add a movie cast.");
                Log.Warning("Tried to create a movie-entry in the database with no director loaded.");
                Log.Error("Director could not created, aborting the inserting-process.");
                return new int[2] { 1, 0 }; //Exit code 1 - Fatal, Director not created, abort the whole process.
            }
        }

        /// <summary>
        /// Insert the actor into the database. Returns an array with 2 values, first the exit code and then, if available, the directors id.
        /// </summary>
        /// <returns></returns>
        private int[] insertActors()
        {
            int[] actor_ids = new int[Global.addPerson_Actors.Count];
            int index = 0;
            List<Person> returned_persons = new List<Person>();
            foreach (Person actor in Global.addPerson_Actors)
            {
                Database.insertActor(actor);                
                SearchDatabase s = new SearchDatabase(actor.Name);
                returned_persons = s.actorByName();
                if (returned_persons.Any())
                {
                    if(returned_persons[0] == null)
                    {
                        Log.Error("Error at retrieving a newly created actors ID, Aborting..");
                        return new int[0];
                    }
                    if (returned_persons[0].Id == null)
                    {
                        Log.Error("Error at retrieving a newly created actors ID, Aborting..");
                        return new int[0];
                    }
                    actor_ids[index] = (int)returned_persons[0].Id;
                }
                index++;
            }
            return actor_ids;
        }

        /// <summary>
        /// Add the values from ucAddPerson.cs into this form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ucCreateEntry_VisibleChanged(object sender, EventArgs e)
        {
            if (Global.addPerson_Director != null && Global.addPerson_Actors.Any())
            {
                lboxActors.Items.Clear();
                lblDirectorName.Text = Global.addPerson_Director.Name;
                lblDirectorBirthdate.Text = Global.addPerson_Director.BirthdateDT.ToString("dd.MM.yyyy");
                lblDirectorCountry.Text = Global.addPerson_Director.Country;
                
                foreach (Person actor in Global.addPerson_Actors)
                {
                    lboxActors.Items.Add(actor.Name);
                }
                btnOverwrite.Enabled = true;
                btnOverwrite.Visible = true;
                btnAddPerson.Text = "Edit";
            }
            setLayoutDesign();
        }

        /// <summary>
        /// Set the layout design and font for this usercontrol.
        /// </summary>
        private void setLayoutDesign()
        {
            if(Layoutinfo.darkmode)
            {
                BackColor = ProjectColors.darkmode_background;
                setFormColor(ProjectColors.darkmode_fillcolor);
                setTextColor(ProjectColors.darkmode_fontcolor);
                btnExit.Image = Images.ReturnIcon_26_white;
                picboxThumbnail.Image = Images.OpenfileIcon_60_white;
                picboxDirectorImg.Image = Images.PersonIcon_75_white;
                pnlDirectorinfo.BackColor = ProjectColors.darkmode_background;
                pnlDirectorinfoToggle.BackColor = ProjectColors.darkmode_background;
            }
            else
            {
                BackColor = ProjectColors.lightmode_background;
                setFormColor(ProjectColors.lightmode_fillcolor);
                setTextColor(ProjectColors.lightmode_fontcolor);
                btnExit.Image = Images.ReturnIcon_26_black;
                picboxThumbnail.Image = Images.OpenfileIcon_60_black;
                picboxDirectorImg.Image = Images.PersonIcon_75_black;
                pnlDirectorinfo.BackColor = ProjectColors.lightmode_background;
                pnlDirectorinfoToggle.BackColor = ProjectColors.lightmode_background;
                //btnAddPerson.FlatAppearance.MouseOverBackColor = ProjectColors.lightmode_btnhover;
                //btnExit.FlatAppearance.MouseOverBackColor = ProjectColors.lightmode_btnhover;
                //btnOverwrite.FlatAppearance.MouseOverBackColor = ProjectColors.lightmode_btnhover;
                //btnSubmit.FlatAppearance.MouseOverBackColor = ProjectColors.lightmode_btnhover;
            }
            if (Layoutinfo.font != null) { setFont(Layoutinfo.font); }
            btnAddPerson.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderGreenlighter;
            btnExit.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderReturnPurple;
            btnOverwrite.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderRed;
            btnSubmit.FlatAppearance.MouseOverBackColor = ProjectColors.buttonBorderSubmitBlue;
        }
        private void setTextColor(Color color)
        {
            lblName.ForeColor = color;
            lblRating.ForeColor = color;
            lblReleasedate.ForeColor = color;
            lblLength.ForeColor = color;
            txtboxName.ForeColor = color;
            txtboxRating.ForeColor = color;
            dtpReleasedate.TextColor = color;
            txtboxLength.ForeColor = color;
            txtboxDesc.ForeColor = color;
            lboxActors.ForeColor = color;
            lblDirectorName.ForeColor = color;
            lblDirectorBirthdate.ForeColor = color;
            lblDirectorCountry.ForeColor = color;
            dtpReleasedate.BorderColor = color;
            btnAddPerson.ForeColor = color;
            btnOverwrite.ForeColor = color;
            btnSubmit.ForeColor = color;
        }
        private void setFormColor(Color color)
        {
            btnExit.BackColor = color;
            btnAddPerson.BackColor = color;
            btnSubmit.BackColor = color;
            btnOverwrite.BackColor = color;
            lblDirectorName.BackColor = color;
            lblDirectorCountry.BackColor = color;
            lblLength.BackColor = color;
            lblReleasedate.BackColor = color;
            lblName.BackColor = color;
            lblRating.BackColor = color;
            lblDirectorBirthdate.BackColor = color;
            picboxDirectorImg.BackColor = color;
            picboxThumbnail.BackColor = color;
            lboxActors.BackColor = color;
            txtboxName.BackColor = color;
            txtboxRating.BackColor = color;
            txtboxLength.BackColor = color;
            txtboxDesc.BackColor = color;
            dtpReleasedate.SkinColor = color;
        }
        private void setFont(Font font)
        {
            lblName.Font = font;
            lblRating.Font = font;
            lblReleasedate.Font = font;
            lblLength.Font = font;
            lblDirectorBirthdate.Font = font;
            lblDirectorCountry.Font = font;
            lblDirectorName.Font = font;
            txtboxDesc.Font = font;
            txtboxLength.Font = font;
            txtboxRating.Font = font;
            txtboxName.Font = font;
            lboxActors.Font = font;
            btnAddPerson.Font = font;
            btnOverwrite.Font = font;
            btnSubmit.Font = font;
            dtpReleasedate.Font = font;
        }

        /// <summary>
        /// Show the open-file-dialog to get the source of the thumbnail.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picboxThumbnail_Click(object sender, EventArgs e)
        {
            try
            {
                if(ofdThumbnail.ShowDialog() == DialogResult.OK)
                {
                    thumbnail_image_location = ofdThumbnail.FileName;
                    int picbox_width = picboxThumbnail.Width;
                    int picbox_height = picboxThumbnail.Height;
                    picboxThumbnail.Image = Global.resizeBitmap(Image.FromFile(thumbnail_image_location), picbox_width, picbox_height);
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception at searching for image/thumbnail: {@ex}", ex);
                return;
            }
        }
    }
}
