﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace MovieProject
{
    public static class JsonUtils
    {
        private static readonly JsonSerializerOptions _options =
            new() { DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull };

        public static void StreamWriteJson(object obj, string filename)
        {
            using var fileStream = File.Create(filename);
            using var utf8JsonWriter = new Utf8JsonWriter(fileStream);

            JsonSerializer.Serialize(utf8JsonWriter, obj);
        }
    }
}
