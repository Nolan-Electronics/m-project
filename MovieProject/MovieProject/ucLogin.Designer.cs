﻿namespace MovieProject
{
    partial class ucLogin
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.radbtnDarkm = new System.Windows.Forms.RadioButton();
            this.radbtnLightm = new System.Windows.Forms.RadioButton();
            this.txtboxServer = new System.Windows.Forms.TextBox();
            this.txtboxDB = new System.Windows.Forms.TextBox();
            this.txtboxPassword = new System.Windows.Forms.TextBox();
            this.txtboxUser = new System.Windows.Forms.TextBox();
            this.lblServer = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblDB = new System.Windows.Forms.Label();
            this.btnLogin = new MovieProject.ncButton();
            this.lblMessage = new System.Windows.Forms.Label();
            this.comboxFontname = new System.Windows.Forms.ComboBox();
            this.txtboxFontsize = new System.Windows.Forms.TextBox();
            this.lblPrefs = new System.Windows.Forms.Label();
            this.lblCreds = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // radbtnDarkm
            // 
            this.radbtnDarkm.AutoSize = true;
            this.radbtnDarkm.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radbtnDarkm.Location = new System.Drawing.Point(464, 101);
            this.radbtnDarkm.Name = "radbtnDarkm";
            this.radbtnDarkm.Size = new System.Drawing.Size(61, 25);
            this.radbtnDarkm.TabIndex = 30;
            this.radbtnDarkm.Text = "Dark";
            this.radbtnDarkm.UseVisualStyleBackColor = true;
            this.radbtnDarkm.CheckedChanged += new System.EventHandler(this.radbtnDarkm_CheckedChanged);
            // 
            // radbtnLightm
            // 
            this.radbtnLightm.AutoSize = true;
            this.radbtnLightm.Checked = true;
            this.radbtnLightm.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.radbtnLightm.Location = new System.Drawing.Point(544, 101);
            this.radbtnLightm.Name = "radbtnLightm";
            this.radbtnLightm.Size = new System.Drawing.Size(63, 25);
            this.radbtnLightm.TabIndex = 29;
            this.radbtnLightm.TabStop = true;
            this.radbtnLightm.Text = "Light";
            this.radbtnLightm.UseVisualStyleBackColor = true;
            this.radbtnLightm.CheckedChanged += new System.EventHandler(this.radbtnLightm_CheckedChanged);
            // 
            // txtboxServer
            // 
            this.txtboxServer.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxServer.Location = new System.Drawing.Point(540, 203);
            this.txtboxServer.Name = "txtboxServer";
            this.txtboxServer.Size = new System.Drawing.Size(254, 29);
            this.txtboxServer.TabIndex = 1;
            this.txtboxServer.Text = "localhost";
            this.txtboxServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtboxServer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UCLogin_KeyDown);
            // 
            // txtboxDB
            // 
            this.txtboxDB.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxDB.Location = new System.Drawing.Point(540, 369);
            this.txtboxDB.Name = "txtboxDB";
            this.txtboxDB.Size = new System.Drawing.Size(254, 29);
            this.txtboxDB.TabIndex = 4;
            this.txtboxDB.Text = "tmpro";
            this.txtboxDB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtboxDB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UCLogin_KeyDown);
            // 
            // txtboxPassword
            // 
            this.txtboxPassword.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxPassword.Location = new System.Drawing.Point(540, 311);
            this.txtboxPassword.Name = "txtboxPassword";
            this.txtboxPassword.Size = new System.Drawing.Size(254, 29);
            this.txtboxPassword.TabIndex = 3;
            this.txtboxPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtboxPassword.UseSystemPasswordChar = true;
            this.txtboxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UCLogin_KeyDown);
            // 
            // txtboxUser
            // 
            this.txtboxUser.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxUser.Location = new System.Drawing.Point(540, 259);
            this.txtboxUser.Name = "txtboxUser";
            this.txtboxUser.Size = new System.Drawing.Size(254, 29);
            this.txtboxUser.TabIndex = 2;
            this.txtboxUser.Text = "root";
            this.txtboxUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtboxUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UCLogin_KeyDown);
            // 
            // lblServer
            // 
            this.lblServer.BackColor = System.Drawing.SystemColors.Window;
            this.lblServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblServer.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblServer.Location = new System.Drawing.Point(276, 203);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(254, 29);
            this.lblServer.TabIndex = 35;
            this.lblServer.Text = "Server:";
            this.lblServer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUser
            // 
            this.lblUser.BackColor = System.Drawing.SystemColors.Window;
            this.lblUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblUser.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblUser.Location = new System.Drawing.Point(276, 259);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(254, 29);
            this.lblUser.TabIndex = 36;
            this.lblUser.Text = "User:";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPassword
            // 
            this.lblPassword.BackColor = System.Drawing.SystemColors.Window;
            this.lblPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPassword.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPassword.Location = new System.Drawing.Point(276, 311);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(254, 29);
            this.lblPassword.TabIndex = 37;
            this.lblPassword.Text = "Password:";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDB
            // 
            this.lblDB.BackColor = System.Drawing.SystemColors.Window;
            this.lblDB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDB.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblDB.Location = new System.Drawing.Point(276, 369);
            this.lblDB.Name = "lblDB";
            this.lblDB.Size = new System.Drawing.Size(254, 29);
            this.lblDB.TabIndex = 38;
            this.lblDB.Text = "Database:";
            this.lblDB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.White;
            this.btnLogin.BackgroundColor = System.Drawing.Color.White;
            this.btnLogin.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.btnLogin.BorderRadius = 10;
            this.btnLogin.BorderSize = 1;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnLogin.ForeColor = System.Drawing.Color.Black;
            this.btnLogin.Location = new System.Drawing.Point(276, 422);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(518, 40);
            this.btnLogin.TabIndex = 39;
            this.btnLogin.TabStop = false;
            this.btnLogin.Text = "Login";
            this.btnLogin.TextColor = System.Drawing.Color.Black;
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.Brown;
            this.lblMessage.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblMessage.ForeColor = System.Drawing.Color.White;
            this.lblMessage.Location = new System.Drawing.Point(0, 479);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(1070, 31);
            this.lblMessage.TabIndex = 40;
            this.lblMessage.Text = "Connection failed! :(";
            this.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMessage.Visible = false;
            // 
            // comboxFontname
            // 
            this.comboxFontname.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.comboxFontname.FormattingEnabled = true;
            this.comboxFontname.Location = new System.Drawing.Point(292, 63);
            this.comboxFontname.Name = "comboxFontname";
            this.comboxFontname.Size = new System.Drawing.Size(380, 29);
            this.comboxFontname.TabIndex = 41;
            // 
            // txtboxFontsize
            // 
            this.txtboxFontsize.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxFontsize.Location = new System.Drawing.Point(678, 63);
            this.txtboxFontsize.Name = "txtboxFontsize";
            this.txtboxFontsize.Size = new System.Drawing.Size(100, 29);
            this.txtboxFontsize.TabIndex = 42;
            // 
            // lblPrefs
            // 
            this.lblPrefs.BackColor = System.Drawing.Color.DimGray;
            this.lblPrefs.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblPrefs.ForeColor = System.Drawing.Color.White;
            this.lblPrefs.Location = new System.Drawing.Point(241, 22);
            this.lblPrefs.Name = "lblPrefs";
            this.lblPrefs.Size = new System.Drawing.Size(588, 28);
            this.lblPrefs.TabIndex = 43;
            this.lblPrefs.Text = "Preferences";
            this.lblPrefs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblPrefs.Visible = false;
            // 
            // lblCreds
            // 
            this.lblCreds.BackColor = System.Drawing.Color.DimGray;
            this.lblCreds.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblCreds.ForeColor = System.Drawing.Color.White;
            this.lblCreds.Location = new System.Drawing.Point(241, 162);
            this.lblCreds.Name = "lblCreds";
            this.lblCreds.Size = new System.Drawing.Size(588, 27);
            this.lblCreds.TabIndex = 44;
            this.lblCreds.Text = "Credentials";
            this.lblCreds.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCreds.Visible = false;
            // 
            // ucLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.lblCreds);
            this.Controls.Add(this.lblPrefs);
            this.Controls.Add(this.txtboxFontsize);
            this.Controls.Add(this.comboxFontname);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lblDB);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblServer);
            this.Controls.Add(this.txtboxUser);
            this.Controls.Add(this.txtboxPassword);
            this.Controls.Add(this.txtboxDB);
            this.Controls.Add(this.txtboxServer);
            this.Controls.Add(this.radbtnDarkm);
            this.Controls.Add(this.radbtnLightm);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ucLogin";
            this.Size = new System.Drawing.Size(1070, 628);
            this.VisibleChanged += new System.EventHandler(this.ucLogin_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UCLogin_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RadioButton radbtnDarkm;
        private RadioButton radbtnLightm;
        private TextBox txtboxServer;
        private TextBox txtboxDB;
        private TextBox txtboxPassword;
        private TextBox txtboxUser;
        private Label lblServer;
        private Label lblUser;
        private Label lblPassword;
        private Label lblDB;
        private ncButton btnLogin;
        private Label lblMessage;
        private ComboBox comboxFontname;
        private TextBox txtboxFontsize;
        private Label lblPrefs;
        private Label lblCreds;
    }
}
