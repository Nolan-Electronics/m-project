using Serilog;

namespace MovieProject
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        ///  Logger gets configured here.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string path = Directory.GetCurrentDirectory() + @"\var";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                //Remove debug in a production environement:
                .WriteTo.File(path + @"\log-debug.txt", rollingInterval: RollingInterval.Hour)
                //Information level can but does not have to be kept:
                .WriteTo.File(path+@"\log-information.txt", rollingInterval: RollingInterval.Hour, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information)
                //Keep error level for easier support and understaning of errors:
                .WriteTo.File(path+@"\log-important.txt", rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Warning)
                .CreateLogger();

            ApplicationConfiguration.Initialize();
            Application.Run(new Form1());
        }
    }
}