﻿namespace MovieProject
{
    partial class ucAddPerson
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboxPersons = new MovieProject.ncCombobox();
            this.lboxPersons = new System.Windows.Forms.ListBox();
            this.btnAddPerson = new MovieProject.ncButton();
            this.btnRemovePerson = new MovieProject.ncButton();
            this.lblName = new System.Windows.Forms.Label();
            this.txtboxName = new System.Windows.Forms.TextBox();
            this.lblBirthdate = new System.Windows.Forms.Label();
            this.txtboxOrigin = new System.Windows.Forms.TextBox();
            this.lblOrigin = new System.Windows.Forms.Label();
            this.btnSubmit = new MovieProject.ncButton();
            this.btnExit = new MovieProject.ncButton();
            this.dtpBirthdate = new MovieProject.NCDatePicker();
            this.SuspendLayout();
            // 
            // comboxPersons
            // 
            this.comboxPersons.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboxPersons.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.comboxPersons.BorderSize = 1;
            this.comboxPersons.Caption = "";
            this.comboxPersons.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.comboxPersons.ForeColor = System.Drawing.Color.DimGray;
            this.comboxPersons.IconColor = System.Drawing.Color.Black;
            this.comboxPersons.Items.AddRange(new object[] {
            "Directors",
            "Actors"});
            this.comboxPersons.ListBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(228)))), ((int)(((byte)(245)))));
            this.comboxPersons.ListTextColor = System.Drawing.Color.Black;
            this.comboxPersons.Location = new System.Drawing.Point(50, 172);
            this.comboxPersons.MinimumSize = new System.Drawing.Size(200, 30);
            this.comboxPersons.Name = "comboxPersons";
            this.comboxPersons.Padding = new System.Windows.Forms.Padding(1);
            this.comboxPersons.Size = new System.Drawing.Size(287, 40);
            this.comboxPersons.TabIndex = 33;
            this.comboxPersons.TabStop = false;
            // 
            // lboxPersons
            // 
            this.lboxPersons.BackColor = System.Drawing.SystemColors.Window;
            this.lboxPersons.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lboxPersons.FormattingEnabled = true;
            this.lboxPersons.ItemHeight = 21;
            this.lboxPersons.Location = new System.Drawing.Point(387, 109);
            this.lboxPersons.Name = "lboxPersons";
            this.lboxPersons.Size = new System.Drawing.Size(633, 46);
            this.lboxPersons.TabIndex = 42;
            this.lboxPersons.TabStop = false;
            // 
            // btnAddPerson
            // 
            this.btnAddPerson.BackColor = System.Drawing.Color.White;
            this.btnAddPerson.BackgroundColor = System.Drawing.Color.White;
            this.btnAddPerson.BorderColor = System.Drawing.Color.Lime;
            this.btnAddPerson.BorderRadius = 10;
            this.btnAddPerson.BorderSize = 1;
            this.btnAddPerson.FlatAppearance.BorderSize = 0;
            this.btnAddPerson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddPerson.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAddPerson.ForeColor = System.Drawing.Color.Black;
            this.btnAddPerson.Location = new System.Drawing.Point(189, 499);
            this.btnAddPerson.Name = "btnAddPerson";
            this.btnAddPerson.Size = new System.Drawing.Size(100, 40);
            this.btnAddPerson.TabIndex = 43;
            this.btnAddPerson.TabStop = false;
            this.btnAddPerson.Text = "Add";
            this.btnAddPerson.TextColor = System.Drawing.Color.Black;
            this.btnAddPerson.UseVisualStyleBackColor = false;
            this.btnAddPerson.Click += new System.EventHandler(this.btnAddPerson_Click);
            // 
            // btnRemovePerson
            // 
            this.btnRemovePerson.BackColor = System.Drawing.Color.White;
            this.btnRemovePerson.BackgroundColor = System.Drawing.Color.White;
            this.btnRemovePerson.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.btnRemovePerson.BorderRadius = 10;
            this.btnRemovePerson.BorderSize = 1;
            this.btnRemovePerson.FlatAppearance.BorderSize = 0;
            this.btnRemovePerson.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemovePerson.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnRemovePerson.ForeColor = System.Drawing.Color.Black;
            this.btnRemovePerson.Location = new System.Drawing.Point(50, 499);
            this.btnRemovePerson.Name = "btnRemovePerson";
            this.btnRemovePerson.Size = new System.Drawing.Size(100, 40);
            this.btnRemovePerson.TabIndex = 44;
            this.btnRemovePerson.TabStop = false;
            this.btnRemovePerson.Text = "Remove";
            this.btnRemovePerson.TextColor = System.Drawing.Color.Black;
            this.btnRemovePerson.UseVisualStyleBackColor = false;
            this.btnRemovePerson.Click += new System.EventHandler(this.btnRemovePerson_Click);
            // 
            // lblName
            // 
            this.lblName.BackColor = System.Drawing.Color.White;
            this.lblName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblName.Location = new System.Drawing.Point(50, 295);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(287, 29);
            this.lblName.TabIndex = 46;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtboxName
            // 
            this.txtboxName.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxName.Location = new System.Drawing.Point(387, 295);
            this.txtboxName.Name = "txtboxName";
            this.txtboxName.Size = new System.Drawing.Size(633, 29);
            this.txtboxName.TabIndex = 47;
            // 
            // lblBirthdate
            // 
            this.lblBirthdate.BackColor = System.Drawing.Color.White;
            this.lblBirthdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblBirthdate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblBirthdate.Location = new System.Drawing.Point(50, 352);
            this.lblBirthdate.Name = "lblBirthdate";
            this.lblBirthdate.Size = new System.Drawing.Size(287, 29);
            this.lblBirthdate.TabIndex = 48;
            this.lblBirthdate.Text = "Birthdate";
            this.lblBirthdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtboxOrigin
            // 
            this.txtboxOrigin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxOrigin.Location = new System.Drawing.Point(387, 409);
            this.txtboxOrigin.Name = "txtboxOrigin";
            this.txtboxOrigin.Size = new System.Drawing.Size(74, 29);
            this.txtboxOrigin.TabIndex = 51;
            // 
            // lblOrigin
            // 
            this.lblOrigin.BackColor = System.Drawing.Color.White;
            this.lblOrigin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblOrigin.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblOrigin.Location = new System.Drawing.Point(50, 409);
            this.lblOrigin.Name = "lblOrigin";
            this.lblOrigin.Size = new System.Drawing.Size(287, 29);
            this.lblOrigin.TabIndex = 50;
            this.lblOrigin.Text = "Origin (ISO 3166 ALPHA-2)";
            this.lblOrigin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSubmit
            // 
            this.btnSubmit.BackColor = System.Drawing.Color.White;
            this.btnSubmit.BackgroundColor = System.Drawing.Color.White;
            this.btnSubmit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(215)))), ((int)(((byte)(255)))));
            this.btnSubmit.BorderRadius = 10;
            this.btnSubmit.BorderSize = 1;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSubmit.ForeColor = System.Drawing.Color.Black;
            this.btnSubmit.Location = new System.Drawing.Point(387, 499);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(633, 40);
            this.btnSubmit.TabIndex = 54;
            this.btnSubmit.TabStop = false;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.TextColor = System.Drawing.Color.Black;
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.White;
            this.btnExit.BackgroundColor = System.Drawing.Color.White;
            this.btnExit.BorderColor = System.Drawing.Color.MediumPurple;
            this.btnExit.BorderRadius = 10;
            this.btnExit.BorderSize = 1;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnExit.ForeColor = System.Drawing.Color.Black;
            this.btnExit.Image = global::MovieProject.Images.ReturnIcon_26_black;
            this.btnExit.Location = new System.Drawing.Point(50, 102);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(105, 53);
            this.btnExit.TabIndex = 57;
            this.btnExit.TextColor = System.Drawing.Color.Black;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // dtpBirthdate
            // 
            this.dtpBirthdate.BorderColor = System.Drawing.Color.Black;
            this.dtpBirthdate.BorderSize = 1;
            this.dtpBirthdate.Font = new System.Drawing.Font("Segoe UI", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.dtpBirthdate.Location = new System.Drawing.Point(387, 352);
            this.dtpBirthdate.MinimumSize = new System.Drawing.Size(0, 35);
            this.dtpBirthdate.Name = "dtpBirthdate";
            this.dtpBirthdate.Size = new System.Drawing.Size(249, 35);
            this.dtpBirthdate.SkinColor = System.Drawing.Color.White;
            this.dtpBirthdate.TabIndex = 67;
            this.dtpBirthdate.TextColor = System.Drawing.Color.Black;
            // 
            // ucAddPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.dtpBirthdate);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtboxOrigin);
            this.Controls.Add(this.lblOrigin);
            this.Controls.Add(this.lblBirthdate);
            this.Controls.Add(this.txtboxName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnRemovePerson);
            this.Controls.Add(this.btnAddPerson);
            this.Controls.Add(this.lboxPersons);
            this.Controls.Add(this.comboxPersons);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ucAddPerson";
            this.Size = new System.Drawing.Size(1070, 628);
            this.Load += new System.EventHandler(this.ucAddPerson_Load);
            this.VisibleChanged += new System.EventHandler(this.ucAddPerson_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ncCombobox comboxPersons;
        private ListBox lboxPersons;
        private ncButton btnAddPerson;
        private ncButton btnRemovePerson;
        private Label lblName;
        private TextBox txtboxName;
        private Label lblBirthdate;
        private TextBox txtboxOrigin;
        private Label lblOrigin;
        private ncButton btnSubmit;
        private ncButton btnExit;
        private NCDatePicker dtpBirthdate;
    }
}
