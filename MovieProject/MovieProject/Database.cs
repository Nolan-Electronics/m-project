﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using Serilog;
using System.Windows.Forms;
using Microsoft.VisualBasic.Devices;
using System.Reflection.Metadata;

namespace MovieProject
{
    static class Database
    {
        private static MySqlConnection? connection;
        private static DataTable? dt;
        private static MySqlDataAdapter? sda;

        /// <summary>
        /// Get the users input and set up a connection string. Uses a ping to check the connectivity.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="dbname"></param>
        /// <returns></returns>
        public static bool EstablishConnection(String server, String user, String password, String dbname)
        {
            try
            {
                MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
                if (server == "localhost") { builder.Server = "127.0.0.1"; }
                else { builder.Server = server; }
                builder.UserID = user;
                builder.Password = password;
                builder.Database = dbname;
                builder.SslMode = MySqlSslMode.Required;
                String builderstr = builder.ToString();
                connection = new MySqlConnection(builderstr);

                connection.Open();
                if (!connection.Ping())
                {
                    connection.Close();
                    return false;
                }
                else
                {
                    Global.connectedToDB = true;
                    connection.Close();
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                Log.Error("Cannot connect to database with credentials: {server}, {@user}, {@dbname}. Got exception: {@ex}", server, user, dbname, ex);
                return false;
            }
        }

        /// <summary>
        /// Execute query with command.
        /// </summary>
        /// <param name="action"></param>
        public static MySqlCommand? execSqlQuery(Action<MySqlCommand> action)
        {
            using (connection)
            {
                if (connection != null)
                {
                    try
                    {
                        connection.Open();
                        using (MySqlCommand command = connection.CreateCommand())
                        {
                            action(command);
                            return command;
                        }
                    }
                    catch (MySql.Data.MySqlClient.MySqlException mysqlex)
                    {
                        Log.Error("Mysql-Exception at last possible point to insert into database: {@ex}", mysqlex);
                        return null;
                    }
                    catch (System.Exception ex)
                    {
                        Log.Error("Exception at last possible point to insert into database: {@ex}", ex);
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Execute query with command-action as parameter.
        /// SQL-Injection safe version.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static MySqlCommand? execSqlQueryISafe(MySqlCommand command)
        {
            using (connection)
            {
                if (connection != null)
                {
                    try
                    {
                        connection.Open();
                        using (command)
                        {
                            command.ExecuteNonQuery();
                            return command;
                        }
                    }
                    catch (MySql.Data.MySqlClient.MySqlException mysqlex)
                    {
                        Log.Error("MySqlexception at last possible point to insert into database: {@ex}", mysqlex);
                        return null;

                    }
                    catch (System.Exception ex)
                    {
                        Log.Error("Exception at last possible point to insert into database: {@ex}", ex);
                        return null;

                    }
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Insert into a table.
        /// </summary>
        /// <param name="movie"></param>
        public static void insertMovie(Movie movie)
        {
            var query = "INSERT INTO movies(movie_name, movie_releasedate, movie_rating, movie_directorid, movie_description, movie_length) VALUES(@movie_name, @movie_releasedate, @movie_rating, @movie_director_id, @movie_description, @movie_length)";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@movie_name", movie.Name);
            command.Parameters.AddWithValue("@movie_releasedate", movie.Releasedate);
            command.Parameters.AddWithValue("@movie_rating", movie.Rating);
            command.Parameters.AddWithValue("@movie_director_id", movie.DirectorID);
            command.Parameters.AddWithValue("@movie_description", movie.Description);
            command.Parameters.AddWithValue("@movie_length", movie.Length);
            execSqlQueryISafe(command);
        }
        public static void insertMovActLink(int movie_id, int actor_id)
        {
            var query = "INSERT INTO movies_actors(movie_id, actor_id) VALUES(@movie_id, @actor_id)";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@movie_id", movie_id);
            command.Parameters.AddWithValue("@actor_id", actor_id);
            execSqlQueryISafe(command);
        }
        public static void insertDirector(Person director)
        {
            var query = "INSERT INTO directors(director_name, director_birthdate, director_country) VALUES(@director_name, @director_birthdate, @director_country)";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@director_name", director.Name);
            command.Parameters.AddWithValue("@director_birthdate", director.BirthdateDT);
            command.Parameters.AddWithValue("@director_country", director.Country);
            execSqlQueryISafe(command);
        }
        public static void insertActor(Person actor)
        {
            var query = "INSERT INTO actors(actor_name, actor_birthdate, actor_country) VALUES(@actor_name, @actor_birthdate, @actor_country)";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@actor_name", actor.Name);
            command.Parameters.AddWithValue("@actor_birthdate", actor.BirthdateDT);
            command.Parameters.AddWithValue("@actor_country", actor.Country);
            execSqlQueryISafe(command);
        }
        public static void insertThumbnail(int movie_id, byte[] thumbnail)
        {
            var query = "INSERT INTO movies_thumbnails(movie_id, movie_thumbnail) VALUES(@movie_id, @movie_thumbnail)";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@movie_id", movie_id);
            command.Parameters.AddWithValue("@movie_thumbnail", thumbnail);
            execSqlQueryISafe(command);
        }


        /// <summary>
        /// Delete an entry by its ID.
        /// </summary>
        /// <param name="id"></param>
        public static void deleteMovieEntry(int id)
        {
            var query = "DELETE FROM tmpro.movies WHERE movie_id=@movie_id";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@movie_id", id);
            execSqlQueryISafe(command);
        }
        public static void deleteMoviesactorsEntry(int id)
        {
            var query = "DELETE FROM tmpro.movies_actors WHERE movie_id = @movie_id";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@movie_id", id);
            execSqlQueryISafe(command);
        }
        public static void deleteDirectorEntry(int id)
        {
            var query = "DELETE from tmpro.directors where director_id=@director_id";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@director_id", id);
            execSqlQueryISafe(command);
        }
        public static void deleteActorEntry(int id)
        {
            var query = "DELETE from tmpro.actors where actor_id = @actor_id";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@actor_id", id);
            execSqlQueryISafe(command);
        }
        public static void deleteThumbEntry(int id)
        {
            var query = "DELETE FROM tmpro.movies_thumbnails WHERE movie_id = @movie_id";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@movie_id", id);
            execSqlQueryISafe(command);
        }

        public static void deleteDirectorProfileEntry(int id)
        {
            var query = "DELETE FROM tmpro.directors_profiles WHERE director_id = @director_id";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@director_id", id);
            execSqlQueryISafe(command);
        }
        public static void deleteActorProfileEntry(int id)
        {
            var query = "DELETE FROM tmpro.actors_profiles WHERE actor_id = @actor_id";
            MySqlCommand command = new MySqlCommand(query, connection);
            command.Parameters.AddWithValue("@actor_id", id);
            execSqlQueryISafe(command);
        }
        /// <summary>
        /// Action of executing the query definded by the SearchDatabase.cs class.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static List<Movie> movieRetAction(string query)
        {
            DataTable dt;
            MySqlDataAdapter sda;
            MySqlCommand? cmd;
            List<Movie> movies = new List<Movie>();
            bool movieInit = false;
            try
            {
                cmd = execSqlQuery(
                    command =>
                    {
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                    });
                if (cmd != null)
                {
                    dt = new DataTable();
                    sda = new MySqlDataAdapter(cmd);
                    sda.Fill(dt);
                    foreach (DataRow dr in dt.Rows)
                    {
                        try
                        {
                            string? name = dr["movie_name"].ToString();
                            int id = (int)dr["movie_id"];
                            string? date = dr["movie_releasedate"].ToString().Split(' ')[0];
                            double rating = (double)dr["movie_rating"];
                            string? director = dr["movie_directorname"].ToString();
                            string? desc = dr["movie_description"].ToString();
                            int length = (int)dr["movie_length"];
                            byte[] thumbnail = (byte[])dr["movie_thumbnail"];
                            if (name != null && desc != null && date != null && director != null && thumbnail.Length > 0)
                            {
                                movies.Add(new Movie(name, desc, date, id, rating, director, length, thumbnail));
                                movieInit = true;
                            }
                            else
                            {
                                Log.Error("Movie retrival failed due to fields with a value of null.");
                                return new List<Movie>();
                            }
                        }
                        catch (InvalidCastException inc)
                        {
                            Log.Error("Invalid cast at reading DataRows from retrieved DataTable. Database might use wrong datatypes.");
                            return new List<Movie>();
                        }

                    }
                    if (movieInit) { return movies; }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error("Movie retrieval failed due to exception: {@ex}", ex);
            }
            return new List<Movie>();
        }
        public static List<Person> personRetAction(string query, string type)
        {
            DataTable dt;
            MySqlDataAdapter sda;
            MySqlCommand? cmd;
            List<Person> persons = new List<Person>();
            bool foundPerson = false;
            try
            {
                cmd = Database.execSqlQuery(
                    command =>
                    {
                        command.CommandText = query;
                        command.ExecuteNonQuery();
                    });
                if (cmd != null)
                {
                    dt = new DataTable();
                    sda = new MySqlDataAdapter(cmd);
                    sda.Fill(dt);

                    foreach (DataRow dr in dt.Rows)
                    {
                        int id = (int)dr[$"{type}_id"];
                        string? name = dr[$"{type}_name"].ToString();
                        string? country = dr[$"{type}_country"].ToString();
                        string? birthdate = dr[$"{type}_birthdate"].ToString();
                        byte[] profile = (byte[])dr[$"{type}_profile"];

                        if (name != null && country != null && birthdate != null && profile.Length > 0)
                        {
                            persons.Add(new Person(id, name, birthdate, country, profile));
                            foundPerson = true;
                        }
                        else
                        {
                            Log.Error("Retrieval of person failed due to field with a value of null.");
                            return new List<Person>();
                        }
                    }
                    if (foundPerson) { return persons; }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error("Person retrieval failed due to exception: ", "@exception");
            }
            return new List<Person>();
        }
        /// <summary>
        /// Get all the movies in the database.
        /// </summary>
        /// <returns></returns>
        public static String[] RetrieveMovie()
        {
            MySqlCommand? cmd;
            Log.Information("Retrieving all movies...");
            cmd = execSqlQuery(
                    command =>
                    {
                        switch (Global.tableSelected)
                        {
                            case "Movies":
                                command.CommandText = "Select movie_name from moviebase.movies";
                                break;
                            case "Directors":
                                command.CommandText = "Select director_name from moviebase.directors";
                                break;
                            case "Actors":
                                command.CommandText = "Select actor_name from moviebase.actors";
                                break;
                        }
                        command.ExecuteNonQuery();
                    }
            );
            String[] names = null;
            if (cmd != null)
            {
                dt = new DataTable();
                sda = new MySqlDataAdapter(cmd);
                sda.Fill(dt);
                names = new string[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0] != null)
                    {
                        names[i] = dt.Rows[i][0].ToString();
                    }
                    else
                    {
                        i--;
                    }
                }
                return names;
            }
            else
            {
                Log.Error("Movie could not be retrieved due to MySqlCommand with value null");
                return new string[0];
            }

        }
        /// <summary>
        /// Retrieve the person's ID by its name.
        /// </summary>
        /// <param name="person"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public static int RetrieveID(String person_name, int mode)
        {
            MySqlCommand command;
            Log.Information("Retrieving an ID...");
            int person_id = -1;
            var query = "";
            if (mode == (int)Global.RETIDFOR.DIRECTOR)
            {
                query = "SELECT directors.director_id FROM directors WHERE directors.director_name = @director_name LIMIT 1";
                command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("@director_name", person_name);
            }
            else if (mode == (int)Global.RETIDFOR.ACTOR)
            {
                query = "SELECT actors.actor_id FROM actors WHERE actors.actor_name = @actor_name LIMIT 1";
                command = new MySqlCommand(query, connection);
                command.Parameters.AddWithValue("@ctor_name", person_name);
            }
            else { return -1; }
            if (command != null)
            {
                MySqlCommand? returned_command = execSqlQueryISafe(command);
                if (returned_command != null)
                {
                    try
                    {
                        dt = new DataTable();
                        sda = new MySqlDataAdapter(returned_command);
                        sda.Fill(dt);
                        person_id = (int)dt.Rows[0][0];
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Error/Exception in retrieval of persons ID: {@ex}", ex);
                        return -1;
                    }

                }
                else
                {
                    Log.Error("Error in retrieval of persons ID: MySqlCommand to return was null");
                }
                return person_id;
            }
            else
            {
                Log.Error("Error in retrieval of persons ID: MySqlCommand to execute was null");
                return -1;
            }
        }
        /// <summary>
        /// Get the ammount of records. Note that this method returnes a long.
        /// </summary>
        /// <returns></returns>
        public static long getAmmountOfRecs(string table)
        {
            string query;
            if (table == "movies") { query = "SELECT COUNT(*) FROM movies"; }
            else if (table == "directors") { query = "SELECT COUNT(*) FROM directors"; }
            else if (table == "actors") { query = "SELECT COUNT(*) FROM actors"; }
            else { Log.Error("Wrong table selected for ret ammount of records."); return 1; }

            if (connection != null)
            {
                try
                {
                    connection.Open();
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = query;
                        long result = (long)command.ExecuteScalar();
                        connection.Close();
                        return result;
                    }
                }
                catch (NullReferenceException e)
                {
                    Log.Error("Null reference at trying to get the ammounts of records: {0}", e);
                    return 0;
                }
                catch (InvalidCastException)
                {
                    Log.Error("Invalid cast, ExecScalar returned result was not a long. (GetAmmoundOfRecs)");
                    return 0;
                }
                catch (Exception ex) { Log.Error("Exception caught in GetAmmountOfRecs: {@ex}", ex); return 0; }
            }
            else
            {
                Log.Error("Connection was null at trying to get the ammount of records.");
                return 0;
            }
        }
        
        /// <summary>
        /// Retrieve the thumbnail as a BLOB from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static byte[] RetrieveBLOBFromID(int id, string table)
        {
            //Not optimal but functional:
            var query = "SELECT movie_thumbnail FROM movies_thumbnails WHERE movie_id=@id";
            switch(table)
            {
                case "movies":
                    query = "SELECT movie_thumbnail FROM movies_thumbnails WHERE movie_id=@id";
                    break;
                case "directors":
                    query = "SELECT director_profile FROM directors_profiles WHERE director_id=@id";
                    break;
                case "actors":
                    query = "SELECT actor_profile FROM actors_profiles WHERE actor_id=@id";
                    break;
            }


            using (MySqlCommand command = new MySqlCommand(query, connection))
            {
                command.Parameters.AddWithValue("@id", id);
                if (connection != null)
                {
                    connection.Open();
                    byte[] blob_bytes = new byte[0];
                    try
                    {
                        blob_bytes = (byte[])command.ExecuteScalar();
                    }
                    catch (InvalidCastException)
                    {
                        Log.Error("Invalid cast trying to get a byte[] from a ExecScalar method.");
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Exception in executing scalar for blob retrieval: {@ex}", ex);
                    }
                    connection.Close();
                    return blob_bytes;
                }
                else
                {
                    Log.Error("Connection was null but expected valid at RetrieveBLOBFromID.");
                    return new byte[0];
                }
            }
        }
    }
}
