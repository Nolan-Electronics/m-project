﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace MovieProject
{
    static class Global
    {
        public static Label loginMessageLbl;
        public static bool connectedToDB = false;
        public static ncCombobox comboxTbl;
        public static string tableSelected;
        public static PictureBox picboxIcon;
        public static Label lblCaption;

        public static UserControl? ucMain = null;
        public static UserControl? ucLogin = null;
        public static UserControl? ucAddPerson = null;
        public static UserControl? ucDisplayMovie = null;
        public static UserControl? ucCreateEntry = null;
        public static UserControl? ucDisplayPerson = null;
        public static List<UserControl>? usercontrols;
        
        public static bool iDSearchEnabled = false;
        public static string varDir;
        public static bool pagesCalculated;

        public static Movie? currentMovie = null;
        public static Person? currentPerson = null;

        //Used by ucAddPerson.cs to deliver information for proccessing in ucCreateEntry.cs
        public static Person? addPerson_Director = null;
        public static List<Person> addPerson_Actors = new List<Person>();

        public enum SEARCH : int
        {
            ID = 1,
            LENGTH = 2,
            RATING = 3,
            NAME = 4,
            DIRECTOR = 5,
            ACTOR = 6
        }

        //Fits the index of the combobox used to select the table in the UI.
        //public enum TABLESELECTED : int
        //{
        //    MOVIES = 0,
        //    DIRECTORS = 1,
        //    ACTORS = 2
        //}

        public enum RETIDFOR : int
        {
            DIRECTOR = 1,
            ACTOR = 2
        }
        /// <summary>
        /// Set the table to show.
        /// </summary>
        public static void setDefaultTable()
        {
            comboxTbl.SelectedItem = "Movies";
            if (comboxTbl.SelectedItem.ToString() != null)
            {
                tableSelected = comboxTbl.SelectedItem.ToString();
            } 
        }
        
        public static void changeIconToDirector()
        {
            picboxIcon.Image = Images.DirectorIcon_25_black;
        }
        public static void changeIconToActor()
        {
            picboxIcon.Image = Images.ActorIcon_25_black;
        }
        public static void changeIconToMovie()
        {
            picboxIcon.Image = Images.MoviesIcon_25_black;
        }
        public static void changelblCaption(string str)
        {
            lblCaption.Text = str;
        }

        private static bool isUsercontrolNull(List<UserControl> uclist)
        {
            bool ret = false;
            foreach (UserControl u in uclist)
            {
                if (u == null) { ret = true; }
            }
            return ret;
        }
        /// <summary>
        /// Change visibility of all usercontrols. Part of one of the ...ToFront() methods.
        /// </summary>
        /// <param name="target"></param>
        private static void changeState(UserControl target)
        {
            foreach (UserControl uc in usercontrols)
            {
                if (uc == target)
                {
                    uc.Enabled = true;
                    uc.Visible = true;
                }
                else
                {
                    uc.Visible = false;
                    uc.Enabled = false;
                }
            }
        }
        /// <summary>
        /// Bring one the Usercontrol to the front and send the others to the last place in the z-order.
        /// </summary>
        public static void loginToFront()
        {
            if(!isUsercontrolNull(usercontrols))
            {
                ucAddPerson.SendToBack();
                ucMain.SendToBack();
                ucLogin.BringToFront();
                ucDisplayMovie.SendToBack();
                ucCreateEntry.SendToBack();
                ucDisplayPerson.SendToBack();
                changeState(ucLogin);
                changelblCaption("Login");
            }
        }
        public static void mainToFront()
        {
            if (!isUsercontrolNull(usercontrols))
            {
                ucMain.BringToFront();
                ucLogin.SendToBack();
                ucAddPerson.SendToBack();
                ucDisplayMovie.SendToBack();
                ucCreateEntry.SendToBack();
                ucDisplayPerson.SendToBack();
                changeState(ucMain);
                changelblCaption("The Movie Project");
            }
        }
        public static void addPersonToFront()
        {
            if (!isUsercontrolNull(usercontrols))
            {
                ucAddPerson.BringToFront();
                ucLogin.SendToBack();
                ucMain.SendToBack();
                ucDisplayMovie.SendToBack();
                ucDisplayPerson.SendToBack();
                ucCreateEntry.SendToBack();
                changeState(ucAddPerson);
                changelblCaption("Movie Cast");
            }
        }
        public static void displayMovieToFront()
        {
            if (!isUsercontrolNull(usercontrols))
            {
                ucDisplayMovie.BringToFront();
                ucAddPerson.SendToBack();
                ucMain.SendToBack();
                ucLogin.SendToBack();
                ucDisplayPerson.SendToBack();
                ucCreateEntry.SendToBack();
                changeState(ucDisplayMovie);
                //Caption label is set in the ucDisplayMovie to the name of the movie.
            }
        }
        
        public static void createEntryToFront()
        {
            if (!isUsercontrolNull(usercontrols))
            {
                ucDisplayMovie.SendToBack();
                ucAddPerson.SendToBack();
                ucMain.SendToBack();
                ucLogin.SendToBack();
                ucDisplayPerson.SendToBack();
                ucCreateEntry.BringToFront();
                changeState(ucCreateEntry);
                changelblCaption("Create a Movie");
            }
        }

        public static void DisplayPersonToFront()
        {
            if(!isUsercontrolNull(usercontrols))
            {
                ucDisplayMovie.SendToBack();
                ucAddPerson.SendToBack();
                ucMain.SendToBack();
                ucLogin.SendToBack();
                ucCreateEntry.SendToBack();
                ucDisplayPerson.BringToFront();
                changeState(ucDisplayPerson);
                //Caption label is set in the UCDisplayPerson to the name of the person.
            }
        }

        /// <summary>
        /// Takes a BLOB and returns an image.
        /// </summary>
        /// <param name="blob"></param>
        /// <returns></returns>
        public static Bitmap? byteToImage(byte[] blob)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                try
                {
                    byte[] pData = blob;
                    ms.Write(pData, 0, Convert.ToInt32(pData.Length));
                    Bitmap image = new Bitmap(ms, false);
                    return image;
                }
                catch
                {
                    Log.Error("Could not convert bytes to image (Thumbnail or profile picture). Byte[] was probably corrupted.");
                    return null;
                }
            }
        }

        /// <summary>
        /// Takes the location of a image file, reads it with a filestream, gets the binary data and then reads the binary data into an array.
        /// </summary>
        /// <param name="fileLocation"></param>
        /// <returns></returns>
        public static Byte[] imageToByte(string fileLocation)
        {
            try
            {
                byte[] image_data;
                FileStream fileStream = new FileStream(fileLocation, FileMode.Open, FileAccess.Read);
                BinaryReader binaryreader = new BinaryReader(fileStream);
                image_data = binaryreader.ReadBytes((int)fileStream.Length);
                binaryreader.Close();
                fileStream.Close();
                return image_data;
            }
            catch (Exception ex)
            {
                Log.Error("Exception in converting a image from location/path into a byte array: {@ex}", ex);
                return new byte[0];
            }
        }

        /// <summary>
        /// High-quality resize:
        /// Might be to heavy on ressources, could be replaced by a lighter resizing-method.
        /// </summary>
        public static Bitmap resizeBitmap(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);
            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var g = Graphics.FromImage(destImage))
            {
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(System.Drawing.Drawing2D.WrapMode.TileFlipXY);
                    g.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }

        public static bool isValidMovie(Movie movie)
        {
            return true;
        }

        public static void displayErrorMessage(string m_message)
        {
            MessageBoxButtons m_buttons = MessageBoxButtons.OK;
            string m_title = "Error!";
            MessageBox.Show(m_message, m_title, m_buttons);
        }

        public static bool checkDateFormat(string bd)
        {
            string[] splitted = bd.Split('-');
            if (splitted.Length != 3)
            {
                Global.displayErrorMessage("The format of the birthdate is wrong, please use the us birthdate format (yyyy-mm-dd).");
                return false;
            }
            if (splitted[0].Length < 1 || splitted[0].Length > 4)
            {
                Global.displayErrorMessage("The format/length of the year in birthdate is wrong.");
                return false;
            }
            if ((splitted[1].Length < 1 || splitted[1].Length > 2) || (splitted[2].Length < 1 || splitted[2].Length > 2))
            {
                Global.displayErrorMessage("The format/length of the month or day in birthdate is wrong.");
                return false;
            }
            return true;
        }

    }
}
