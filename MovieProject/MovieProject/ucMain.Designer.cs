﻿namespace MovieProject
{
    partial class ucMain
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCreate = new MovieProject.ncButton();
            this.btnSelect = new MovieProject.ncButton();
            this.btnReload = new MovieProject.ncButton();
            this.comboxTables = new MovieProject.ncCombobox();
            this.btnSearch = new MovieProject.ncButton();
            this.txtboxSearch = new System.Windows.Forms.TextBox();
            this.timerDisplayCreateError = new System.Windows.Forms.Timer(this.components);
            this.timerFillListbox = new System.Windows.Forms.Timer(this.components);
            this.btnOptions = new MovieProject.ncButton();
            this.pnlImagecontainer = new System.Windows.Forms.Panel();
            this.pnlPageNumbers = new System.Windows.Forms.Panel();
            this.btnDelete = new MovieProject.ncButton();
            this.SuspendLayout();
            // 
            // btnCreate
            // 
            this.btnCreate.BackColor = System.Drawing.Color.White;
            this.btnCreate.BackgroundColor = System.Drawing.Color.White;
            this.btnCreate.BorderColor = System.Drawing.Color.Lime;
            this.btnCreate.BorderRadius = 10;
            this.btnCreate.BorderSize = 1;
            this.btnCreate.FlatAppearance.BorderSize = 0;
            this.btnCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnCreate.ForeColor = System.Drawing.Color.Black;
            this.btnCreate.Location = new System.Drawing.Point(378, 79);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(314, 40);
            this.btnCreate.TabIndex = 38;
            this.btnCreate.TabStop = false;
            this.btnCreate.Text = "Create Entry";
            this.btnCreate.TextColor = System.Drawing.Color.Black;
            this.btnCreate.UseVisualStyleBackColor = false;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.White;
            this.btnSelect.BackgroundColor = System.Drawing.Color.White;
            this.btnSelect.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.btnSelect.BorderRadius = 10;
            this.btnSelect.BorderSize = 1;
            this.btnSelect.FlatAppearance.BorderSize = 0;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSelect.ForeColor = System.Drawing.Color.Black;
            this.btnSelect.Image = global::MovieProject.Images.SelectIcon_26_black;
            this.btnSelect.Location = new System.Drawing.Point(720, 79);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(50, 40);
            this.btnSelect.TabIndex = 37;
            this.btnSelect.TabStop = false;
            this.btnSelect.TextColor = System.Drawing.Color.Black;
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnReload
            // 
            this.btnReload.BackColor = System.Drawing.Color.White;
            this.btnReload.BackgroundColor = System.Drawing.Color.White;
            this.btnReload.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.btnReload.BorderRadius = 10;
            this.btnReload.BorderSize = 1;
            this.btnReload.FlatAppearance.BorderSize = 0;
            this.btnReload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReload.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnReload.ForeColor = System.Drawing.Color.Black;
            this.btnReload.Image = global::MovieProject.Images.ReloadIcon_26_black;
            this.btnReload.Location = new System.Drawing.Point(35, 79);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(314, 40);
            this.btnReload.TabIndex = 34;
            this.btnReload.TabStop = false;
            this.btnReload.TextColor = System.Drawing.Color.Black;
            this.btnReload.UseVisualStyleBackColor = false;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // comboxTables
            // 
            this.comboxTables.BackColor = System.Drawing.Color.WhiteSmoke;
            this.comboxTables.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.comboxTables.BorderSize = 1;
            this.comboxTables.Caption = "";
            this.comboxTables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.comboxTables.ForeColor = System.Drawing.Color.DimGray;
            this.comboxTables.IconColor = System.Drawing.Color.Black;
            this.comboxTables.Items.AddRange(new object[] {
            "Movies",
            "Directors",
            "Actors"});
            this.comboxTables.ListBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(228)))), ((int)(((byte)(245)))));
            this.comboxTables.ListTextColor = System.Drawing.Color.Black;
            this.comboxTables.Location = new System.Drawing.Point(35, 33);
            this.comboxTables.MinimumSize = new System.Drawing.Size(200, 30);
            this.comboxTables.Name = "comboxTables";
            this.comboxTables.Padding = new System.Windows.Forms.Padding(1);
            this.comboxTables.Size = new System.Drawing.Size(314, 40);
            this.comboxTables.TabIndex = 32;
            this.comboxTables.TabStop = false;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.White;
            this.btnSearch.BackgroundColor = System.Drawing.Color.White;
            this.btnSearch.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.btnSearch.BorderRadius = 10;
            this.btnSearch.BorderSize = 1;
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.ForeColor = System.Drawing.Color.Black;
            this.btnSearch.Image = global::MovieProject.Images.SearchIcon_23_black;
            this.btnSearch.Location = new System.Drawing.Point(720, 33);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(91, 29);
            this.btnSearch.TabIndex = 31;
            this.btnSearch.TabStop = false;
            this.btnSearch.TextColor = System.Drawing.Color.Black;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtboxSearch
            // 
            this.txtboxSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtboxSearch.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtboxSearch.Location = new System.Drawing.Point(378, 33);
            this.txtboxSearch.Name = "txtboxSearch";
            this.txtboxSearch.Size = new System.Drawing.Size(314, 29);
            this.txtboxSearch.TabIndex = 27;
            this.txtboxSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtboxSearch_KeyDown);
            // 
            // timerDisplayCreateError
            // 
            this.timerDisplayCreateError.Interval = 1000;
            // 
            // timerFillListbox
            // 
            this.timerFillListbox.Interval = 20000;
            // 
            // btnOptions
            // 
            this.btnOptions.BackColor = System.Drawing.Color.White;
            this.btnOptions.BackgroundColor = System.Drawing.Color.White;
            this.btnOptions.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.btnOptions.BorderRadius = 10;
            this.btnOptions.BorderSize = 1;
            this.btnOptions.FlatAppearance.BorderSize = 0;
            this.btnOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOptions.ForeColor = System.Drawing.Color.Black;
            this.btnOptions.Image = global::MovieProject.Images.Setup_23_black;
            this.btnOptions.Location = new System.Drawing.Point(828, 34);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(91, 29);
            this.btnOptions.TabIndex = 52;
            this.btnOptions.TabStop = false;
            this.btnOptions.TextColor = System.Drawing.Color.Black;
            this.btnOptions.UseVisualStyleBackColor = false;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // pnlImagecontainer
            // 
            this.pnlImagecontainer.BackColor = System.Drawing.Color.AliceBlue;
            this.pnlImagecontainer.Location = new System.Drawing.Point(35, 200);
            this.pnlImagecontainer.Margin = new System.Windows.Forms.Padding(0);
            this.pnlImagecontainer.Name = "pnlImagecontainer";
            this.pnlImagecontainer.Size = new System.Drawing.Size(1000, 328);
            this.pnlImagecontainer.TabIndex = 53;
            this.pnlImagecontainer.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlImagecontainer_Paint);
            this.pnlImagecontainer.Resize += new System.EventHandler(this.pnlImagecontainer_Resize);
            // 
            // pnlPageNumbers
            // 
            this.pnlPageNumbers.AutoScroll = true;
            this.pnlPageNumbers.Location = new System.Drawing.Point(35, 161);
            this.pnlPageNumbers.Margin = new System.Windows.Forms.Padding(0);
            this.pnlPageNumbers.Name = "pnlPageNumbers";
            this.pnlPageNumbers.Size = new System.Drawing.Size(1000, 39);
            this.pnlPageNumbers.TabIndex = 54;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.White;
            this.btnDelete.BackgroundColor = System.Drawing.Color.White;
            this.btnDelete.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.btnDelete.BorderRadius = 10;
            this.btnDelete.BorderSize = 1;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(787, 79);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(248, 40);
            this.btnDelete.TabIndex = 55;
            this.btnDelete.TabStop = false;
            this.btnDelete.Text = "Delete Entry";
            this.btnDelete.TextColor = System.Drawing.Color.Black;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // ucMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.pnlPageNumbers);
            this.Controls.Add(this.pnlImagecontainer);
            this.Controls.Add(this.btnOptions);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.comboxTables);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.txtboxSearch);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ucMain";
            this.Size = new System.Drawing.Size(1070, 628);
            this.Load += new System.EventHandler(this.ucMain_Load);
            this.VisibleChanged += new System.EventHandler(this.ucMain_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private ncButton btnCreate;
        private ncButton btnSelect;
        private ncButton btnReload;
        private ncCombobox comboxTables;
        private ncButton btnSearch;
        private TextBox txtboxSearch;
        private System.Windows.Forms.Timer timerDisplayCreateError;
        private System.Windows.Forms.Timer timerFillListbox;
        private ncButton btnOptions;
        private Panel pnlImagecontainer;
        private Panel pnlPageNumbers;
        private ncButton btnDelete;
    }
}
