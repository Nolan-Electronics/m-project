﻿namespace MovieProject
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlTopbar = new System.Windows.Forms.Panel();
            this.picboxIcon = new System.Windows.Forms.PictureBox();
            this.picboxClose = new System.Windows.Forms.PictureBox();
            this.lblCaption = new System.Windows.Forms.Label();
            this.ucMain1 = new MovieProject.ucMain();
            this.ucAddPerson1 = new MovieProject.ucAddPerson();
            this.ucLogin1 = new MovieProject.ucLogin();
            this.ucDisplaymovie1 = new MovieProject.ucDisplaymovie();
            this.ucCreateEntry1 = new MovieProject.ucCreateEntry();
            this.ucDisplayPerson1 = new MovieProject.UCDisplayPerson();
            this.pnlTopbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picboxIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picboxClose)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTopbar
            // 
            this.pnlTopbar.Controls.Add(this.picboxIcon);
            this.pnlTopbar.Controls.Add(this.picboxClose);
            this.pnlTopbar.Controls.Add(this.lblCaption);
            this.pnlTopbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopbar.Location = new System.Drawing.Point(0, 0);
            this.pnlTopbar.Name = "pnlTopbar";
            this.pnlTopbar.Size = new System.Drawing.Size(1070, 32);
            this.pnlTopbar.TabIndex = 2;
            // 
            // picboxIcon
            // 
            this.picboxIcon.BackColor = System.Drawing.Color.DodgerBlue;
            this.picboxIcon.Dock = System.Windows.Forms.DockStyle.Left;
            this.picboxIcon.ErrorImage = global::MovieProject.Images.LoginIcon_25_black;
            this.picboxIcon.Image = global::MovieProject.Images.LoginIcon_25_white;
            this.picboxIcon.Location = new System.Drawing.Point(0, 0);
            this.picboxIcon.Name = "picboxIcon";
            this.picboxIcon.Size = new System.Drawing.Size(31, 32);
            this.picboxIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picboxIcon.TabIndex = 4;
            this.picboxIcon.TabStop = false;
            // 
            // picboxClose
            // 
            this.picboxClose.BackColor = System.Drawing.Color.Maroon;
            this.picboxClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.picboxClose.Image = global::MovieProject.Images.CloseIcon_23_white;
            this.picboxClose.Location = new System.Drawing.Point(1038, 0);
            this.picboxClose.Name = "picboxClose";
            this.picboxClose.Size = new System.Drawing.Size(32, 32);
            this.picboxClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picboxClose.TabIndex = 2;
            this.picboxClose.TabStop = false;
            this.picboxClose.Click += new System.EventHandler(this.picboxClose_Click);
            this.picboxClose.MouseEnter += new System.EventHandler(this.picboxClose_MouseEnter);
            this.picboxClose.MouseLeave += new System.EventHandler(this.picboxClose_MouseLeave);
            // 
            // lblCaption
            // 
            this.lblCaption.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblCaption.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCaption.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblCaption.ForeColor = System.Drawing.Color.White;
            this.lblCaption.Location = new System.Drawing.Point(0, 0);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(1070, 32);
            this.lblCaption.TabIndex = 1;
            this.lblCaption.Text = "Login";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCaption.MouseDown += new System.Windows.Forms.MouseEventHandler(this.obj_mouseDown);
            this.lblCaption.MouseMove += new System.Windows.Forms.MouseEventHandler(this.obj_MouseMove);
            this.lblCaption.MouseUp += new System.Windows.Forms.MouseEventHandler(this.obj_MouseUp);
            // 
            // ucMain1
            // 
            this.ucMain1.BackColor = System.Drawing.SystemColors.Control;
            this.ucMain1.Location = new System.Drawing.Point(0, 32);
            this.ucMain1.Margin = new System.Windows.Forms.Padding(0);
            this.ucMain1.Name = "ucMain1";
            this.ucMain1.Size = new System.Drawing.Size(1070, 628);
            this.ucMain1.TabIndex = 3;
            this.ucMain1.Visible = false;
            // 
            // ucAddPerson1
            // 
            this.ucAddPerson1.AutoSize = true;
            this.ucAddPerson1.BackColor = System.Drawing.SystemColors.Control;
            this.ucAddPerson1.Location = new System.Drawing.Point(0, 32);
            this.ucAddPerson1.Margin = new System.Windows.Forms.Padding(0);
            this.ucAddPerson1.Name = "ucAddPerson1";
            this.ucAddPerson1.Size = new System.Drawing.Size(1070, 628);
            this.ucAddPerson1.TabIndex = 4;
            this.ucAddPerson1.Visible = false;
            // 
            // ucLogin1
            // 
            this.ucLogin1.BackColor = System.Drawing.SystemColors.Control;
            this.ucLogin1.Location = new System.Drawing.Point(0, 32);
            this.ucLogin1.Margin = new System.Windows.Forms.Padding(0);
            this.ucLogin1.Name = "ucLogin1";
            this.ucLogin1.Size = new System.Drawing.Size(1070, 628);
            this.ucLogin1.TabIndex = 5;
            // 
            // ucDisplaymovie1
            // 
            this.ucDisplaymovie1.BackColor = System.Drawing.SystemColors.Control;
            this.ucDisplaymovie1.Location = new System.Drawing.Point(0, 32);
            this.ucDisplaymovie1.Margin = new System.Windows.Forms.Padding(0);
            this.ucDisplaymovie1.Name = "ucDisplaymovie1";
            this.ucDisplaymovie1.Size = new System.Drawing.Size(1070, 628);
            this.ucDisplaymovie1.TabIndex = 6;
            this.ucDisplaymovie1.Visible = false;
            // 
            // ucCreateEntry1
            // 
            this.ucCreateEntry1.BackColor = System.Drawing.SystemColors.Control;
            this.ucCreateEntry1.Location = new System.Drawing.Point(0, 32);
            this.ucCreateEntry1.Margin = new System.Windows.Forms.Padding(0);
            this.ucCreateEntry1.Name = "ucCreateEntry1";
            this.ucCreateEntry1.Size = new System.Drawing.Size(1070, 628);
            this.ucCreateEntry1.TabIndex = 7;
            this.ucCreateEntry1.Visible = false;
            // 
            // ucDisplayPerson1
            // 
            this.ucDisplayPerson1.Location = new System.Drawing.Point(0, 32);
            this.ucDisplayPerson1.Margin = new System.Windows.Forms.Padding(0);
            this.ucDisplayPerson1.Name = "ucDisplayPerson1";
            this.ucDisplayPerson1.Size = new System.Drawing.Size(1070, 628);
            this.ucDisplayPerson1.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1070, 660);
            this.Controls.Add(this.ucDisplayPerson1);
            this.Controls.Add(this.ucCreateEntry1);
            this.Controls.Add(this.ucLogin1);
            this.Controls.Add(this.ucDisplaymovie1);
            this.Controls.Add(this.ucAddPerson1);
            this.Controls.Add(this.ucMain1);
            this.Controls.Add(this.pnlTopbar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlTopbar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picboxIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picboxClose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Panel pnlTopbar;
        private PictureBox picboxIcon;
        private PictureBox picboxClose;
        private Label lblCaption;
        private ucMain ucMain1;
        private ucAddPerson ucAddPerson1;
        private ucLogin ucLogin1;
        private ucDisplaymovie ucDisplaymovie1;
        private ucCreateEntry ucCreateEntry1;
        private UCDisplayPerson ucDisplayPerson1;
    }
}