﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace MovieProject
{
    internal class SearchDatabase
    {
        String? valueString;
        double? valueDouble;
        int? valueInt;
        DateTime? valueDateTime;

        public SearchDatabase(string s)
        {
            valueString = s;
        }
         
        public SearchDatabase(int? i)
        {
            valueInt = i;
        }

        public SearchDatabase(double? d) 
        {
            valueDouble = d;
        }

        public SearchDatabase(DateTime dt) 
        {
            valueDateTime = dt;
        }

        public List<Movie> movieByName()
        {
            string query = $"SELECT movies.movie_id, movies.movie_name, movies.movie_description, movies.movie_length, movies.movie_rating, movies.movie_releasedate, movies.movie_name, movie_directorname, movie_thumbnail FROM movies INNER JOIN movies_thumbnails ON movies.movie_id = movies_thumbnails.movie_id WHERE movies.movie_name = \"{valueString}\"";
            return Database.movieRetAction(query);
        }
        public List<Movie> movieByDirector()
        {
            string query = $"SELECT movies.movie_id, movies.movie_name, movies.movie_description, movies.movie_length, movies.movie_rating, movies.movie_releasedate, movie_directorname, movie_thumbnail FROM movies INNER JOIN movies_thumbnails ON movies.movie_id = movies_thumbnails.movie_id WHERE movies.movie_directorid = {valueInt}";
            return Database.movieRetAction(query);
        }
        public List<Movie> movieByActor()
        {
            string query = $"SELECT movies.movie_id, movies.movie_name, movies.movie_description, movies.movie_length, movies.movie_rating, movies.movie_releasedate, movie_directorname, movie_thumbnail FROM movies INNER JOIN movies_actors ON movies.movie_id=movies_actors.movie_id INNER JOIN movies_thumbnails ON movies.movie_id = movies_thumbnails.movie_id WHERE movies_actors.actor_id = {valueInt}";
            return Database.movieRetAction(query);
        }
        public List<Movie> movieByID()
        {
            string query = $"SELECT movies.movie_id, movies.movie_name, movies.movie_description, movies.movie_length, movies.movie_rating, movies.movie_releasedate, movies.movie_directorname, movie_thumbnail FROM movies INNER JOIN movies_thumbnails ON movies.movie_id = movies_thumbnails.movie_id WHERE movies.movie_id = {valueInt}  limit 1";
            return Database.movieRetAction(query);
        }
        public List<Movie> movieByRating()
        {
            string query = $"SELECT movies.movie_id, movies.movie_name, movies.movie_description, movies.movie_length, movies.movie_rating, movies.movie_releasedate, movies.movie_name, movie_thumbnail FROM movies INNER JOIN movies_thumbnails ON movies.movie_id = movies_thumbnails.movie_id WHERE movies.movie_rating = {valueDouble}";
            return Database.movieRetAction(query);
        }
        public List<Movie> movieByLength()
        {
            string query = $"SELECT movies.movie_id, movies.movie_name, movies.movie_description, movies.movie_length, movies.movie_rating, movies.movie_releasedate, movies.movie_name, movie_thumbnail FROM movies INNER JOIN movies_thumbnails ON movies.movie_id = movies_thumbnails.movie_id  WHERE movies.movie_length = {valueInt}";
            return Database.movieRetAction(query);
        }
        public List<Person> directorByName()
        {
            string query = $"Select director_name, directors.director_id, director_birthdate, director_country, directors_profiles.director_profile from directors INNER JOIN directors_profiles ON directors.director_id = directors_profiles.director_id WHERE director_name = \"{valueString}\"";
            return Database.personRetAction(query, "director");
        }
        public List<Person> actorByName()
        {
            string query = $"Select actor_name, actors.actor_id, actor_birthdate, actor_country, actors_profiles.actor_profile from actors INNER JOIN actors_profiles ON actors.actor_id = actors_profiles.actor_id WHERE actor_name = \"{valueString}\"";
            return Database.personRetAction(query, "actor");
        }
        public List<Person> actorByMoviename()
        {
            string query = $"SELECT actors.actor_name, actors.actor_id, actors.actor_birthdate, actors.actor_country, actors_profiles.actor_profile FROM actors INNER JOIN actors_profiles ON actors.actor_id = actors_profiles.actor_id INNER JOIN movies_actors ON movies_actors.actor_id=actors.actor_id INNER JOIN movies ON movies_actors.movie_id = movies.movie_id WHERE movies.movie_name = \"{valueString}\"";
           return Database.personRetAction(query, "actor");
        }
        public List<Person> directorByMoviename()
        {
            string query = $"SELECT directors.director_name, directors.director_id, directors.director_birthdate, directors.director_country, directors_profiles.director_profile FROM directors INNER JOIN directors_profiles ON directors.director_id = directors_profiles.director_id INNER JOIN movies ON movies.movie_directorid = directors.director_id WHERE movies.movie_name = \"{valueString}\"";
            return Database.personRetAction(query, "director");
        }
        public List<Person> directorById()
        {
            string query = $"SELECT directors.director_name, directors.director_id, directors.director_birthdate, directors.director_country, directors_profiles.director_profile FROM directors INNER JOIN directors_profiles ON directors.director_id = directors_profiles.director_id WHERE directors.director_id = {valueInt}";
            return Database.personRetAction(query, "director");
        }
        public List<Person> actorById()
        {
            string query = $"SELECT actors.actor_name, actors.actor_id, actors.actor_birthdate, actors.actor_country, actors_profiles.actor_profile FROM actors INNER JOIN actors_profiles ON actors.actor_id = actors_profiles.actor_id WHERE actors.actor_id = {valueInt}";
            return Database.personRetAction(query, "actor");
        }

        public List<Person> actorByBirthdate()
        {
            string query = $"SELECT actors.actor_name, actors.actor_id, actors.actor_birthdate, actors.actor_country, actors_profiles.actor_profile FROM actors INNER JOIN actors_profiles ON actors.actor_id = actors_profiles.actor_id WHERE actors.actor_birthdate = \"{valueDateTime}\"";
            return Database.personRetAction(query, "actor");
        }
        public List<Person> actorByOrigin()
        {
            string query = $"SELECT actors.actor_name, actors.actor_id, actors.actor_birthdate, actors.actor_country, actors_profiles.actor_profile FROM actors INNER JOIN actors_profiles ON actors.actor_id = actors_profiles.actor_id WHERE actors.actor_country = \"{valueString}\"";
            return Database.personRetAction(query, "actor");
        }

        public List<Person> directorByBirthdate()
        {
            string query = $"SELECT directors.director_name, directors.director_id, directors.director_birthdate, directors.director_country, directors_profiles.director_profile FROM directors INNER JOIN directors_profiles ON directors.director_id = directors_profiles.director_id WHERE directors.director_birthdate = \"{valueDateTime}\"";
            return Database.personRetAction(query, "director");
        }

        public List<Person> directorByOrigin()
        {
            string query = $"SELECT directors.director_name, directors.director_id, directors.director_birthdate, directors.director_country, directors_profiles.director_profile FROM directors INNER JOIN directors_profiles ON directors.director_id = directors_profiles.director_id WHERE directors.director_country = \"{valueString}\"";
            return Database.personRetAction(query, "director");
        }
    }
}
