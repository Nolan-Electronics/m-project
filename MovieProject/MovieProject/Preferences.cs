﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject
{
    internal class Preferences
    {
        public Preferences(bool Darkmode, int Fontsize, string Fontname)
        {
            this.Darkmode = Darkmode;
            this.Fontsize = Fontsize;
            this.Fontname = Fontname;
        }
        public bool Darkmode { get; set; }
        public int Fontsize { get; set; }
        public string? Fontname { get; set; }
    }
}
