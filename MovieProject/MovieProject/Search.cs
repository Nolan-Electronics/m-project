﻿using System;
using System.Collections.Generic;
using Google.Protobuf.WellKnownTypes;
using Serilog;


namespace MovieProject
{
    internal class Search
    {
        /// <summary>
        /// Crossing to redirect to precise search queries.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<Movie> SearchForMovie(string value)
        {
            double searchNumb;

            if (double.TryParse(value, out searchNumb))
            {
                return Number_movie_search(searchNumb);
            }
            else if (value != null)
            {
                return String_movie_search(value);
            }
            else { return new List<Movie>(); }
        }
        public List<Person> SearchForPerson(string value, bool isTargetDirector)
        {
            if(isTargetDirector)
            {
                return String_director_search(value);
            }
            else
            {
                return String_actor_search(value);
            }
        }

        /// <summary>
        /// Do the actual search by trial and error.. 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private List<Movie> String_movie_search(string value)
        {
            Log.Information("Movie search-value detected as a text");

            SearchDatabase search = new SearchDatabase(value);
            List<Movie> movTriedByName = search.movieByName();
            if (movTriedByName.Any())
            {
                return movTriedByName;
            }
            int directorId = Database.RetrieveID(value, (int)Global.RETIDFOR.DIRECTOR);
            search = new SearchDatabase(directorId);
            List<Movie> movTriedByDirector = search.movieByDirector();
            if (movTriedByDirector.Any())
            {
                return movTriedByDirector;
            }
            int actorId = Database.RetrieveID(value, (int)Global.RETIDFOR.ACTOR);
            search = new SearchDatabase(actorId);
            List<Movie> movTriedByActor = search.movieByActor();
            if (movTriedByActor.Any())
            {
                return movTriedByActor;
            }
            Log.Error("No text-based results found for value {@value}", value);
            return new List<Movie>();
        }
        private List<Movie> Number_movie_search(double value)
        {
            Log.Information("Movie search-value detected as a number.");
            SearchDatabase search = new SearchDatabase(value);
            List<Movie> movieList = search.movieByRating();
            if (movieList.Any()) { return movieList; }
            search = new SearchDatabase((int)value);
            movieList = search.movieByLength();
            if (movieList.Any()) { return movieList; }
            Log.Error("No numeric based results found on movies for {@value}", value.ToString());
            return new List<Movie>();
        }
        /// <summary>
        /// Same for directors
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private List<Person> String_director_search(string value)
        {
            SearchDatabase search = new SearchDatabase(value);
            List<Person> personList = search.directorByName();
            if (personList.Any())
            {
                return personList;
            }
            personList = search.directorByOrigin();
            if (personList.Any())
            {
                return personList;
            }
            personList = search.directorByMoviename();
            if (personList.Any())
            {
                return personList;
            }
            DateTime birthdate = new DateTime();
            try
            {
                birthdate = DateTime.Parse(value);
            }
            catch (FormatException)
            {
                Log.Debug("Formatexception at parsing string into DateTime for last try of search.");
            }
            search = new SearchDatabase(birthdate);
            personList = search.directorByBirthdate();
            if (personList.Any())
            {
                return personList;
            }
            Log.Error("No text-based results found on directors for value {@value}", value);
            return new List<Person>();
        }
        /// <summary>
        /// And actors as well.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private List<Person> String_actor_search(string value)
        {
            SearchDatabase search = new SearchDatabase(value);
            List<Person> personList = search.directorByName();
            if (personList.Any())
            {
                return personList;
            }
            personList = search.actorByOrigin();
            if (personList.Any())
            {
                return personList;
            }
            personList = search.actorByMoviename();
            if (personList.Any())
            {
                return personList;
            }
            DateTime birthdate = new DateTime();
            try
            {
                birthdate = DateTime.Parse(value);
            }
            catch (FormatException)
            {
                Log.Debug("Formatexception at parsing string into DateTime for last try of search.");
            }
            search = new SearchDatabase(birthdate);
            personList = search.actorByBirthdate();
            if (personList.Any())
            {
                return personList;
            }
            Log.Error("No text-based results found on actors§ for value {@value}", value);
            return new List<Person>();
        }

    }
}
