﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieProject
{
    internal class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Birthdate { get; set; }   //Only used for retrieved movie/informations from the db. Will be changed to Datetime soon.
        public DateTime BirthdateDT { get; set; }
        public string Country { get; set; }

        public byte[] Profilepic { get; set; }

        public Person(string name)
        {
            Name = name;

            //Empties:
            Id = 0;
            Birthdate = "";
            BirthdateDT = DateTime.Now;
            Country = "";
            Profilepic = new byte[0];
        }
        public Person(int id, byte[] profilepic) 
        { 
            Id = id;
            Profilepic = profilepic;

            //Empties:
            Name = "";
            Birthdate = "";
            BirthdateDT = DateTime.Now;
            Country = "";
        }
        public Person(string name, string birthdate, string country)    //Only used for retrieved movie/informations from the db. Will be changed to Datetime soon.
        {
            Name = name;
            Birthdate = birthdate;
            Country = country;

            //Empties:
            Id = 0;
            BirthdateDT = DateTime.Now;
            Profilepic = new byte[0];
        }
        public Person(string name, DateTime birthdate, string country)
        {
            Name = name;
            BirthdateDT = birthdate;
            Country = country;

            //Empties:
            Birthdate = "";
            Profilepic = new byte[0];
            Id = 0;
        }

        public Person(int id, string name, string birthdate, string country)
        {
            Name = name;
            Birthdate = birthdate;
            Country = country;
            Id = id;

            //Empties:
            Profilepic = new byte[0];
            BirthdateDT = DateTime.Now;
        }
        public Person(int id, string name, string birthdate, string country, byte[] profile)   //Only used for retrieved movie/informations from the db. Will be changed to Datetime soon.
        {
            Name = name;
            Birthdate = birthdate;
            Country = country;
            Id = id;
            Profilepic = profile;

            //Empties:
            BirthdateDT = DateTime.Now;
        }
        public Person(int id, string name, DateTime birthdate, string country)
        {
            Name = name;
            BirthdateDT = birthdate;
            Country = country;
            Id = id;

            //Empties:
            Profilepic = new byte[0];
            Birthdate = "";
        }

    }
}
