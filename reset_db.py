
#Database setup script.
#MySQL-Server must be installed and running on localhost.

#The mysql connector module is required for this script, it can be installed from the requirements.txt:
#   pip install -r requirements.txt
#or:
#   python -m pip install mysql-connector-python
#Additionally the Pillow module is used to display or save a preview for a thumbnail (for debuging-purposes):
#   python -m pip install Pillow

# It is critical to follow this given structure to be able to use all functionalities once exported and installed:

# Project/Program working directory
#   ->reset_db.py (Executable)
#   ->images (Dir)
#       ->thumbnails (Dir)
#           ->All thumbnails with correct name.

import mysql.connector
import os
from pathlib import Path

#===========#

def query_itable_moviesthumbnails(verbout):

    thumbnail_names = [
        "Soul.jpg",
        "Der_Rausch.jpg",
        "The_Father.jpg",
        "Tenet.jpg",
        "Promising_young_woman.jpg",
        "Helden_der_Wahrscheinlichkeit.jpg",
        "Der_unsichtbare.jpg",
        "Nomadland.jpg",
        "Palm_Spring.jpg",
        "Dunkirk.jpg",
        "Das_perfekte_Verbrechen.jpg",
        "Dune.jpg",
        "Drive_My_Car.jpg",
        "The_French_Dispatch.jpg",
        "Die_Ausgrabung.jpg",
        "Little_Fish.jpg",
        "Last_Night_in_Soho.jpg",
        "Spider-Man_No_Way_Home.jpg",
        "James_Bond__007_Keine_Zeit_zu_sterben.jpg",
        "The_Green_Knight.jpg",
        "The_Power_of_the_Dog.jpg",
        "West_Side_Story.jpg"
    ]

    current_workingdir = os.getcwd()
    next = "images/thumbnails/"
    imagedir = os.path.join(current_workingdir, next)
    abs_thumbnail_paths = []

    for name in thumbnail_names:
        s = imagedir + name
        abs_thumbnail_paths.append(s)

    movieid = 1
    for p in abs_thumbnail_paths:
        file = open(p, 'rb')
        args = (movieid, file.read())
        query = "INSERT INTO `movies_thumbnails` (`movie_id`, `movie_thumbnail`) VALUES(%s, %s)"
        if verbout: print("Inserted thumbnail: " + str(movieid) + " --- " + thumbnail_names[movieid-1])
        cursor.execute(query, args)
        file.close()
        movieid += 1
    return

def query_itable_directorprofiles(verbout):
    profile_names = [
        "peter_docter.jpg",
        "thomas_vinterberg.jpg",
        "florian_zeller.jpg",
        "christopher_nolan.jpg",
        "emerald_fennell.jpg",
        "anders_thomas_jensen.jpg",
        "leigh_whannell.jpg",
        "chloe_zhano.jpg",
        "max_barbakow.jpg",
        "gregory_hoblit.jpg",
        "matt_reeves.jpg",
        "james_cameron.jpg",
        "rian_johnson.jpg",
        "matthew_vaughn.jpg",
        "wes_anderson.jpg",
        "martin_scorsese.jpg",
        "jamie_foxx.jpg",
        "denis_villeneuve.jpg",
        "ryuesuke_hamaguchi.jpg",
        "simon_stone.jpg",
        "chad_hartigan.jpg",
        "edgar_wright.jpg",
        "john_watts.jpg",
        "cary_joji_fukunaga.jpg",
        "david_lowery.jpg",
        "jane_champion.jpg",
        "steven_spielberg.jpg",
        "paul_thomas_anderson.jpg",
        "guillermo_del_toro.jpg",
        "nikole_beckwith.jpg",
        "sian_heder.jpg",
        "ridley_scott.jpg",
        "ilya_naishuller.jpg",
        "michael_sarnoski.jpg"
    ]
    current_workingdir = os.getcwd()
    next = "images/profiles/directors/"
    imagedir = os.path.join(current_workingdir, next)
    abs_thumbnail_paths = []

    for name in profile_names:
        s = imagedir + name
        abs_thumbnail_paths.append(s)

    directorid = 1
    for p in abs_thumbnail_paths:
        file = open(p, 'rb')
        args = (directorid, file.read())
        query = "INSERT INTO `directors_profiles` (`director_id`, `director_profile`) VALUES(%s, %s)"
        if verbout: print("Inserted profilepicture: " + str(directorid) + " --- " + profile_names[directorid-1])
        cursor.execute(query, args)
        file.close()
        directorid += 1
    return

def query_itable_actorprofiles(verbout):
    profile_names = [
        "richard_ayoade.jpg",
        "mads_mikkelsen.jpg",
        "anthony_hopkins.jpg",
        "robert_pattinson.jpg",
        "carey_mulligan.jpg",
        "elisabeth_moss.jpg",
        "frances_mcdormand.jpg",
        "andy_samberg.jpg",
        "fionn_whitehead.jpg",
        "david_soul.jpg",
        "magnus_millang.jpg",
        "olivia_colman.jpg",
        "elizabeth_debicki.jpg",
        "bo_burnham.jpg",
        "christian_hornhof.jpg",
        "david_strathairn.jpg",
        "christin_milioti.jpg",
        "cillian_murphy.jpg",
        "ryan_gosling.jpg",
        "aldis_hodge.jpg",
        "ansel_elgort.jpg",
        "rachel_zegler.jpg",
        "ariana_debose.jpg",
        "benedict_cumberbatch.jpg",
        "genenieve_lemon.jpg",
        "jesse_plemons.jpg",
        "anais_rizzo.jpg",
        "joe_anderson.jpg",
        "dev_patel.jpg",
        "daniel_craig.jpg",
        "lea_seydoux.jpg",
        "rami_malek.jpg",
        "tom_holland.jpg",
        "zendaya.jpg",
        "thomasin_mckenzie.jpg",
        "aime_cassetari.jpg",
        "rita_tushingham.jpg",
        "olivia_cooke.jpg",
        "jack_o_connell.jpg",
        "soko.jpg",
        "ralph_fiennes.jpg",
        "lily_james.jpg",
        "benicio_tel_toro.jpg",
        "adrien_brody.jpg",
        "tilda_swinton.jpg",
        "hidetoshi_nishijima.jpg",
        "toko_miura.jpg",
        "reika_kirishima.jpg",
        "timothee_chalamet.jpg",
        "rebecca_ferguson.jpg",
        "oscar_isaac.jpg"
    ]
    current_workingdir = os.getcwd()
    next = "images/profiles/actors/"
    imagedir = os.path.join(current_workingdir, next)
    abs_thumbnail_paths = []

    # i = 0
    # while i < len(profile_names) -1:
    #     s = imagedir + profile_names[i]
    #     abs_thumbnail_paths.append(s)
    #     i = i + 1

    for name in profile_names:
        s = imagedir + name
        abs_thumbnail_paths.append(s)

    actorid = 1
    for p in abs_thumbnail_paths:
        file = open(p, 'rb')
        args = (actorid, file.read())
        query = "INSERT INTO `actors_profiles` (`actor_id`, `actor_profile`) VALUES(%s, %s)"
        if verbout: print("Inserted profilepicture: " + str(actorid) + " --- " + profile_names[actorid-1])
        cursor.execute(query, args)
        file.close()
        actorid += 1
    return

#===========#
def create_and_insert_tables(verbout):
    cursor.execute("USE `tmpro`")

    #Table actors:
    cursor.execute("DROP TABLE IF EXISTS `actors`")
    query_ctable_actors = "CREATE TABLE `actors` (`actor_id` int NOT NULL AUTO_INCREMENT,`actor_name` varchar(32) DEFAULT NULL,`actor_birthdate` date DEFAULT NULL,`actor_country` char(2) DEFAULT NULL,PRIMARY KEY (`actor_id`))"
    query_itable_actors = "INSERT INTO `actors` (`actor_name`, `actor_birthdate`, `actor_country`) VALUES (\"Richard Ayoade\",\"1977-5-23\",\"UK\"),(\"Mads Mikkelsen\",\"1965-11-22\",\"DK\"),(\"Antony Hopkins\",\"1937-12-31\",\"UK\"),(\"Robert Pattinson\",\"1986-5-13\",\"UK\"),(\"Carey Mulligan\",\"1985-5-28\",\"UK\"),(\"Elisabeth Moss\",\"1982-7-24\",\"US\"),(\"Frances McDormand\",\"1957-6-23\",\"US\"),(\"Andy Samberg\",\"1978-8-18\",\"US\"),(\"Fionn Whitehead\",\"1997-7-18\",\"UK\"),(\"David Soul\",\"1943-8-28\",\"US\"),(\"Magnus Millang\",\"1981-7-20\",\"DK\"),(\"Olivia Colman\",\"1974-1-30\",\"UK\"),(\"Elizabeth Debicki\",\"1990-8-24\",\"FR\"),(\"Bo Burnham\",\"1990-8-21\",\"US\"),(\"Christian Hornhof\",\"1970-2-1\",\"DE\"),(\"David Strathairn\",\"1949-1-26\",\"US\"),(\"Christin Milioti\",\"1985-8-16\",\"US\"),(\"Cillian Murphy\",\"1976-5-25\",\"IR\"),(\"Ryan Cosling\",\"1980-11-12\",\"CA\"),(\"Aldis hodge\",\"1986-9-20\",\"US\"),(\"Ansel Elgort\", \"1994-3-14\", \"US\"),(\"Rachel Zegler\", \"2001-5-3\", \"US\"),(\"Ariana DeBose\", \"1991-1-25\", \"US\"),(\"Benedict Cumberbatch\", \"1976-7-19\", \"UK\"),(\"Genenieve Lemon\", \"1958-4-21\", \"AU\"),(\"Jesse Plemons\", \"1988-3-2\", \"US\"),(\"Anais Rizzo\", \"1996-1-1\", \"IT\"),(\"Joe Anderson\", \"1982-3-26\", \"EN\"),(\"Dev Patel\", \"1990-4-23\", \"EN\"),(\"Daniel Craig\", \"1968-3-2\", \"EN\"),(\"Lea Seydou\", \"1985-7-1\", \"FR\"),(\"Rami Malek\", \"1981-5-12\", \"US\"),(\"Tom Holland\", \"1996-6-1\", \"EN\"),(\"Zendaya\",\"1996-9-1\", \"US\"),(\"Thomasin McKenzie\", \"2000-7-26\", \"NZ\"),(\"Aimee Cassettari\", \"1970-1-1\", \"US\"),(\"Rita Tushingham\", \"1942-3-14\", \"EN\"),(\"Olivia Cooke\", \"1993-12-27\", \"EN\"),(\"Jack O'Connell\", \"1990-8-1\", \"EN\"),(\"Soko\", \"1985-10-26\", \"FR\"),(\"Ralph Fiennes\", \"1962-12-22\", \"EN\"),(\"Lily James\", \"1989-4-1\", \"EN\"),(\"Benicio del Toro\", \"1967-2-19\", \"PR\"),(\"Adrien Brody\", \"1973-4-14\", \"US\"),(\"Tilda Swinton\", \"1960-11-5\", \"EN\"),(\"Hidetoshi Nishijima\", \"1971-3-29\", \"JP\"),(\"Toko Miura\", \"1996-10-20\", \"JP\"),(\"Reika Kirishima\", \"1972-8-5\", \"JP\"),(\"Timothee Chalamet\", \"1995-12-27\", \"US\"),(\"Rebecca Ferguson\", \"1983-10-19\", \"SE\"),(\"Oscar Isaac\", \"1979-3-9\", \"GT\")"
    cursor.execute(query_ctable_actors)
    cursor.execute(query_itable_actors)
    print("[i]\tActor table created.\n")

    #Table directors:
    cursor.execute("DROP TABLE IF EXISTS `directors`")
    query_ctable_directors = "CREATE TABLE `directors` (`director_id` int NOT NULL AUTO_INCREMENT,`director_name` varchar(32) DEFAULT NULL,`director_birthdate` date DEFAULT NULL,`director_country` char(2) DEFAULT NULL,PRIMARY KEY (`director_id`))"
    query_itable_directors = "INSERT INTO `directors` (`director_name`, `director_birthdate`, `director_country`) VALUES (\"Peter Docter\",\"1986-10-9\",\"US\"),(\"Thomas Vinterberg\",\"1969-5-19\",\"DK\"),(\"Florian Zeller\",\"1979-6-28\",\"FR\"),(\"Christopher Nolan\",\"1970-7-30\",\"UK\"),(\"Emerald Fennell\",\"1985-10-1\",\"UK\"),(\"Anders Thomans Jensen\",\"1972-4-6\",\"DK\"),(\"Leigh Whannell\",\"1977-1-17\",\"AU\"),(\"Chloe Zhano\",\"1982-3-31\",\"US\"),(\"Max Barbakow\",\"1982-3-4\",\"US\"),(\"Gregory Hoblit\",\"1944-11-27\",\"US\"),(\"Matt Reeves\", \"1966-4-27\", \"US\"),(\"James Cameron\", \"1954-8-16\", \"KH\"),(\"Rian Johnson\", \"1973-12-17\", \"US\"),(\"Matthew Vaughn\", \"1971-3-7\", \"UK\"),(\"Wes Anderson\", \"1969-5-1\", \"US\"),(\"Martin Scorsese\", \"1942-11-17\", \"US\"),(\"Jamie Foxx\", \"1967-12-13\", \"US\"),(\"Denis Villeneuve\", \"1967-10-3\", \"KH\"),(\"Ryûsuke Hamaguchi\", \"1978-12-16\", \"JP\"),(\"Simon Stone\", \"1984-8-19\", \"US\"),(\"Chad Hartigan\", \"1982-8-31\", \"CY\"),(\"Edgar Wright\", \"1974-4-18\", \"UK\"),(\"John Watts\", \"1954-12-27\", \"US\"),(\"Cary Joji Fukunaga\", \"1977-7-10\", \"US\"),(\"David Lowery\", \"1980-12-26\", \"US\"),(\"Jane Campion\", \"1954-4-30\", \"NZ\"),(\"Steven Spielberg\", \"1946-12-18\", \"US\"),(\"Paul Thomas Anderson\", \"1970-6-26\", \"US\"),(\"Guillermo del Toro\", \"1964-10-9\", \"MX\"),(\"Nikole Beckwith\", \"1970-1-1\", \"US\"),(\"Sian Heder\", \"1977-6-23\", \"US\"),(\"Ridley Scott\", \"1937-11-30\", \"UK\"),(\"Ilya Naishuller\", \"1937-11-30\", \"RU\"),(\"Michael Sarnoski\", \"1970-1-1\", \"US\")"
    cursor.execute(query_ctable_directors)
    cursor.execute(query_itable_directors)
    print("[i]\tDirector table created.\n")

    #Table movies:
    cursor.execute("DROP TABLE IF EXISTS `movies`")
    query_ctable_movies = "CREATE TABLE `movies` (`movie_id` int NOT NULL AUTO_INCREMENT,`movie_name` varchar(75) DEFAULT NULL,`movie_description` varchar(255) DEFAULT NULL,`movie_directorname` varchar(32) DEFAULT NULL, `movie_directorid` int NOT NULL,`movie_length` int DEFAULT NULL,`movie_rating` double DEFAULT NULL,`movie_releasedate` date DEFAULT NULL, PRIMARY KEY (`movie_id`))"
    query_itable_movies = "INSERT INTO movies (movie_name, movie_description, movie_directorname, movie_directorid, movie_length, movie_rating, movie_releasedate) VALUES(\"Soul\",\"Soul ist eine Reise von New York bis in kosmische Sphären und schickt zwei Seelen auf die Suche nach dem Sinn des Lebens.\",\"Peter Docter\",1,100,7.6,\"2020-10-11\"),(\"Der Rausch\",\"Der Rausch ist eine oscarprämierte dänische Komödie mit Mads Mikkelsen, in der vier Lehrer ein Experiment starten, bei dem sie ihren Alkoholpegel auf einem konstanten Level halten, um der Welt offener zu begegnen.\",\"Thomas Vinterberg\",2,117,7.3,\"2020-04-28\"),(\"The Father\",\"The Father spielt der oscarprämierte Anthony Hopkins einen zunehmend unter Demenz leidenden Vater, der sich nicht von seiner Tochter helfen lassen will.\",\"Florian Zeller\",3,97,7.9,\"2020-01-20\"),(\"Tenet\",\"In Christopher Nolans Thriller Tenet wird John David Washington in eine Welt der Spionage hineingezogen, die er sich so niemals hätte ausmalen können, da die Regeln der Zeit hier scheinbar anders funktionieren.\",\"Christopher Nolan\",4,150,7,\"2020-08-12\"),(\"Promising young woman\",\"Im Thriller Promising Young Woman ergreift die von einer Tragödie in ihrer Vergangenheit traumatisierte Carey Mulligan ihre Chance, Rache an allen zu nehmen, die Frauen nicht genug Respekt entgegenbringen.\",\"Emerald Fennell\",5,113,7.3,\"2020-05-12\"),(\"Helden der Warscheinlichkeit\",\"In der dänischen schwarzen Komödie Helden der Wahrscheinlichkeit schwört Mads Mikkelsen, Rache an denen zu üben, die seine Frau mit einer Zug-Entgleisung getötet haben.\",\"Anders Thomans Jensen\",6,116,7.6,\"2020-11-19\"),(\"Der unsichtbare\",\"Der Unsichtbare verfolgt in diesem Psychohorrorfilm Elisabeth Moss, als sie glaubt, sich endlich von ihrem gewalttätigen Ehemann losgesagt zu haben.\",\"Leigh Whannell\",7,124,6.9,\"2020-02-16\"),(\"Nomadland\",\"Im Oscar-Gewinnerfilm 2021 Nomadland lebt Frances McDormand in ihrem Auto und reist als moderne Nomadin durch den Westen der USA, arbeitet an unterschiedlichen Orten und trifft Menschen, die in einer ähnlichen Situation sind wie sie.\",\"Chloe Zhano\",8,108,7.6,\"2020-07-01\"),(\"Palm Spring\",\"Als der sorgenfreie Nyles und die widerwillige Brautjungfer Sarah einander in Palm Springs bei einer Hochzeit zufällig begegnen, wird es kompliziert, da sie den Gefühlen, die sie für einander haben, nicht entkommen können.\",\"Max Barbakow\",9,100,7.2,\"2020-10-11\"),(\"Dunkirk\",\"Alliierte Soldaten aus Belgien, dem britischen Empire, Kanada und Frankreich sind vom deutschen Heer eingekesselt und werden während einer erbitterten Schlacht im Zweiten Weltkrieg evakuiert.\",\"Christopher Nolan\",4,106,7.8,\"1017-05-19\"),(\"Das perfekte Verbrechen\",\"Ein Anwalt, der beabsichtigt, die Karriereleiter zum Erfolg zu erklimmen, findet einen unwahrscheinlichen Gegner in einem manipulierten Verbrecher, den er zu verfolgen versucht.\",\"Gregory Hoblit\",10,113,7.2,\"2007-04-20\"),(\"Dune\", \"Eine Adelsfamilie wird in einen Krieg um die Kontrolle über das wertvollste Gut der Galaxis verwickelt, während ihr Erbe von Visionen einer dunklen Zukunft geplagt wird.\", \"Denis Villeneuve\",18, 155, 8.0, \"2021-10-22\"),(\"Drive My Car\", \"Nishijima Hidetoshi ist als Bühnenschauspieler und Regisseur glücklich mit seiner Dramatikerin verheiratet. Dann verschwindet eines Tages die Frau.\", \"Ryûsuke Hamaguchi\", 19,179, 7.6, \"2021-12-22\"),(\"The French Dispatch\", \"The French Dispatch erweckt eine Sammlung von Geschichten aus der letzten Ausgabe eines amerikanischen Magazins, das in einer fiktiven französischen Stadt des 20. Jahrhunderts erscheint, zum Leben.\", \"Wes Anderson\", 15,107, 7.2, \"2021-10-21\"),(\"Die Ausgrabung\", \"Während der Zweite Weltkrieg auszubrechen droht, heuert eine vermögende Witwe einen Hobbyarchäologen an, um die Hügel auf ihrem Landsitz auszugraben.\", \"Simon Stone\", 20,112, 7.1, \"2021-1-14\"),(\"Little Fish\", \"Ein Paar kämpft darum, seine Beziehung zusammenzuhalten, während sich ein Virus des Gedächtnisverlusts ausbreitet und droht, die Geschichte ihrer Liebe und ihres Werbens auszulöschen.\", \"Chad Hartigan\", 21,101, 6.8, \"2021-2-5\"),(\"Last Night in Soho\", \"Geplanter Horror-Thriller von Edgar Wright, bei dem eine Frau aus London im Mittelpunkt stehen und der sich an <<Wenn die Gondeln Trauer tragen>> und Roman Polanskis <<Ekel>> orientieren soll.\", \"Edgar Wright\", 22,116, 7.1, \"2021-9-4\"),(\"Spider-Man No Way Home\", \"Da Spider-Mans Identität nun aufgedeckt ist, bittet Peter Doktor Strange um Hilfe. Als ein Zauber schiefgeht, tauchen gefährliche Feinde aus anderen Welten auf und zwingen Peter zu entdecken, was es wirklich bedeutet, Spider-Man zu sein.\", \"John Watts\", 23,148, 8.3, \"2021-12-17\"),(\"James Bond 007 Keine Zeit zu sterben\", \"Eigentlich wollte James Bond mit seiner großen Liebe Madeleine Swann seinen Ruhestand genießen und endlich ein normales Leben führen. Doch zurück in sein altes Leben holt ihn sein alter Freund, CIA-Agent Felix Leiter.\", \"Cary Joji Fukunaga\", 24,163, 7.3, \"2021-9-30\"),(\"The Green Knight\", \"Eine Fantasy-Nacherzählung der mittelalterlichen Geschichte von Sir Gawain und dem Grünen Ritter.\", \"David Lowery\", 25, 130, 6.6, \"2021-7-30\"),(\"The Power of the Dog\", \"Der charismatische Rancher Phil Burbank löst in seiner Umgebung Furcht und Ehrfurcht aus. Als sein Bruder eine neue Frau und ihren Sohn nach Hause bringt, quält Phil sie, bis er sich seiner eigenen Möglichkeit der Liebe ausgesetzt sieht.\", \"Jane Campion\", 26,126, 6.8, \"2021-9-2\"),(\"West Side Story\", \"Eine Adaption des Musicals von 1957, West Side Story, untersucht verbotene Liebe und die Rivalität zwischen den Jets und den Sharks, zwei Straßenbanden unterschiedlicher ethnischer Herkunft.\", \"Steven Spielberg\", 27,156, 7.2, \"2021-12-10\")"
    cursor.execute(query_ctable_movies)
    cursor.execute(query_itable_movies)
    print("[i]\tMovies table created.\n")

    #Table movies_actors:
    cursor.execute("DROP TABLE IF EXISTS `movies_actors`")
    query_ctable_moviesactors = "CREATE TABLE `movies_actors` (  `movie_id` int NOT NULL,  `actor_id` int NOT NULL);"
    query_itable_moviesactors = "INSERT INTO `movies_actors` VALUES (1,1),(1,10),(2,2),(2,11),(3,3),(3,12),(4,4),(4,13),(5,5),(5,14),(6,6),(6,15),(7,6),(7,20),(8,7),(8,16),(9,8),(9,17),(10,9),(10,18),(11,3),(12,2),(22,30),(22,31),(12,50),(12,51),(12,52),(13,47),(13,48),(13,49),(14,44),(14,45),(14,46),(14,31),(15,42),(15,5),(15,43),(16,39),(16,40),(16,41),(17,36),(17,37),(17,38),(18,33),(18,34),(18,24),(19,30),(19,31),(19,32),(20,27),(20,28),(20,29),(21,24),(21,25),(21,26),(22,21),(22,22),(22,23);"
    cursor.execute(query_ctable_moviesactors)
    cursor.execute(query_itable_moviesactors)
    print("[i]\tMovies-actors link table created\n")

    #Table thumbnails:
    cursor.execute("DROP TABLE IF EXISTS `movies_thumbnails`")
    query_ctable_moviesthumbnails = "CREATE TABLE `movies_thumbnails` (  `movie_id` int NOT NULL,  `movie_thumbnail` LONGBLOB NOT NULL);"
    cursor.execute(query_ctable_moviesthumbnails)
    query_itable_moviesthumbnails(verbout)
    print("[i]\tMovies-thumbnails link table created\n")

    #Table director profilepic:
    cursor.execute("DROP TABLE IF EXISTS `directors_profiles`")
    query_ctlable_directorprofiles = "CREATE TABLE `directors_profiles` ( `director_id` int NOT NULL, `director_profile` LONGBLOB NOT NULL);"
    cursor.execute(query_ctlable_directorprofiles)
    query_itable_directorprofiles(verbout)
    print("[i]\tDirector-Profilepictures link table created\n")

    #Table actor profilepic:
    cursor.execute("DROP TABLE IF EXISTS `actors_profiles`")
    query_ctable_actorprofiles = "CREATE TABLE `actors_profiles` (`actor_id` int NOT NULL, `actor_profile` LONGBLOB NOT NULL);"
    cursor.execute(query_ctable_actorprofiles)
    query_itable_actorprofiles(verbout)
    print("[i]\tActor-Profilepictures link table created\n")
#===========#

def showImage():
    # sel_query = "SELECT movie_thumbnail FROM movies_thumbnails WHERE movie_id = 26"
    sel_query = "SELECT director_profile FROM directors_profiles WHERE director_id = 1;"
    cursor.execute("USE tmpro")
    cursor.execute(sel_query)
    data = cursor.fetchall()
    image = data[0][0]
    with open("imgTest.jpg", 'wb') as file:
        file.write(image)
    
#============#

if(__name__ == '__main__'):

    #Show verbose output:
    verbout = True

    print("Please enter root password...")
    password = input()

    database = mysql.connector.connect(
        host="127.0.0.1",
        user="root",
        password=password,
    )

    cursor = database.cursor()

    #showImage()
    #exit()

    #Create database:
    cursor.execute("DROP DATABASE IF EXISTS `tmpro`")
    cursor.execute("CREATE DATABASE `tmpro`")
    cursor.execute("USE tmpro")
    print("[i]\tDatabase created and named `tmpro`.\n")

    create_and_insert_tables(verbout)

    database.commit()
    exit()